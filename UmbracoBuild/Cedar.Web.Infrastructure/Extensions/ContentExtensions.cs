﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.WebSockets;
using Cedar.Web.Infrastructure.Data.Models;
using Cedar.Web.Infrastructure.Data.Models.Interface;
using Cedar.Web.Infrastructure.Data.Models.Supplemental;
using Cedar.Web.Infrastructure.Data.ViewModels;
using Cedar.Web.Infrastructure.Helpers;
using Our.Umbraco.Vorto.Extensions;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Our.Umbraco.Vorto.Models;

namespace Cedar.UmbracoExtensions.Extensions
{
    public static class ContentExtensions
    {
        public static T As<T>(this IPublishedContent content)
        {
            // Create an empty instance of the POCO
            var model = Activator.CreateInstance<T>();
            var helper = new UmbracoHelper(UmbracoContext.Current);

            // Discover properties of the poco with reflection
            PropertyInfo[] properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            var modelType = model.GetType();

            foreach (PropertyInfo propertyInfo in properties)
            {
                var contentType = content.GetType();
                if (contentType.GetProperty(propertyInfo.Name) != null)
                {
                    // It is a default propery - get the value with refelection
                    var propertyValue = contentType.GetProperty(propertyInfo.Name).GetValue(content, null);
                    modelType.GetProperty(propertyInfo.Name).SetValue(model, propertyValue, null);
                }
                else if (content.GetPropertyValue(propertyInfo.Name) is VortoValue)
                {
                    // it is a translated vorto value
                    var propertyValue = content.GetVortoValue(propertyInfo.Name, fallbackCultureName: "en-GB");
                    modelType.GetProperty(propertyInfo.Name).SetValue(model, propertyValue, null);
                }
                else if (propertyInfo.PropertyType == typeof(SiteContainer))
                {
                    var propertyValue = Convert.ToInt32(content.GetPropertyValue(propertyInfo.Name));
                    modelType.GetProperty(propertyInfo.Name).SetValue(model, helper.TypedContent(propertyValue).As<SiteContainer>(), null);
                }
                else
                {
                    // it is a doctype property - ask Umbraco for the value
                    var propertyValue = content.GetPropertyValue(propertyInfo.Name);
                    modelType.GetProperty(propertyInfo.Name).SetValue(model, propertyValue, null);
                }
            }

            return model;
        }

        public static WidgetVm GetWidget(this IPublishedContent content, string propertyName)
        {
            var property = content.GetProperty(propertyName);

            if (property != null && !string.IsNullOrEmpty(property.DataValue.ToString()))
            {
                var macros = property.DataValue.ToString().Split(new string[] { "/>" }, StringSplitOptions.RemoveEmptyEntries).Select(p => p + "/>");

                foreach (var macro in macros)
                {
                    var macroAlias = NodeHelper.GetPropertyValue("macroAlias", macro).ToString();
                    var dataSourceId = 0;
                    var dataSource = NodeHelper.GetPropertyValue("DataSource", macro).ToString();

                    int.TryParse(dataSource, out dataSourceId);

                    return new WidgetVm { MacroAlias = macroAlias, DataSourceId = dataSourceId };
                }

            }
            return null;
        }

        public static IList<WidgetVm> GetWidgets(this IPublishedContent content, string propertyName)
        {
            var widgets = new List<WidgetVm>();

            var property = content.GetProperty(propertyName);

            if (property != null && !string.IsNullOrEmpty(property.DataValue.ToString()))
            {
                var macros = property.DataValue.ToString().Split(new string[] { "/>" }, StringSplitOptions.RemoveEmptyEntries).Select(p => p + "/>");

                foreach (var macro in macros)
                {
                    var macroAlias = NodeHelper.GetPropertyValue("macroAlias", macro).ToString();
                    var dataSourceId = 0;
                    var dataSource = NodeHelper.GetPropertyValue("DataSource", macro).ToString();

                    int.TryParse(dataSource, out dataSourceId);

                    widgets.Add(new WidgetVm() { MacroAlias = macroAlias, DataSourceId = dataSourceId });
                }

            }
            return widgets;
        }

        public static string ToSlug(this string value)
        {
            return value.Replace("&", "and").Replace(" ", "-").ToLower();
        }

        public static MvcHtmlString GetVortoGridHtml(this HtmlHelper html, IPublishedContent contentItem, string propertyAlias, string framework)
        {
            Mandate.ParameterNotNullOrEmpty(propertyAlias, "propertyAlias");
            string text = "Grid/" + framework;
            object value = contentItem.GetVortoValue(propertyAlias);
            string text2 = value as string;
            if (text2 != null && string.IsNullOrEmpty(text2))
            {
                return new MvcHtmlString(string.Empty);
            }
            return PartialExtensions.Partial(html, text, value);
        }

        public static void GetFooterIncludes(this IWidgets customModel)
        {
            var model = ((BaseModel)customModel);
            foreach (var widget in customModel.Widgets)
            {
                switch (widget.MacroAlias)
                {
                    case "PeopleList":
                    case "ServicesList":
                        model.FooterIncludes["React"] = true;
                        break;
                    case "OfficesList":
                        model.FooterIncludes["Maps"] = true;
                        break;
                }
            }
        }

        public static string GetUrl(this UrlHelper html, string fileName)
        {
            var absoluteFileName = html.RequestContext.HttpContext.Server.MapPath(fileName);

            if (!System.IO.File.Exists(absoluteFileName)) return fileName;

            var version = System.IO.File.GetLastWriteTimeUtc(absoluteFileName).Ticks.ToString();
            return string.Concat("/static/", version, fileName);
        }

        public static string GetAnalyticsUrl(this UrlHelper html, string fileName)
        {
            const string analyticsScript = "https://www.google-analytics.com/analytics.js";
            var absoluteFileName = html.RequestContext.HttpContext.Server.MapPath(fileName);

            HttpWebResponse response;           

            // check if we have a local file
            if (System.IO.File.Exists(absoluteFileName))
            {
                // if its been less than 8 hours since we last checked.. just return our version of analytics
                var valid = HttpContext.Current.Cache["ValidAnalyticsJs"] as bool?;
                if (valid.HasValue)
                {
                    return html.GetUrl(fileName);
                }
                
                // otherwise compare our version with google's to see if we need to get the latest file
                var version = System.IO.File.GetLastWriteTimeUtc(absoluteFileName);

                var request = (HttpWebRequest)WebRequest.Create(analyticsScript);
                response = (HttpWebResponse)request.GetResponse();

                var googleVersion = response.LastModified;

                // if our version has a modified date later than google's version, then just return our version of analytics
                if (version > googleVersion.ToUniversalTime())
                {
                    // first set the current file to valid for another 8 hours
                    HttpContext.Current.Cache.Insert("ValidAnalyticsJs", true, null, DateTime.Now.AddHours(8), Cache.NoSlidingExpiration);

                    return html.GetUrl(fileName);
                }
            }
            else
            {
                var request = (HttpWebRequest)WebRequest.Create(analyticsScript);
                response = (HttpWebResponse)request.GetResponse();
            }

            try
            { 
                // if we are here, we either have no version locally, or it has expired so we will request google's version
                using (var output = System.IO.File.Open(absoluteFileName, FileMode.Create))
                using (var input = response.GetResponseStream())
                {
                    // if the input is null there may have been a problem requesting the file, just return the google analytics script
                    if (input == null) return analyticsScript;

                    // otherwise save google's version locally, and set it to valid for 8 hours
                    input.CopyTo(output);
                    HttpContext.Current.Cache.Insert("ValidAnalyticsJs", true, null, DateTime.Now.AddHours(8), Cache.NoSlidingExpiration);

                    return html.GetUrl(fileName);
                }
            }
            catch(Exception e)
            {
                return analyticsScript;
            }
        }

        public static string GetJsonFeedUrl(this UrlHelper html, string fileName, string feedUrl)
        {
            var absoluteFileName = html.RequestContext.HttpContext.Server.MapPath(fileName);
            var cacheKey = "Valid" + fileName;

            // check if we have a local file
            if (System.IO.File.Exists(absoluteFileName))
            {
                // if the cache has not expired for this file
                var valid = HttpContext.Current.Cache[cacheKey] as bool?;
                if (valid.HasValue)
                {
                    return html.GetUrl(fileName);
                }
            }

            var domain = HttpContext.Current.Request.Url.Scheme + Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host;

            try
            {
                // if we are here, we either have no version locally, or it has expired so we will request the feed
                var request = (HttpWebRequest)WebRequest.Create(domain + feedUrl);
                var response = (HttpWebResponse)request.GetResponse();

                // create the json folder if it doesnt already exist
                Directory.CreateDirectory(html.RequestContext.HttpContext.Server.MapPath("/json"));

                using (var output = System.IO.File.Open(absoluteFileName, FileMode.Create))
                using (var input = response.GetResponseStream())
                {
                    // if the input is null there may have been a problem requesting the feed, just return the full feed url
                    if (input == null) return feedUrl;

                    // otherwise save the feed version locally, and set it to valid until the umbraco.config changes
                    input.CopyTo(output);

                    var dependency = new CacheDependency(HttpContext.Current.Server.MapPath("/App_Data/umbraco.config"));
                    HttpContext.Current.Cache.Insert(cacheKey, true, dependency, Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration);

                    return html.GetUrl(fileName);
                }
            }
            catch (Exception)
            {
                // something went wrong, just return the feed url
                return feedUrl;
            }
        }
    }
}
