﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cedar.Web.Infrastructure.Helpers;
using Umbraco.Core;
using Umbraco.Web.Routing;

namespace Umbraco.Extensions.EventHandlers
{
    public class DynamicPageRoutes : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            base.ApplicationStarting(umbracoApplication, applicationContext);
            UrlProviderResolver.Current.InsertTypeBefore<DefaultUrlProvider, ArticleUrlProvider>();
            ContentFinderResolver.Current.InsertType<ContentFinder>();
        }
    }
}