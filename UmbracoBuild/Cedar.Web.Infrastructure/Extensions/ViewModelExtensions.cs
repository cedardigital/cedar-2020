﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc.Html;
using System.Web.UI.WebControls;
using Cedar.UmbracoExtensions.Extensions;
using Cedar.Web.Infrastructure.Data.Models.Folder;
using Cedar.Web.Infrastructure.Data.Models.Interface;
using Cedar.Web.Infrastructure.Data.Models.JsonResult;
using Our.Umbraco.uTwit;
using Our.Umbraco.uTwit.Models;
using umbraco;
using Umbraco.Core;
using Umbraco.Web;
using Cedar.Web.Infrastructure.Data.Models.Supplemental;
using Cedar.Web.Infrastructure.Data.Models.Widgets;
using Cedar.Web.Infrastructure.Data.ViewModels;
using Cedar.Web.Infrastructure.Helpers;
using Umbraco.Web.Media.EmbedProviders.Settings;

namespace Cedar.Web.Infrastructure.Extensions
{
    public static class ViewModelExtensions
    {
        private readonly static UmbracoHelper Helper = new UmbracoHelper(UmbracoContext.Current);

        public static HomeBannerVm Map(this HomeBanner content)
        {
            var model = new HomeBannerVm
            {
                Title = content.Title.Replace(Environment.NewLine, "<br>"),
                Summary = content.Summary,
                VideoBackgroundUrl = content.VideoBackgroundUrl,
                FallbackImage = string.Format("{0}?width=2000&height=1125", Helper.Media(content.FallbackImage).Url),
                FallbackImageMobile = string.Format("{0}?width=720&height=405", Helper.Media(content.FallbackImage).Url),
                Link = Helper.TypedContent(content.Link),
                FeaturedNews = Helper.TypedContent(content.FeaturedNews),
                DisplayVideo = true
            };

            var userAgent = HttpContext.Current.Request.UserAgent;
            if (userAgent != null && (userAgent.ToLower().Contains("iphone") || userAgent.ToLower().Contains("ipad") || userAgent.ToLower().Contains("android")))
            {
                model.DisplayVideo = false;
            }

            return model;
        }

        public static BannerVm Map(this Banner content)
        {
            var model = new BannerVm
            {
                Title = content.Title,
                SubTitle = content.SubTitle,
                IntroText = content.IntroText,
                Image = string.Format("{0}?width=2000&height=1125", Helper.Media(content.Image).Url),
                MobileImage = string.Format("{0}?width=720&height=405", Helper.Media(content.Image).Url),
                BackgroundPosition = content.BackgroundPosition,
                Logo = Helper.Media(content.Logo)
            };

            return model;
        }
        
        public static AwardsListVm Map(this AwardsList content)
        {
            var model = new AwardsListVm
            {
                Title = content.Title,
                Awards = NodeHelper.GetMultiNodePickerItems<Award>(content.Awards)
            };

            return model;
        }

        public static CaseStudiesListVm Map(this CaseStudiesList content)
        {
            var model = new CaseStudiesListVm
            {
                Title = content.Title,
                CaseStudies = NodeHelper.GetMultiNodePickerItems<CaseStudy>(content.CaseStudies),
                Link = Helper.Content(content.Link),
                LinkTitle = content.LinkTitle
            };

            return model;
        }

        public static ClientsListVm Map(this ClientsList content)
        {
            var model = new ClientsListVm
            {
                Title = content.Title,
                Clients = NodeHelper.GetMultiNodePickerItems<Client>(content.Clients)
            };

            return model;
        }

        public static FullWidthImageVm Map(this FullWidthImage content)
        {
            var model = new FullWidthImageVm
            {
                Image = Helper.Media(content.Image),
                Caption = content.Caption
            };

            return model;
        }

        public static JobsListVm Map(this JobsList content)
        {
            var model = new JobsListVm
            {
                Jobs = NodeHelper.GetMultiNodePickerItems<Job>(content.Jobs)
            };

            return model;
        }

        public static MediaListVm Map(this MediaList content)
        {
            var model = new MediaListVm
            {
                Title = content.Title,
                MediaItems = NodeHelper.GetMultiNodePickerItems<MediaItem>(content.MediaItems)
            };

            return model;
        }

        public static RichTextContentVm Map(this RichTextContent content)
        {
            var model = new RichTextContentVm
            {
                Content = content.Content
            };

            return model;
        }

        public static WhitePapersListVm Map(this WhitePapersList content)
        {
            var model = new WhitePapersListVm
            {
                Title = content.Title,
                WhitePapers = NodeHelper.GetMultiNodePickerItems<WhitePaper>(content.WhitePapers)
            };

            return model;
        }

        public static OfficesListVm Map(this OfficesList content)
        {
            var model = new OfficesListVm
            {
                Title = content.Title,
                Offices = NodeHelper.GetMultiNodePickerItems<Office>(content.Offices)
            };

            return model;
        }

        public static PeopleListVm Map(this PeopleList content)
        {
            var model = new PeopleListVm
            {
                DataSourceId = content.Id,
                Title = content.Title
            };

            return model;
        }

        public static TechnologiesListVm Map(this TechnologiesList content)
        {
            var model = new TechnologiesListVm
            {
                Title = content.Title,
                Technologies = NodeHelper.GetMultiNodePickerItems<Technology>(content.Technologies)
            };

            return model;
        }

        public static TweetsListVm Map(this TweetsList content)
        {
            var tweets = uTwit.LookupTweets(content.TwitterAuth, content.StickyTweets).ToList();
            if (tweets.Count() < 3)
            {
                // take from the latest tweets but make sure there are no duplicates
                var latest = uTwit.GetLatestTweets(content.TwitterAuth, content.ScreenName, 10, false, false).Where(x => tweets.All(t => t.Id != x.Id));
                tweets.AddRange(latest.Take(3 - tweets.Count()));
            }

            var model = new TweetsListVm
            {
                Title = content.Title,
                Tweets = tweets
            };

            return model;
        }

        public static IEnumerable<PersonJson> MapToJson(this PeopleList content)
        {
            return NodeHelper.GetMultiNodePickerItems<Person>(content.People).Select((person, i) => new PersonJson
            {
                id = i,
                name = person.Name,
                title = person.Title,
                img = string.Format("{0}?width=320&height=320", Helper.Media(person.Image).Url),
                thumb = string.Format("{0}?width=250&height=250", Helper.Media(person.Image).Url),
                desc = (person.Summary ?? string.Empty).Replace(Environment.NewLine, "<br>"),
                email = person.Email
            });
        }

        //public static ServicesListVm Map(this ServicesList content)
        //{
        //    var model = new ServicesListVm
        //    {
        //        DataSourceId = content.Id
        //    };

        //    return model;
        //}

        public static IEnumerable<ServiceJson> MapToJson(this ServicesList content)
        {
            return NodeHelper.GetMultiNodePickerItems<ServiceExtract>(content.Services).Select((service, i) => new ServiceJson
            {
                id = i,
                name = service.NavigationTitle,
                thumb = string.Format("{0}?width=307&height=173", Helper.Media(service.Thumb).Url),
                desc = (service.Summary ?? string.Empty).Replace(Environment.NewLine, "<br>"),
                link = service.DocumentTypeAlias == "Service" ? service.Url : string.Empty
            });
        }

        public static CarouselVm Map(this Carousel content)
        {
            var model = new CarouselVm
            {
                DataSourceId = content.Id
            };

            return model;
        }

        public static IEnumerable<CarouselItemJson> MapToJson(this Carousel content)
        {
            return content.Children.Select(x => x.As<CarouselItem>()).Select((carouselItem, i) => new CarouselItemJson
            {
                id = i,
                imageDesktop = string.Format("{0}?width=1366&height=768", Helper.Media(carouselItem.Image).Url),
                imageMobile = string.Format("{0}?width=720&height=405", Helper.Media(carouselItem.Image).Url),
                caption = carouselItem.Caption
            });
        }

        public static CaseStudyLandingJson MapToJson(this CaseStudyLanding content)
        {
            var caseStudiesList = NodeHelper.GetAllCaseStudies().ToList();
            if (content.OnlyShowLocalContent || content.PrioritiseLocalContent)
            {
                // first filter articles by current site
                var site = NodeHelper.GetCurrentSite();
                var filteredCaseStudiesList = caseStudiesList.Where(x => x.PrimarySite.Id == site.Id).ToList();

                // if we are prioritising local content, add the other articles back in at the end of the list
                if (content.PrioritiseLocalContent)
                {
                    filteredCaseStudiesList.AddRange(caseStudiesList.Where(x => x.PrimarySite.Id != site.Id));
                }

                caseStudiesList = filteredCaseStudiesList;
            }

            var allTags = caseStudiesList.Where(x => x.Tags != null).SelectMany(x => NodeHelper.GetMultiNodePickerItems<Tag>(x.Tags)).Distinct();
            var tagsList = allTags.OrderBy(tag => tag.TagName).Select(tag => new TagJson
            {
                name = tag.TagName,
                slug = tag.Name.ToSlug()
            });

            return new CaseStudyLandingJson
            {
                tags = tagsList,
                items = caseStudiesList.Select((caseStudy, i) => new CaseStudyJson
                {
                    id = i,
                    tags = (caseStudy.Tags != null ? NodeHelper.GetMultiNodePickerItems<Tag>(caseStudy.Tags).Select(tag => tag.Name.ToSlug()) : new string[0]),
                    title =  caseStudy.NavigationTitle,
                    desc = caseStudy.Description,
                    image = string.Format("{0}?width=684&height=385", Helper.Media(caseStudy.Image).Url),
                    url = caseStudy.Url,
                    location = caseStudy.PrimarySite.Title
                })
            };
        }

        public static NewsLandingJson MapToJson(this NewsLanding content)
        {
            var articlesList = NodeHelper.GetAllArticles().ToList();
            if (content.OnlyShowLocalContent || content.PrioritiseLocalContent)
            {
                // first filter articles by current site
                var site = NodeHelper.GetCurrentSite();
                var filteredArticlesList = articlesList.Where(x => x.PrimarySite.Id == site.Id).ToList();
                
                // if we are prioritising local content, add the other articles back in at the end of the list
                if (content.PrioritiseLocalContent)
                {
                    filteredArticlesList.AddRange(articlesList.Where(x => x.PrimarySite.Id != site.Id));
                }

                articlesList = filteredArticlesList;
            }

            var categoriesList = articlesList.Select(article => article.Category).Distinct().OrderBy(tag => tag).Select(tag => new TagJson
            {
                name = library.GetDictionaryItem(tag),
                slug = tag.ToSlug()
            });
            var allTags = articlesList.Where(x => x.Tags != null).SelectMany(x => NodeHelper.GetMultiNodePickerItems<Tag>(x.Tags)).Distinct();
            var tagsList = allTags.OrderBy(tag => tag.TagName).Select(tag => new TagJson
            {
                name = tag.TagName,
                slug = tag.Name.ToSlug()
            });
            // select active authors (authors with at least 3 posts)
            var activeAuthorsList = articlesList.Where(x => x.Author > 0).GroupBy(article => article.Author).Where(group => group.Count() >= 3).Select(group => new { Author = Helper.Content(group.Key).Name }).OrderBy(author => author.Author).Select(tag => new TagJson
            {
                name = tag.Author,
                slug = ((string)tag.Author).ToSlug()
            });
            
            return new NewsLandingJson
            {
                categories = categoriesList,
                tags = tagsList,
                activeAuthors = activeAuthorsList,
                items = articlesList.Select((article, i) => new ArticleJson
                {
                    id = i,
                    tags = (article.Tags != null ? NodeHelper.GetMultiNodePickerItems<Tag>(article.Tags).Select(tag => tag.Name.ToSlug()) : new string[0]),
                    title = article.NavigationTitle,
                    image = string.Format("{0}?width=307&height=173", Helper.Media(article.Image).Url),
                    category = article.Category.ToSlug(),
                    author = ((string)Helper.Content(article.Author).Name).ToSlug(),
                    url = article.Url,
                    location = article.Location
                })
            };
        }

        public static HeaderVm Map(this Header content)
        {
            var model = new HeaderVm
            {
                HeaderLinks = content.Children.Where(x => x.Name == "Main")
                    .SelectMany(x => x.Children)
                    .Select(item => new { ItemName = item.Name, Link = Helper.TypedContent(item["Link"]) })
                    .Select(link => new InternalLink
                {
                    Name = link.ItemName,
                    Class = HttpContext.Current.Request.Url.AbsolutePath.Contains(link.Link.Url) ? "on" : string.Empty,
                    Link = link.Link
                }).ToList()
            };

            return model;
        }

        public static FooterVm Map(this Footer content)
        {
            var model = new FooterVm
            {
                Offices = NodeHelper.GetMultiNodePickerItems<Office>(content.Offices),
                TopLinks = content.Children.Where(x => x.Name == "Top")
                    .SelectMany(x => x.Children)
                    .Select(item => new { ItemName = item.Name, Link = Helper.Content(item["Link"]) })
                    .Select(link => new InternalLink
                {
                    Name = link.ItemName,
                    Link = link.Link
                }).ToList(),
                BottomLinks = content.Children.Where(x => x.Name == "Bottom")
                    .SelectMany(x => x.Children)
                    .Select(item => new { ItemName = item.Name, Link = Helper.Content(item["Link"]) })
                    .Select(link => new InternalLink
                {
                    Name = link.ItemName,
                    Link = link.Link
                }).ToList()
            };

            return model;
        }
    }
}
