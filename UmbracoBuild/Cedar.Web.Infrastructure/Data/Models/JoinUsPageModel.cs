﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cedar.Web.Infrastructure.Data.Models.Interface;
using Cedar.Web.Infrastructure.Data.ViewModels;
using Umbraco.Core.Models;
using Umbraco.Web.Models;

namespace Cedar.Web.Infrastructure.Data.Models
{
    public class JoinUsPageModel : BaseModel, IWidgets
    {
        public IList<WidgetVm> Widgets { get; set; }
        public WidgetVm CTA { get; set; }

        public JoinUsPageModel(RenderModel model) : base(model) { }
    }
}
