﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace Cedar.Web.Infrastructure.Data.Models.Supplemental
{
    public class InternalLink : BaseDocument
    {
        public string Class { get; set; }
        public IPublishedContent Link { get; set; }
    }
}
