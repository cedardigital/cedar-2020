﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cedar.Web.Infrastructure.Data.Models.Supplemental
{
    public class BaseDocument
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string UrlName { get; set; }
    }
}
