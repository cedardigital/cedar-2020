﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace Cedar.Web.Infrastructure.Data.Models.Supplemental
{
    public class Article : BaseDocument
    {
        public string Tags { get; set; }
        public string NavigationTitle { get; set; }
        public int Image { get; set; }
        public string Category { get; set; }
        public int Author { get; set; }
        public string Location { get; set; }
        public SiteContainer PrimarySite { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
