﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core.Models;

namespace Cedar.Web.Infrastructure.Data.Models.Supplemental
{
    public class Tag : BaseDocument
    {
        public string TagName { get; set; }
    }
}
