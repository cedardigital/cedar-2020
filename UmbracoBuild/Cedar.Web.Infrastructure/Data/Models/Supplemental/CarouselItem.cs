﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core.Models;

namespace Cedar.Web.Infrastructure.Data.Models.Supplemental
{
    public class CarouselItem : BaseDocument
    {
        public int Image { get; set; }
        public string Caption { get; set; }
    }
}
