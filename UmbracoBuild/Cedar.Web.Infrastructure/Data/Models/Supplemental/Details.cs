﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cedar.Web.Infrastructure.Data.Models.Supplemental
{
    public class Details : BaseDocument
    {
        public string Title { get; set; }
        public string Content { get; set; }
    }
}
