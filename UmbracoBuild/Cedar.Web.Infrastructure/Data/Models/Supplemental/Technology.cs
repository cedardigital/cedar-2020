﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cedar.Web.Infrastructure.Data.Models.Supplemental
{
    public class Technology : BaseDocument
    {
        public int Logo { get; set; }
        public string Content { get; set; }
    }
}
