﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace Cedar.Web.Infrastructure.Data.Models.Supplemental
{
    public class MediaItem : BaseDocument
    {
        public int Image { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Link { get; set; }
    }
}
