﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cedar.Web.Infrastructure.Data.Models.Supplemental
{
    public class Award : BaseDocument
    {
        public string Title { get; set; }
        public string AwardTitle { get; set; }
        public string Client { get; set; }
        public string Description { get; set; }
        public int CaseStudyLink { get; set; }
    }
}
