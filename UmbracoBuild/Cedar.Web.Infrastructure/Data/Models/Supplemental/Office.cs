﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cedar.Web.Infrastructure.Data.Models.Supplemental
{
    public class Office : BaseDocument
    {
        public string Title { get; set; }
        public string OfficeAddress { get; set; }
        public string OfficePhone { get; set; }
        public string PostalAddress { get; set; }
        public string ContactDetails { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public string MapLink { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string TimeZone { get; set; }
    }
}
