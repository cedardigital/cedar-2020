﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cedar.Web.Infrastructure.Data.Models.Supplemental
{
    public class ServiceExtract : BaseDocument
    {
        public string DocumentTypeAlias { get; set; }
        public string NavigationTitle { get; set; }
        public int Thumb { get; set; }
        public string Summary { get; set; }
    }
}
