﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core.Models;

namespace Cedar.Web.Infrastructure.Data.Models.Supplemental
{
    public class Service : BaseDocument
    {
        public string NavigationTitle { get; set; }
        public int Thumb { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
    }
}
