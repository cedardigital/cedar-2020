﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace Cedar.Web.Infrastructure.Data.Models.Supplemental
{
    public class WhitePaper : BaseDocument
    {
        public string Title { get; set; }
        public string Summary { get; set; }
        public int Thumb { get; set; }
        public int Download { get; set; }
        public int RelatedArticle { get; set; }
    }
}
