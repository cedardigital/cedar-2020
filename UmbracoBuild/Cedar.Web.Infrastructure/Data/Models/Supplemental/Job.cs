﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Cedar.Web.Infrastructure.Data.Models.Supplemental
{
    public class Job : BaseDocument
    {
        public string JobTitle { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
    }
}
