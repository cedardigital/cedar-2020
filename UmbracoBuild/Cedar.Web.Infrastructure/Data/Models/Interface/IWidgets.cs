﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cedar.Web.Infrastructure.Data.ViewModels;

namespace Cedar.Web.Infrastructure.Data.Models.Interface
{
    public interface IWidgets
    {
        IList<WidgetVm> Widgets { get; set; }
    }
}
