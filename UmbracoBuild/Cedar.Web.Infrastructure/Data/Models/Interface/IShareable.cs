﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cedar.Web.Infrastructure.Data.Models.Interface
{
    public interface IShareable
    {
        string Title { get; set; }
    }
}
