﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace Cedar.Web.Infrastructure.Data.Models.Folder
{
    public class Footer
    {
        public string Offices { get; set; }
        public IOrderedEnumerable<IPublishedContent> Children { get; set; }
    }
}
