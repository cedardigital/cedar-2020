﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace Cedar.Web.Infrastructure.Data.Models.JsonResult
{
    public class CarouselItemJson
    {
        public int id { get; set; }
        public string imageDesktop { get; set; }
        public string imageMobile { get; set; }
        public string caption { get; set; }
    }
}
