﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace Cedar.Web.Infrastructure.Data.Models.JsonResult
{
    public class NewsLandingJson
    {
        public IEnumerable<TagJson> categories { get; set; }
        public IEnumerable<TagJson> tags { get; set; }
        public IEnumerable<TagJson> activeAuthors { get; set; }
        public IEnumerable<ArticleJson> items { get; set; }
    }
}
