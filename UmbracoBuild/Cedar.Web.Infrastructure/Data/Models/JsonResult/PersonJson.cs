﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace Cedar.Web.Infrastructure.Data.Models.JsonResult
{
    public class PersonJson
    {
        public int id { get; set; }
        public string name { get; set; }
        public string title { get; set; }
        public string thumb { get; set; }
        public string img { get; set; }
        public string desc { get; set; }
        public string email { get; set; }
    }
}
