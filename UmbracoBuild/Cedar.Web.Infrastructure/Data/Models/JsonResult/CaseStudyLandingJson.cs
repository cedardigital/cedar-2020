﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace Cedar.Web.Infrastructure.Data.Models.JsonResult
{
    public class CaseStudyLandingJson
    {
        public IEnumerable<TagJson> tags { get; set; }
        public IEnumerable<CaseStudyJson> items { get; set; }
    }
}
