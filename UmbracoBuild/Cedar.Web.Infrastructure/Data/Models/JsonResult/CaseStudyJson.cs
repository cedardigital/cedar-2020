﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace Cedar.Web.Infrastructure.Data.Models.JsonResult
{
    public class CaseStudyJson
    {
        public int id { get; set; }
        public IEnumerable<string> tags { get; set; } 
        public string title { get; set; }
        public string desc { get; set; }
        public string image { get; set; }
        public string url { get; set; }
        public string location { get; set; }
    }
}
