﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cedar.Web.Infrastructure.Data.Models.Interface;
using Cedar.Web.Infrastructure.Data.ViewModels;
using Umbraco.Core.Models;
using Umbraco.Web.Models;

namespace Cedar.Web.Infrastructure.Data.Models
{
    public class Error404Model : BaseModel
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string IntroText { get; set; }

        public Error404Model(RenderModel model) : base(model) { }
    }
}
