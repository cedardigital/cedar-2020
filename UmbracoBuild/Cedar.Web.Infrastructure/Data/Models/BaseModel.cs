﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Cedar.Web.Infrastructure.Data.Models.Supplemental;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Cedar.UmbracoExtensions.Extensions;
using Cedar.Web.Infrastructure.Helpers;
using Our.Umbraco.Vorto.Extensions;
using Our.Umbraco.Vorto.Models;

namespace Cedar.Web.Infrastructure.Data.Models
{
    public class BaseModel : RenderModel
    {
        public dynamic CurrentPage { get; set; }
        public string MetaTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaUrl { get; set; }
        public string MetaImage { get; set; }
        public string HomeUrl { get; set; }
        public string BodyClass { get; set; }
        public Dictionary<string, bool> FooterIncludes { get; set; }

        public BaseModel(RenderModel model) : base(model.Content, model.CurrentCulture)
        {
            var dynamicPublishedContent = new DynamicPublishedContent(Content);
            CurrentPage = dynamicPublishedContent.AsDynamic();

            var metaTitle = model.Content.GetVortoValue("MetaTitle", fallbackCultureName: "en-GB");
            MetaTitle = metaTitle as string ?? model.Content.Name;
            MetaDescription = model.Content.GetVortoValue("MetaDescription", fallbackCultureName: "en-GB") as string ?? string.Empty;

            var domain = HttpContext.Current.Request.Url.Scheme + Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host;
            MetaUrl = domain + CurrentPage.Url;
            MetaImage = domain + "/images/logo-banner.png";
            if (!Object.ReferenceEquals(CurrentPage.Image, Umbraco.Core.Dynamics.DynamicNull.Null))
            {
                MetaImage = domain + UmbracoContext.Current.MediaCache.GetById(CurrentPage.Image).Url;
            }

            var home = NodeHelper.GetHomePage();
            HomeUrl = domain + home.Url.TrimEnd('/');

            var classes = new List<string>();

            var userAgent = HttpContext.Current.Request.UserAgent;
            if (userAgent != null && (userAgent.ToLower().Contains("iphone") || userAgent.ToLower().Contains("ipad") || userAgent.ToLower().Contains("android")))
            {
                classes.Add("is-touch");
            }

            if (HttpContext.Current.Request.Cookies.AllKeys.Contains("webfont-primary"))
            {
                classes.Add("webfont-primary");
            }
            if (HttpContext.Current.Request.Cookies.AllKeys.Contains("webfont-secondary"))
            {
                classes.Add("webfont-secondary");
            }
            if (HttpContext.Current.Request.Cookies.AllKeys.Contains("webfont-tertiary"))
            {
                classes.Add("webfont-tertiary");
            }

            BodyClass = string.Join(" ", classes);

            FooterIncludes = new Dictionary<string, bool>();
        }
    }
}
