﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Cedar.Web.Infrastructure.Data.Models.Widgets
{
    public class FullWidthImage
    {
        public int Image { get; set; }
        public string Caption { get; set; }
    }
}
