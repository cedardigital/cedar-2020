﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Our.Umbraco.uTwit.Models;
using Umbraco.Core.Models;

namespace Cedar.Web.Infrastructure.Data.Models.Widgets
{
    public class TweetsList
    {
        public string Title { get; set; }
        public uTwitModel TwitterAuth { get; set; }
        public string ScreenName { get; set; }
        public string StickyTweets { get; set; }
    }
}
