﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cedar.Web.Infrastructure.Data.Models.Supplemental;
using Umbraco.Core.Models;

namespace Cedar.Web.Infrastructure.Data.Models.Widgets
{
    public class Carousel
    {
        public int Id { get; set; }
        public IOrderedEnumerable<IPublishedContent> Children { get; set; }
    }
}
