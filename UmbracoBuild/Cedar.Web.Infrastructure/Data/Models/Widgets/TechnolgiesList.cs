﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace Cedar.Web.Infrastructure.Data.Models.Widgets
{
    public class TechnologiesList
    {
        public string Title { get; set; }
        public string Technologies { get; set; }
    }
}
