﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Cedar.Web.Infrastructure.Data.Models.Widgets
{
    public class BannerWithLogo
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public int Image { get; set; }
        public int MobileImage { get; set; }
        public int Logo { get; set; }
    }
}
