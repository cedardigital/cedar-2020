﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace Cedar.Web.Infrastructure.Data.Models.Widgets
{
    public class CaseStudiesList
    {
        public string Title { get; set; }
        public string CaseStudies { get; set; }
        public int Link { get; set; }
        public string LinkTitle { get; set; }
    }
}
