﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cedar.Web.Infrastructure.Data.Models.Widgets
{
    public class HomeBanner
    {
        public string Title { get; set; }
        public string Summary { get; set; }
        public string VideoBackgroundUrl { get; set; }
        public int FallbackImage { get; set; }
        public int Link { get; set; }
        public int FeaturedNews { get; set; }
    }
}
