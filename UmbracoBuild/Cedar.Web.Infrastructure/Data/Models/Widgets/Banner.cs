﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Cedar.Web.Infrastructure.Data.Models.Widgets
{
    public class Banner
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string IntroText { get; set; }
        public int Image { get; set; }
        public string BackgroundPosition { get; set; }
        public int Logo { get; set; }
    }
}
