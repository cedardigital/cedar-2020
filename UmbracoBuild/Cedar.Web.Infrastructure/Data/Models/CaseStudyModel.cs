﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Cedar.Web.Infrastructure.Data.Models.Interface;
using Cedar.Web.Infrastructure.Data.Models.Supplemental;
using Cedar.Web.Infrastructure.Data.ViewModels;
using Umbraco.Web.Models;

namespace Cedar.Web.Infrastructure.Data.Models
{
    public class CaseStudyModel : BaseModel, ICanonical, IShareable
    {
        public string Title { get; set; }
        public IEnumerable<Award> Awards { get; set; }
        public IEnumerable<CaseStudy> RelatedCaseStudies { get; set; }
        public string CanonicalUrl { get; set; }
        public IEnumerable<Tag> Tags { get; set; }

        public CaseStudyModel(RenderModel model) : base(model) { }
    }
}
