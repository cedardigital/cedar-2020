﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Cedar.Web.Infrastructure.Data.Models.Interface;
using Cedar.Web.Infrastructure.Data.Models.Supplemental;
using Cedar.Web.Infrastructure.Data.ViewModels;
using Umbraco.Web.Models;

namespace Cedar.Web.Infrastructure.Data.Models
{
    public class ArticleModel : BaseModel, ICanonical, IShareable
    {
        public string Title { get; set; }
        public WidgetVm CTA { get; set; }
        public SiteContainer PrimarySite { get; set; }
        public Person Author { get; set; }
        public bool DisplayAuthorLink { get; set; }
        public IEnumerable<Article> RelatedArticles { get; set; }
        public string CanonicalUrl { get; set; }
        public IEnumerable<Tag> Tags { get; set; }
        public String BackgroundPosition { get; set; }

        public ArticleModel(RenderModel model) : base(model) { }
    }
}
