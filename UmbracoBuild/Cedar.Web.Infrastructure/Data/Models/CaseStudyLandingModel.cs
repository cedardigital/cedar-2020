﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Cedar.Web.Infrastructure.Data.Models.Interface;
using Cedar.Web.Infrastructure.Data.ViewModels;
using Umbraco.Core.Models;
using Umbraco.Web.Models;

namespace Cedar.Web.Infrastructure.Data.Models
{
    public class CaseStudyLandingModel : BaseModel, IWidgets
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Description { get; set; }
        public IList<WidgetVm> Widgets { get; set; }
        public WidgetVm CTA { get; set; }

        public CaseStudyLandingModel(RenderModel model) : base(model) { }
    }
}
