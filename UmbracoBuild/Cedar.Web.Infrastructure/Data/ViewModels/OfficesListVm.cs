﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cedar.Web.Infrastructure.Data.Models;
using Cedar.Web.Infrastructure.Data.Models.Supplemental;
using Umbraco.Core.Models;
using Umbraco.Web.Models;

namespace Cedar.Web.Infrastructure.Data.ViewModels
{
    public class OfficesListVm
    {
        public string Title { get; set; }
        public IEnumerable<Office> Offices { get; set; }
    }
}
