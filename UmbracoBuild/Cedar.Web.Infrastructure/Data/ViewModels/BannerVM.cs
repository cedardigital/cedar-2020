﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cedar.Web.Infrastructure.Data.ViewModels
{
    public class BannerVm
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string IntroText { get; set; }
        public string Image { get; set; }
        public string MobileImage { get; set; }
        public string BackgroundPosition { get; set; }
        public dynamic Logo { get; set; }
    }
}
