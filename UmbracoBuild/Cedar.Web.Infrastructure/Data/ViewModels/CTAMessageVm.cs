﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cedar.Web.Infrastructure.Data.Models.Supplemental;

namespace Cedar.Web.Infrastructure.Data.ViewModels
{
    public class CtaMessageUsVm
    {
        [Required]
        [DisplayName("Name")]
        public string Name { get; set; }
        [Required]
        [DisplayName("Job Title")]
        public string JobTitle { get; set; }
        [Required]
        [DisplayName("Email")]
        public string Email { get; set; }
        [Required]
        [DisplayName("Message")]
        public string Message { get; set; }
    }
}
