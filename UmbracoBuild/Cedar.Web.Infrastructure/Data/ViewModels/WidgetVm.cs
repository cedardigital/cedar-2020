﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cedar.Web.Infrastructure.Data.ViewModels
{
    public class WidgetVm
    {
        public string MacroAlias { get; set; }
        public int DataSourceId { get; set; }
    }
}
