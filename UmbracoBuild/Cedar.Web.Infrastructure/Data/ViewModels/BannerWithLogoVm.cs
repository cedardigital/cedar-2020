﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cedar.Web.Infrastructure.Data.ViewModels
{
    public class BannerWithLogoVm
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public dynamic Image { get; set; }
        public dynamic Logo { get; set; }
    }
}
