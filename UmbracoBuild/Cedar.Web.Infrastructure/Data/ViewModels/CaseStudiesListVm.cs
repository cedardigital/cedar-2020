﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cedar.Web.Infrastructure.Data.Models;
using Cedar.Web.Infrastructure.Data.Models.Supplemental;
using Umbraco.Core.Models;
using Umbraco.Web.Models;

namespace Cedar.Web.Infrastructure.Data.ViewModels
{
    public class CaseStudiesListVm
    {
        public string Title { get; set; }
        public IEnumerable<CaseStudy> CaseStudies { get; set; }
        public dynamic Link { get; set; }
        public string LinkTitle { get; set; }
    }
}
