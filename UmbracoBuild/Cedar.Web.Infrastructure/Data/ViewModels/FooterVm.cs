﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cedar.Web.Infrastructure.Data.Models.Folder;
using Cedar.Web.Infrastructure.Data.Models.Supplemental;

namespace Cedar.Web.Infrastructure.Data.ViewModels
{
    public class FooterVm
    {
        public IEnumerable<Office> Offices { get; set; }
        public IEnumerable<InternalLink> TopLinks { get; set; }
        public IEnumerable<InternalLink> BottomLinks { get; set; }
        public List<ExternalLink> SocialLinks { get; set; }
    }
}
