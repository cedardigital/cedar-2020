﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cedar.Web.Infrastructure.Data.ViewModels
{
    public class FullWidthImageVm
    {
        public dynamic Image { get; set; }
        public string Caption { get; set; }
    }
}
