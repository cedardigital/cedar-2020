﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cedar.Web.Infrastructure.Data.Models.Supplemental;

namespace Cedar.Web.Infrastructure.Data.ViewModels
{
    public class CtaFollowVm
    {
        public List<ExternalLink> SocialLinks { get; set; }
    }
}
