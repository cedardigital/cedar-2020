﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace Cedar.Web.Infrastructure.Data.ViewModels
{
    public class HomeBannerVm
    {
        public string Title { get; set; }
        public string Summary { get; set; }
        public string VideoBackgroundUrl { get; set; }
        public string FallbackImage { get; set; }
        public string FallbackImageMobile { get; set; }
        public IPublishedContent Link { get; set; }
        public IPublishedContent FeaturedNews { get; set; }
        public bool DisplayVideo { get; set; }
    }
}
