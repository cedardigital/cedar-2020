﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cedar.Web.Infrastructure.Data.Models.Supplemental;

namespace Cedar.Web.Infrastructure.Data.ViewModels
{
    public class HeaderVm
    {
        public string SiteTitle { get; set; }
        public List<SiteContainer> Sites { get; set; }
        public List<InternalLink> HeaderLinks { get; set; }
        public List<ExternalLink> SocialLinks { get; set; }
        public string LocalizedHomeUrl { get; set; }
    }
}
