﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace Cedar.Web.Infrastructure.Data.ViewModels
{
    public class BaseVm
    {
        private UmbracoHelper _helper;

        public UmbracoContext UmbracoContext
        {
            get
            {
                return UmbracoContext.Current;
            }
        }
        public ApplicationContext ApplicationContext
        {
            get
            {
                return this.UmbracoContext.Application;
            }
        }

        public UmbracoHelper UmbracoHelpr
        {
            get
            {
                if (this._helper == null)
                {
                    this._helper = new UmbracoHelper(this.UmbracoContext);
                }
                return this._helper;
            }
        }
    }
}
