﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using umbraco;
using umbraco.NodeFactory;
using Umbraco.Web;
using Umbraco.Web.Routing;

namespace Cedar.Web.Infrastructure.Helpers
{
    public class ArticleUrlProvider : IUrlProvider
    {
        public string GetUrl(UmbracoContext umbracoContext, int id, Uri current, UrlProviderMode mode)
        {
            var content = umbracoContext.ContentCache.GetById(id);

            if (content != null && content.DocumentTypeAlias == "Article")
            {
                var allLandingNodes = uQuery.GetNodesByType("NewsLanding").OrderByDescending(n => n.Level);
                return (from landing in allLandingNodes 
                        let url = landing.GetFullNiceUrl()
                        where landing.GetFullNiceUrl().StartsWith(current.Host, StringComparison.InvariantCultureIgnoreCase) 
                        select landing.Url + content.UrlName).FirstOrDefault();
            }
            
            if (content != null && content.DocumentTypeAlias == "CaseStudy")
            {
                var allLandingNodes = uQuery.GetNodesByType("CaseStudyLanding").OrderByDescending(n => n.Level);
                return (from landing in allLandingNodes
                        let url = landing.GetFullNiceUrl()
                        where landing.GetFullNiceUrl().StartsWith(current.Host, StringComparison.InvariantCultureIgnoreCase)
                        select landing.Url + content.UrlName).FirstOrDefault();
            }


            if (content != null && content.DocumentTypeAlias == "Service")
            {
                var allLandingNodes = uQuery.GetNodesByType("ServicesLanding").OrderByDescending(n => n.Level);
                return (from landing in allLandingNodes
                        let url = landing.GetFullNiceUrl()
                        where landing.GetFullNiceUrl().StartsWith(current.Host, StringComparison.InvariantCultureIgnoreCase)
                        select landing.Url + content.UrlName).FirstOrDefault();
            }

            return null;
        }

        public IEnumerable<string> GetOtherUrls(UmbracoContext umbracoContext, int id, Uri current)
        {
            return Enumerable.Empty<string>();
        }
    }
}
