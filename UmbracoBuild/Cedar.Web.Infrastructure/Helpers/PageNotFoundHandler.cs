﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Xml;
using Cedar.UmbracoExtensions.Extensions;
using Cedar.Web.Infrastructure.Data.Models;
using Cedar.Web.Infrastructure.Data.Models.Interface;
using Cedar.Web.Infrastructure.Data.Models.Supplemental;
using Umbraco.Core.Models;
using umbraco.interfaces;
using umbraco.MacroEngines;
using Umbraco.Web;

namespace Cedar.Web.Infrastructure.Helpers
{
    public class PageNotFoundHandler : INotFoundHandler
    {
        public bool Execute(string url)
        {
            var home = NodeHelper.GetHomePage();
            if (home == null) return false;

            var notFoundPage = home.Children().FirstOrDefault(x => x.DocumentTypeAlias.Equals("Error404"));
            if (notFoundPage == null) return false;

            redirectID = notFoundPage.Id;
            return true;
        }

        public bool CacheUrl
        {
            get { return false; }
        }

        public int redirectID { get; private set; }
    }
}
