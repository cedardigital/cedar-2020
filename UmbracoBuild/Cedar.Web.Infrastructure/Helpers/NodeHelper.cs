﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Xml;
using Cedar.UmbracoExtensions.Extensions;
using Cedar.Web.Infrastructure.Data.Models;
using Cedar.Web.Infrastructure.Data.Models.Interface;
using Cedar.Web.Infrastructure.Data.Models.Supplemental;
using Umbraco.Core.Models;
using umbraco.MacroEngines;
using Umbraco.Web;

namespace Cedar.Web.Infrastructure.Helpers
{
    public class NodeHelper
    {
        public static IPublishedContent GetCurrentSite()
        {
            return GetHomePage().Parent;
        }
        /*temporty fix to the yellow screen*/
        public static string LogoLocalizedUrl(IPublishedContent currentPage)
        {
            var page = currentPage.AncestorsOrSelf(2).FirstOrDefault().Children.Where(m => m.DocumentTypeAlias == "HomePage");
            string currentUrl = string.Empty;
            if (page.Any())
            {
                 currentUrl = currentPage.AncestorsOrSelf(2).FirstOrDefault().Children.Where(m => m.DocumentTypeAlias == "HomePage").LastOrDefault().Url;
            }
            else
            {
                var helper = new UmbracoHelper(UmbracoContext.Current);
                if ((!currentUrl.Any()) || currentUrl == null)
                {
                    currentUrl = helper.TypedContentAtRoot().DescendantsOrSelf("HomePage").FirstOrDefault().Url;
                }
            }
            return currentUrl;
        }
      
        public static IPublishedContent GetHomePage()
        {
            if (UmbracoContext.Current.PublishedContentRequest != null && UmbracoContext.Current.PublishedContentRequest.PublishedContent != null)
            {
                var homepage = UmbracoContext.Current.PublishedContentRequest.PublishedContent.AncestorOrSelf("HomePage");
                if (homepage != null)
                {
                    return homepage;
                }
            }

            foreach (var site in GetSites())
            {
                var siteHome = site.Children().First(x => x.DocumentTypeAlias.Equals("HomePage"));
                var domains = umbraco.library.GetCurrentDomains(siteHome.Id);
                if (domains.Any(x => x.Name.Contains(HttpContext.Current.Request.Url.Host)))
                {
                    return siteHome;
                }
            }

            return null;
        }

        public static IEnumerable<IPublishedContent> GetSites()
        {
            var helper = new UmbracoHelper(UmbracoContext.Current);
            return helper.TypedContent(1057).Children(x => x.DocumentTypeAlias.Equals("SiteContainer"));
        }

        public static IPublishedContent GetPageElements()
        {
            return GetHomePage().Siblings().First(x => x.DocumentTypeAlias.Equals("PageElements"));
        }

        public static IPublishedContent GetNavigation()
        {
            return GetPageElements().Children().First(x => x.DocumentTypeAlias.Equals("Navigation"));
        }

        public static IPublishedContent GetHeaderNavigation()
        {
            return GetNavigation().Children().First(x => x.DocumentTypeAlias.Equals("NavigationHeader"));
        }

        public static IPublishedContent GetFooterNavigation()
        {
            return GetNavigation().Children().First(x => x.DocumentTypeAlias.Equals("NavigationFooter"));
        }

        public static IPublishedContent GetSocialElements()
        {
            return GetPageElements().Children().First(x => x.DocumentTypeAlias.Equals("SocialElements"));
        }

        public static IOrderedEnumerable<Article> GetAllArticles()
        {
            var helper = new UmbracoHelper(UmbracoContext.Current);
            var articles = helper.TypedContent(1351).Children(x => x.DocumentTypeAlias.Equals("Article"));
            return articles.Select(a => a.As<Article>()).OrderByDescending(x => x.DateCreated);
        }

        public static IOrderedEnumerable<CaseStudy> GetAllCaseStudies()
        {
            var helper = new UmbracoHelper(UmbracoContext.Current);
            var articles = helper.TypedContent(1354).Children(x => x.DocumentTypeAlias.Equals("CaseStudy"));
            return articles.Select(a => a.As<CaseStudy>()).OrderByDescending(x => x.DateCreated);
        }

        public static IEnumerable<Award> GetAllAwards()
        {
            var helper = new UmbracoHelper(UmbracoContext.Current);
            var articles = helper.TypedContent(1078).Children(x => x.DocumentTypeAlias.Equals("Award"));
            return articles.Select(a => a.As<Award>());
        }

        public static IEnumerable<T> GetMultiNodePickerItems<T>(string csv)
        {
            var helper = new UmbracoHelper(UmbracoContext.Current);
            var nodes = csv.Split(new [] { "," }, StringSplitOptions.RemoveEmptyEntries);
            return helper.TypedContent(nodes).Where(x => x != null).Select(content => content.As<T>());
        }

        public static string GetCanonicalUrl(BaseModel content)
        {
            foreach (var site in GetSites())
            {
                if (site.Id != content.CurrentPage.PrimarySite) continue;

                var siteHome = site.Children().First(x => x.DocumentTypeAlias.Equals("HomePage"));
                return HttpContext.Current.Request.Url.Scheme + Uri.SchemeDelimiter + umbraco.library.GetCurrentDomains(siteHome.Id).First().Name + content.CurrentPage.Url;
            }
            return string.Empty;
        }

        public static string GetPropertyValue(string parameter, string text)
        {
            var parameterIndex = text.IndexOf(parameter, StringComparison.Ordinal);
            if (parameterIndex != -1)
            {
                var startIndex = text.IndexOf("\"", parameterIndex, StringComparison.Ordinal);
                var endIndex = text.IndexOf("\"", startIndex + 1, StringComparison.Ordinal);

                var parameterValue = text.Substring(startIndex + 1, endIndex - startIndex - 1);
                return parameterValue;
            }
            return string.Empty;
        }
    }
}
