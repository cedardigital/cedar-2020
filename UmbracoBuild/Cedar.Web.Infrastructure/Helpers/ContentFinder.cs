﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cedar.UmbracoExtensions.Extensions;
using Cedar.Web.Infrastructure.Data.Models.Supplemental;
using umbraco;
using Umbraco.Web;
using Umbraco.Web.Routing;

namespace Cedar.Web.Infrastructure.Helpers
{
    public class ContentFinder : IContentFinder
    {
        public bool TryFindContent(PublishedContentRequest contentRequest)
        {
            var fullUri = contentRequest.Uri.AbsolutePath;
            var helper = new UmbracoHelper(UmbracoContext.Current);
            try
            {
                #region Find Article Content
                var allNewsLandingNodes = uQuery.GetNodesByType("NewsLanding").OrderByDescending(n => n.Level);
                foreach (var newsLanding in allNewsLandingNodes)
                {
                    // check the current url against landing url to see if it is a child
                    if (!fullUri.StartsWith(newsLanding.Url, StringComparison.InvariantCultureIgnoreCase)) continue;

                    // get all articles 
                    var articles = NodeHelper.GetAllArticles();

                    // get the url name of the current page
                    var urlName = fullUri.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Last();

                    // make sure the url name matches a article in the listing
                    if (!articles.Any(x => x.UrlName.Equals(urlName, StringComparison.CurrentCultureIgnoreCase))) continue;

                    // finally, we have a match - set the content on the current request to the matched article
                    var articleId = articles.First(x => x.UrlName.Equals(urlName, StringComparison.CurrentCultureIgnoreCase)).Id;
                    contentRequest.PublishedContent = helper.TypedContent(articleId);

                    return true;
                }
                #endregion

                #region Find Case Study Content
                var allCaseStudyLandingNodes = uQuery.GetNodesByType("CaseStudyLanding").OrderByDescending(n => n.Level);
                foreach (var caseStudyLanding in allCaseStudyLandingNodes)
                {
                    // check the current url against landing url to see if it is a child
                    if (!fullUri.StartsWith(caseStudyLanding.Url, StringComparison.InvariantCultureIgnoreCase)) continue;

                    // get all articles 
                    var caseStudies = NodeHelper.GetAllCaseStudies();

                    // get the url name of the current page
                    var urlName = fullUri.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Last();

                    // make sure the url name matches a case study in the listing
                    if (!caseStudies.Any(x => x.UrlName.Equals(urlName, StringComparison.CurrentCultureIgnoreCase))) continue;

                    // finally, we have a match - set the content on the current request to the matched case study
                    var caseStudyId = caseStudies.First(x => x.UrlName.Equals(urlName, StringComparison.CurrentCultureIgnoreCase)).Id;
                    contentRequest.PublishedContent = helper.TypedContent(caseStudyId);

                    return true;
                }
                #endregion

                #region Find Service Content
                var allServiceLandingNodes = uQuery.GetNodesByType("ServicesLanding").OrderByDescending(n => n.Level);
                foreach (var serviceLanding in allServiceLandingNodes)
                {
                    // check the current url against landing url to see if it is a child
                    if (!fullUri.StartsWith(serviceLanding.Url, StringComparison.InvariantCultureIgnoreCase)) continue;

                    // we have a match - load the landing and get the services added to this page
                    var model = helper.TypedContent(serviceLanding.Id);
                    var services = NodeHelper.GetMultiNodePickerItems<Service>(model.GetPropertyValue("Services").ToString()).ToList();

                    // get the url name of the current page
                    var urlName = fullUri.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).Last();

                    // make sure the url name matches a service in the listing
                    if (!services.Any(x => x.UrlName.Equals(urlName, StringComparison.CurrentCultureIgnoreCase))) continue;

                    // finally, we have a match - set the content on the current request to the matched service
                    var serviceId = services.First(x => x.UrlName.Equals(urlName, StringComparison.CurrentCultureIgnoreCase)).Id;
                    contentRequest.PublishedContent = helper.TypedContent(serviceId);

                    return true;
                }
                #endregion
            }
            catch (System.Threading.ThreadAbortException ex)
            {
                // do nothing
            }

            return false;
        }
    }
}
