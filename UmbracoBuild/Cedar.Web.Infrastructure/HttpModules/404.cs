﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Cedar.Web.Infrastructure.HttpModules
{
    class DCX404Module : IHttpModule
    {
        public void Init(HttpApplication application)
        {
            application.Error += application_Error;
            application.EndRequest += application_EndRequest;
        }

        void application_Error(object sender, EventArgs e)
        {
            var ex = HttpContext.Current.Server.GetLastError() as HttpException;

            if (ex != null && ex.GetHttpCode() == 404)
            {
                NotFoundHandler();
            }
        }

        protected bool IsDevEnvironment => Debugger.IsAttached;

        void application_EndRequest(object sender, EventArgs e)
        {
            if (!IsDevEnvironment)
                return;

            var application = HttpContext.Current;

            // GET IMAGE FROM LIVE SERVER
            switch (HttpContext.Current.Response.StatusCode)
            {
                case 404:
                case 400:
                    {
                        NotFoundHandler();
                    }
                    break;
            }
        }

        public virtual void Dispose()
        {

        }

        private static bool IsMedia(string requestPath)
        {
            return (requestPath.EndsWith(".jpg", StringComparison.CurrentCultureIgnoreCase) || requestPath.EndsWith(".gif", StringComparison.CurrentCultureIgnoreCase) || requestPath.EndsWith(".png", StringComparison.CurrentCultureIgnoreCase) || requestPath.EndsWith(".svg", StringComparison.CurrentCultureIgnoreCase));
        }


        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        private class MyWebClient : WebClient
        {
            protected override void OnDownloadProgressChanged(DownloadProgressChangedEventArgs e)
            {
                base.OnDownloadProgressChanged(e);
            }
            protected override WebRequest GetWebRequest(Uri uri)
            {
                var w = base.GetWebRequest(uri);
                w.Timeout = 1000;
                return w;
            }
        }

        private object syncObject = new object();

        public string CreateDirectories(string basePhysicalPath, string[] pathElements)
        {
            basePhysicalPath = basePhysicalPath.TrimEnd("\\".ToCharArray());

            var directoryCreate = "";
            var directoryPhysicalPath = "";
            var directoryElements = pathElements.Skip(1).Take(pathElements.Length - 2).ToList();

            foreach (var directory in directoryElements)
            {
                directoryCreate = string.Format("{0}\\{1}", directoryCreate, directory);
                directoryPhysicalPath = string.Format("{0}{1}", basePhysicalPath, directoryCreate);// application.Server.MapPath(string.Concat("~", directoryCreate));

                lock (syncObject)
                {
                    if (!Directory.Exists(directoryPhysicalPath))
                        Directory.CreateDirectory(directoryPhysicalPath);
                }
            }

            return directoryPhysicalPath;
        }

        private void NotFoundHandler()
        {
            var requestPath = HttpContext.Current.Request.Url.LocalPath;

            #region DEV_DOWNLOADIMAGEFROMLIVE
            if (IsDevEnvironment)
            {
                if (IsMedia(requestPath))
                {
                    var liveRequest = string.Format("http://www.cedarcom.co.uk/{0}", requestPath);


                    var dstFilename = Path.GetFullPath(Path.Combine(Path.Combine(AssemblyDirectory, @"..\\" + requestPath.Replace("/", "\\"))));// string.Concat(Path.GetFullPath(Path.Combine(AssemblyDirectory, @"..\\Media\images\")), requestPath.Split('/').Last());

                    CreateDirectories(Path.GetFullPath(AssemblyDirectory + @"..\\..\\"), requestPath.Split('/'));

                    var httpReq = new MyWebClient();

                    try
                    {
                        httpReq.DownloadFile(liveRequest, dstFilename);
                        HttpContext.Current.Response.Redirect(requestPath);
                    }
                    catch (Exception ex2)
                    {
                        //TescoRealFood.BusinessLogic.Utilities.LogRealFoodError(ex2, "ImageRetrieve");
                    }
                }
            }
            #endregion DEV_DOWNLOADIMAGEFROMLIVE

        }
    }
}
