﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cedar.UmbracoExtensions.Extensions;
using Cedar.Web.Infrastructure.Data.Models.Widgets;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace Cedar.Web.Infrastructure.DataAccess
{
    public class ComponentsDao
    {
        public T GetComponent<T>(int dataSourceId)
        {
            return UmbracoContext.Current.ContentCache.GetById(dataSourceId).As<T>();
        }
    }
}
