﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cedar.UmbracoExtensions.Extensions;
using Umbraco.Core.Models;
using Umbraco.Web;
using Cedar.Web.Infrastructure.Data.Models.JsonResult;
using Cedar.Web.Infrastructure.Data.Models.Widgets;
using Cedar.Web.Infrastructure.Data.ViewModels;
using Cedar.Web.Infrastructure.DataAccess;
using Cedar.Web.Infrastructure.Extensions;

namespace Cedar.Web.Infrastructure.Services
{
    public class ComponentsService
    {
        readonly ComponentsDao _componentsDao = new ComponentsDao();

        public HomeBannerVm GetHomeBanner(int dataSourceId)
        {
            var componentViewModel = _componentsDao.GetComponent<HomeBanner>(dataSourceId);
            return componentViewModel.Map();
        }

        public BannerVm GetBanner(int dataSourceId)
        {
            var componentViewModel = _componentsDao.GetComponent<Banner>(dataSourceId);
            return componentViewModel.Map();
        }

        //public BannerWithLogoVm GetBannerWithLogo(int dataSourceId)
        //{
        //    var componentViewModel = _componentsDao.GetComponent<BannerWithLogo>(dataSourceId);
        //    return componentViewModel.Map();
        //}

        public AwardsListVm GetAwardsList(int dataSourceId)
        {
            var componentViewModel = _componentsDao.GetComponent<AwardsList>(dataSourceId);
            return componentViewModel.Map();
        }

        public CaseStudiesListVm GetCaseStudiesList(int dataSourceId)
        {
            var componentViewModel = _componentsDao.GetComponent<CaseStudiesList>(dataSourceId);
            return componentViewModel.Map();
        }

        public ClientsListVm GetClientsList(int dataSourceId)
        {
            var componentViewModel = _componentsDao.GetComponent<ClientsList>(dataSourceId);
            return componentViewModel.Map();
        }

        public FullWidthImageVm GetFullWidthImage(int image, string caption)
        {
            var componentViewModel = new FullWidthImage
            {
                Image = image,
                Caption = caption
            };
            return componentViewModel.Map();
        }

        public GridContentVm GetGridContent(int dataSourceId, string framework)
        {
            return new GridContentVm { Content = UmbracoContext.Current.ContentCache.GetById(dataSourceId), Framework = framework };
        }

        public JobsListVm GetJobsList(int dataSourceId)
        {
            var componentViewModel = _componentsDao.GetComponent<JobsList>(dataSourceId);
            return componentViewModel.Map();
        }

        public MediaListVm GetMediaList(int dataSourceId)
        {
            var componentViewModel = _componentsDao.GetComponent<MediaList>(dataSourceId);
            return componentViewModel.Map();
        }

        public RichTextContentVm GetRichTextContent(int dataSourceId)
        {
            var componentViewModel = _componentsDao.GetComponent<RichTextContent>(dataSourceId);
            return componentViewModel.Map();
        }

        public WhitePapersListVm GetWhitePapersList(int dataSourceId)
        {
            var componentViewModel = _componentsDao.GetComponent<WhitePapersList>(dataSourceId);
            return componentViewModel.Map();
        }

        public OfficesListVm GetOfficesList(int dataSourceId)
        {
            var componentViewModel = _componentsDao.GetComponent<OfficesList>(dataSourceId);
            return componentViewModel.Map();
        }

        public PeopleListVm GetPeopleList(int dataSourceId)
        {
            var componentViewModel = _componentsDao.GetComponent<PeopleList>(dataSourceId);
            return componentViewModel.Map();
        }

        public IEnumerable<PersonJson> GetPeopleListJson(int dataSourceId)
        {
            var componentViewModel = _componentsDao.GetComponent<PeopleList>(dataSourceId);
            return componentViewModel.MapToJson();
        }

        public TechnologiesListVm GetTechnologiesList(int dataSourceId)
        {
            var componentViewModel = _componentsDao.GetComponent<TechnologiesList>(dataSourceId);
            return componentViewModel.Map();
        }

        public TweetsListVm GetTweetsList(int dataSourceId)
        {
            var componentViewModel = _componentsDao.GetComponent<TweetsList>(dataSourceId);
            return componentViewModel.Map();
        }

        //public ServicesListVm GetServicesList(int dataSourceId)
        //{
        //    var componentViewModel = _componentsDao.GetComponent<ServicesList>(dataSourceId);
        //    return componentViewModel.Map();
        //}

        public IEnumerable<ServiceJson> GetServicesListJson(int dataSourceId)
        {
            var componentViewModel = _componentsDao.GetComponent<ServicesList>(dataSourceId);
            return componentViewModel.MapToJson();
        }

        public CarouselVm GetCarousel(int dataSourceId)
        {
            var componentViewModel = _componentsDao.GetComponent<Carousel>(dataSourceId);
            return componentViewModel.Map();
        }

        public IEnumerable<CarouselItemJson> GetCarouselJson(int dataSourceId)
        {
            var componentViewModel = _componentsDao.GetComponent<Carousel>(dataSourceId);
            return componentViewModel.MapToJson();
        }

        public CaseStudyLandingJson GetCaseStudyLandingJson(int dataSourceId)
        {
            var componentViewModel = _componentsDao.GetComponent<CaseStudyLanding>(dataSourceId);
            return componentViewModel.MapToJson();
        }

        public NewsLandingJson GetNewsLandingJson(int dataSourceId)
        {
            var componentViewModel = _componentsDao.GetComponent<NewsLanding>(dataSourceId);
            return componentViewModel.MapToJson();
        }
    }
}
