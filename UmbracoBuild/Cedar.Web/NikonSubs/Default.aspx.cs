using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using ApplicationLogic;
using System.Linq;

namespace NikonSubscriptionForm
{
    public partial class Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            creditCardNumberField.Attributes.Add("alt", "cc");

            expiryDateYearSelect.Items.Clear();
            expiryDateYearSelect.DataSource = Enumerable.Range(DateTime.Now.Year, 11);
            expiryDateYearSelect.DataBind();
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            if (!IsPostBack) return;
            if (!IsValid) return;
            SubscriptionForm form = SubscriptionFormFactory.Get(Request.Form);
            if (form.Country != "en_GB") form.County = string.Empty;
            try
            {
                using (SubscriptionEmail email = new SubscriptionEmail(form))
                {
                    email.Send();
                }
                successMessage.Visible = true;
            }
            catch (Exception exception)
            {
                if (ConfigurationReaderFactory.Get().IsDebug)
                    exceptionMessage.Text = exception.ToString();
                failMessage.Visible = true;
            }
            finally
            {
                form1.Visible = false;
            }
            base.OnLoadComplete(e);
        }
        
        public override void Validate()
        {
            bool validateCreditCard = int.Parse(paymentMethodSelect.Value) == (int)PaymentMethods.CreditCard;
            creditCardNumberFieldReq.Enabled = validateCreditCard;
            cardHoldersNameFieldReq.Enabled = validateCreditCard;
            expiryDateValid.Enabled = validateCreditCard;
            ccvNumberValid.Enabled = validateCreditCard;
            ccvNumberRequired.Enabled = validateCreditCard;
            countyRequired.Enabled = Request.Form["countrySelect"] == "en_GB";
            base.Validate();
        }

        protected void privacyPolicyCheckReq_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = privacyPolicyCheck.Checked;
        }

        protected void expiryDateValid_ServerValidate(object source, ServerValidateEventArgs args)
        {
            DateTime expiryDate = GetExpiryDate();
            DateTime now = DateTime.Now;
            args.IsValid = expiryDate.Year > now.Year || (expiryDate.Month >= now.Month && expiryDate.Year >= now.Year);
        }

        private DateTime GetExpiryDate()
        {
            // int expiryYear = int.Parse(expiryDateYearSelect.Value);
            int expiryYear = int.Parse(Request.Form[expiryDateYearSelect.ClientID]);
            int expiryMonth = int.Parse(expiryDateMonthSelect.Value);
            return new DateTime(expiryYear, expiryMonth, 1);
        }

        protected void languageRequiredServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = !string.IsNullOrEmpty(magazineLanguageSelect.Value);
        }

        protected void countryRequiredServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = !string.IsNullOrEmpty(Request.Form["countrySelect"]);
        }

        protected void countyRequiredServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = !string.IsNullOrEmpty(countySelect.Value);
        }
    }
}
