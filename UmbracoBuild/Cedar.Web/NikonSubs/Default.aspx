﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="NikonSubscriptionForm.Default" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Nikon Pro Subscription Form</title>
    <link href="css/screen.css" rel="stylesheet" type="text/css" media="screen" />    
	<link href="fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
	<div id="wrapper">
    	<div id="container">
            <div id="header">
            	<a href="#" class="logo">
                	<img src="images/logo-nikon-pro.gif" width="239" height="61" alt="Nikon Pro logo" />
                </a>
                <h1>Subscription Form</h1>
            </div>
            <div class="leftCol">
                <asp:PlaceHolder runat="server" Visible="false">
                    <h2><strong>Page undergoing maintenance</strong></h2>    
                    <p>The Nikon Pro subscription page is currently undergoing essential maintenance. </p>
                    <p>Please check back soon when the service will be restored.</p>				
                </asp:PlaceHolder>

                <asp:PlaceHolder runat="server" visible="true">
            	    <h2><strong>Nikon Pro</strong> Direct to your door</h2>
                    <p>For just <strong>&pound;18*a year </strong>you can get the Spring, Summer and Winter editions of the award winning <strong>Nikon Pro magazine delivered direct to your door</strong>. You'll get a whole year's worth of news, product reviews, advice and support, award winning journalism, competitions and the best photographs from Nikon professionals all over the world.</p>
                    <p>Nikon Pro is available in English, French, Italian and German and is already enjoyed by 75,000 photographers and enthusiasts worldwide. <strong>Just complete the subscription form below and we'll do the rest</strong>.</p>
                    <p>Your subscription will start with the next available issue.</p>
                    <p>You can now get your favourite magazine as a tablet edition for both iPad and Android devices. All your favourite features but with added exclusive content. Available to download from the <a href="https://itunes.apple.com/gb/app/nikon-pro/id479875337" target="_blank">App Store</a> now. 
                    </p>
                    <p><strong>FREE NIKON PRO WILD – NATURE SPECIAL EDITION: DOWNLOAD NOW!</strong>
                        <br/>This special tablet edition is themed around Wildlife and is free of charge from the App Store. Designed to give newcomers a taste of what the full Nikon Pro magazine has to offer, the WILD version celebrates awe-inspiring photography and the stories behind the image, just as the regular Nikon Pro does.  
                        To get your copy simply download the Nikon Pro app and click on the “Nikon Pro WILD” icon.
                    </p>
                    <p class="smaller"><i>Nikon Pro is published on behalf of Nikon Europe BV by Cedar Communications Ltd</i></p>

                    <asp:Panel runat="server" ID="successMessage" Visible="false">
                        <p>Your request for a Nikon Pro subscription was successfully sent.</p>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="failMessage" Visible="false">
                		    <p>There was a problem sending your Nikon Pro subscription. Please contact us on <br /><span class="highlight">+44 (0) 207 550 8000</span> or email <a href="mailto:nikonprosubs@cedarcom.co.uk">nikonprosubs@cedarcom.co.uk</a></p>
                		    <p><asp:Literal runat="server" ID="exceptionMessage" /></p>
                    </asp:Panel>
                    <form runat="server" id="form1"> 
                        <p class="smaller">All Fields are mandatory</p>
                	    <asp:ValidationSummary runat="server" ID="errorMessage" ForeColor="" HeaderText="<p>Please fill out the following fields:</p>" DisplayMode="BulletList" />
                	    <fieldset>
                	        <label for="titleSelect">Title</label>
                            <select id="titleSelect" class="titleSelect" name="titleSelect">
                        	    <option value="Mr" selected="selected">Mr.</option>
                                <option value="Mrs">Mrs.</option>
                                <option value="Ms">Ms.</option>
                            </select>
                            <label for="<%# firstNameField.ClientID %>" class="clear">First Name</label>
                            <asp:TextBox runat="server" ID="firstNameField" />
                            <asp:RequiredFieldValidator runat="server" ID="firstNameFieldReq" ControlToValidate="firstNameField" ErrorMessage="First Name" Display="None" ForeColor="" />
                            <label for="<%# lastNameField.ClientID %>">Last Name</label>
                            <asp:TextBox runat="server" ID="lastNameField" />
                            <asp:RequiredFieldValidator runat="server" ID="lastNameFieldReq" ControlToValidate="lastNameField" ErrorMessage="Last Name" Display="None" ForeColor="" />
                        </fieldset>
                        <fieldset>
                            <label for="<%# emailField.ClientID %>">Email</label>
                            <asp:TextBox runat="server" ID="emailField" />
                            <asp:RequiredFieldValidator runat="server" ID="emailFieldReq" ControlToValidate="emailField" ErrorMessage="Email" Display="None" ForeColor="" />
                            <asp:RegularExpressionValidator runat="server" ID="emailFieldValid" ControlToValidate="emailField" ErrorMessage="Email - Please enter a valid email address" Display="None" ForeColor="" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                            <label for="<%# phoneNumberField.ClientID %>">Telephone Number</label>
                            <asp:TextBox runat="server" ID="phoneNumberField" />
                            <asp:RequiredFieldValidator runat="server" ID="phoneNumberFieldReq" ControlToValidate="phoneNumberField" ErrorMessage="Telephone Number" Display="None" ForeColor="" />
                            <label for="<%# addressLine1Field.ClientID %>">Address</label>
                            <asp:TextBox runat="server" ID="addressLine1Field" />
                            <asp:RequiredFieldValidator runat="server" ID="addressLine1FieldReq" ControlToValidate="addressLine1Field" ErrorMessage="Address" Display="None" ForeColor="" />
                            <input name="addressLine2Field" id="addressLine2Field" class="addressLine2" />
                            <label for="<%# cityField.ClientID %>">City</label>
                            <asp:TextBox runat="server" ID="cityField" />
                            <asp:RequiredFieldValidator runat="server" ID="cityFieldReq" ControlToValidate="cityField" ErrorMessage="City" Display="None" ForeColor="" />
                            <label for="countySelect">County <span>(UK only)</span></label>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <select runat="server" name="countySelect" id="countySelect">
                            <option value="">Please select</option>
                        	<option>Aberdeenshire</option>
                            <option>Anglesey/Sir Fon</option>
                            <option>Angus/Forfarshire</option>
                            <option>Antrim</option>
                            <option>Argyllshire</option>
                            <option>Armagh</option>
                            <option>Ayrshire</option>
                            <option>Banffshire</option>
                            <option>Bedfordshire</option>
                            <option>Berkshire</option>
                            <option>Berwickshire</option>
                            <option>Brecknocshir/Sir Frycheiniog</option>
                            <option>Bristol</option>
                            <option>Buckinghamshire</option>
                            <option>Buteshire</option>
                            <option>Caernarfonshire/Sir Gaernarfon</option>
                            <option>Caithness</option>
                            <option>Cambridgeshire</option>
                            <option>Cardiganshire/Ceredigion</option>
                            <option>Carmarthenshire/Sir Gaerfyrddin</option>
                            <option>Cheshire</option>
                            <option>Clackmannanshire</option>
                            <option>Clwyd</option>
                            <option>Cornwall</option>
                            <option>Cromartyshire</option>
                            <option>Cumberland</option>
                            <option>Cumbria</option>
                            <option>Denbighshire/Sir Ddinbych</option>
                            <option>Derbyshire</option>
                            <option>Devon</option>
                            <option>Dorset</option>
                            <option>Down</option>
                            <option>Dumfriesshire</option>
                            <option>Dunbartonshire/Dumbartonshire</option>
                            <option>Durham</option>
                            <option>Dyfed</option>
                            <option>East Lothian/Haddingtonshire</option>
                            <option>East Sussex</option>
                            <option>East Yorkshire</option>
                            <option>Essex</option>
                            <option>Fermanagh</option>
                            <option>Fife</option>
                            <option>Flintshire/Sir Fflint</option>
                            <option>Glamorgan/Morgannwg</option>
                            <option>Gloucestershire</option>
                            <option>Greater London</option>
                            <option>Greater Manchester</option>
                            <option>Gwent</option>
                            <option>Gwynedd</option>
                            <option>Hampshire</option>
                            <option>Herefordshire</option>
                            <option>Hertfordshire</option>
                            <option>Huntingdonshire</option>
                            <option>Inverness-shire</option>
                            <option>Isle of Wight</option>
                            <option>Kent</option>
                            <option>Kincardineshire</option>
                            <option>Kinross-shire</option>
                            <option>Kirkcudbrightshire</option>
                            <option>Lanarkshire</option>
                            <option>Lancashire</option>
                            <option>Leicestershire</option>
                            <option>Lincolnshire</option>
                            <option>London</option>
                            <option>Londonderry</option>
                            <option>Merioneth/Merionnydd</option>
                            <option>Merseyside</option>
                            <option>Middlesex</option>
                            <option>Midlothian/Edingburghshire</option>
                            <option>Monmouthshire/Sir Fynwy</option>
                            <option>Montgomeryshire/Sir Drefaldwyn</option>
                            <option>Morayshire</option>
                            <option>Nairnshire</option>
                            <option>Norfolk</option>
                            <option>North Yorkshire</option>
                            <option>North Hamptonshire</option>
                            <option>Northumberland</option>
                            <option>Nottinghamshire</option>
                            <option>Orkney</option>
                            <option>Oxfordshire</option>
                            <option>Peeblessshire</option>
                            <option>Pembrokeshire/Sir Benfro</option>
                            <option>Perthshire</option>
                            <option>Powys</option>
                            <option>Radnorshire/Sir Faesyfed</option>
                            <option>Renfrewshire</option>
                            <option>Ross-shire</option>
                            <option>Roxburghshire</option>
                            <option>Rutland</option>
                            <option>Selkirkshire</option>
                            <option>Shetland</option>
                            <option>Shrospshire</option>
                            <option>Somerset</option>
                            <option>South Yorkshire</option>
                            <option>Staffordshire</option>
                            <option>Stirlingshire</option>
                            <option>Suffolk</option>
                            <option>Surrey</option>
                            <option>Sutherland</option>
                            <option>Tyne &amp; Wear</option>
                            <option>Tyrone</option>
                            <option>Warwickshire</option>
                            <option>West Lothian//Linlithgowshire</option>
                            <option>West Midlands</option>
                            <option>West Sussex</option>
                            <option>West Yorkshire</option>
                            <option>Westmorland</option>
                            <option>Wigtownshire</option>
                            <option>Wiltshire</option>
                            <option>Worcestershire</option>
                            <option>Yorkshire</option>
                        </select>
                            <asp:CustomValidator runat="server" ID="countyRequired" ErrorMessage="County" Display="None" ForeColor="" OnServerValidate="countyRequiredServerValidate" />
                            <label for="<%# postcodeField.ClientID %>" class="clearLeft">Postcode</label>
                            <asp:TextBox runat="server" ID="postcodeField" />
                            <asp:RequiredFieldValidator runat="server" ID="postcodeFieldReq" ControlToValidate="postcodeField" ErrorMessage="Postcode" Display="None" ForeColor="" />
                            <label for="countrySelect" class="clear">Country</label>
                                                                                                                                                                                        <select name="countrySelect" id="countrySelect">
                            <option value="">Please select</option>
                        	<optgroup label="European countries">
                                <option value="en_GB">UK and Ireland</option>
                                <option value="fr_BE">Belgique</option>
                                <option value="nl_BE">Belgi&euml;</option>
                                <option value="da_DK">Danmark</option>
                                <option value="de_DE">Deutschland</option>										
                                <option value="et_EE">Eesti</option>
                                <option value="es_ES">Espa&ntilde;a</option>
                                <option value="fr_FR">France</option>
                                <option value="it_IT">Italia</option>										
                                <option value="lv_LV">Latvija</option>
                                <option value="lt_LT">Lietuva</option>
                                <option value="hu_HU">Magyarorsz&aacute;g</option>
                                <option value="nl_NL">Nederland</option>
                                <option value="no_NO">Norge</option>
                                <option value="pl_PL">Polska</option>
                                <option value="pt_PT">Portugal</option>
                                <option value="ro_RO">Romania</option>
                                <option value="de_CH">Schweiz</option>
                                <option value="fr_CH">Suisse</option>
                                <option value="sl_SL">Slovenija</option>
                                <option value="fi_FI">Suomi</option>
                                <option value="sv_SE">Sverige</option>
                                <option value="en_GB">UK and Ireland</option>
                                <option value="de_AT">&ouml;sterreich</option>
                                <option value="cs_CZ">Česká republika</option>
                                <option value="el_GR">Ελλάδα</option>
                                <option value="ru_RU">Россия</option>
                            </optgroup>
                            <optgroup label="African countries">
                            	<option value="en_ZA">South Africa</option>
                            </optgroup>
                            <optgroup label="Other regions">	
                                <option value="usa">The Americas</option>
                                <option value="jp_JP">Japan</option>
                                <option value="oceania">Oceania</option>
                            </optgroup>
                        </select>
                            <asp:CustomValidator runat="server" ID="countryRequired" ErrorMessage="Country" Display="None" ForeColor="" OnServerValidate="countryRequiredServerValidate" />
						    <label for="magazineLanguageSelect" class="clearLeft">Nikon Pro Language</label>
                            <select runat="server" name="magazineLanguageSelect" id="magazineLanguageSelect">
                                <option value="">Please select</option>
                        	    <option>English</option> 
                                <option>German</option>
                                <option>French</option>
                                <option>Italian</option>
                    	    </select>
                            <asp:CustomValidator runat="server" ID="languageRequired" ErrorMessage="Nikon Pro Language" Display="None" ForeColor="" OnServerValidate="languageRequiredServerValidate" />
                        </fieldset>
                        <fieldset>
                    	    <label for="<%# paymentMethodSelect.ClientID %>">Payment Method</label>
                            <select runat="server" id="paymentMethodSelect">
                        	    <option value="0">Credit/Debit Card</option>
                                <option value="1">Cheque</option>
                                <option value="2">BACS</option>
                    	    </select>
                            <fieldset class="creditCardDetails clear">
                        	    <label for="creditCardTypeSelect">Credit Card Type</label>
                                <select name="creditCardTypeSelect" id="creditCardTypeSelect">
                                    <option>Visa/Delta/Electron</option>
                                    <option>MasterCard/EuroCard</option>
                                    <option>Solo/Maestro</option>
                                </select>
                                <label for="<%# creditCardNumberField.ClientID %>" class="clear">Credit Card Number</label>
                                <asp:TextBox runat="server" ID="creditCardNumberField" MaxLength="16" />
                                <asp:RequiredFieldValidator runat="server" ID="creditCardNumberFieldReq" ControlToValidate="creditCardNumberField" ErrorMessage="Credit Card Number" Display="None" ForeColor="" />
                                <label for="<%# cardHoldersNameField.ClientID %>" class="clear">Cardholder's Name <em>(as it appears on the credit card)</em></label>
                                <asp:TextBox runat="server" ID="cardHoldersNameField" CssClass="cardHolderName" />
                                <asp:RequiredFieldValidator runat="server" ID="cardHoldersNameFieldReq" ControlToValidate="cardHoldersNameField" ErrorMessage="Cardholder's Name" Display="None" ForeColor="" />
                             
                                <label for="<%# issueNoField.ClientID %>" class="clear">Issue No / Start Date <em>Switch/Solo only: If your card does not have an issue number please enter the start date (mm/yy)</em></label>
                                <asp:TextBox runat="server" ID="issueNoField" CssClass="issueNo" />
                                <label for="<%# expiryDateMonthSelect.ClientID %>" class="clear">Expiry Date</label>
                                <select runat="server" id="expiryDateMonthSelect" name="expiryDateMonthSelect" class="expiryMonth">
                                    <option value="1">January</option>
                                    <option value="2">February</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>

                                <select runat="server" id="expiryDateYearSelect" name="expiryDateYearSelect" class="expiryYear">
                                </select>

                                <asp:CustomValidator runat="server" ID="expiryDateValid" ErrorMessage="Expiry Date - Please enter a valid expiry date" Display="None" ForeColor="" OnServerValidate="expiryDateValid_ServerValidate" />
                                <label for="<%# ccvNumber.ClientID %>" class="ccvNumber">CCV Number <em><a href="#">What is this? <cite>The CCV (Credit Card Validation) number is a 3 digit code printed on the BACK of your card in the signature panel. If there are more than 3 digits, the CCV is always the LAST 3, as shown to the right. This number provides an additional check on the validity of the card.</cite></a></em></label>
                                <asp:TextBox runat="server" ID="ccvNumber" CssClass="ccvNumber" MaxLength="3" />
                                <asp:RequiredFieldValidator runat="server" ID="ccvNumberRequired" ControlToValidate="ccvNumber" ErrorMessage="CCV number required" Display="None" ForeColor="" />             
                                <asp:RegularExpressionValidator runat="server" ID="ccvNumberValid" ControlToValidate="ccvNumber" ErrorMessage="CCV number must be three digits" Display="None" ForeColor="" ValidationExpression="\d{3}" />
                            </fieldset>
                        </fieldset>
                        <fieldset>                    	
                            <label class="clear">Subscription length</label>
                            <div class="subscriptionLength">
                                <input class="smallInput" type="radio" id="lengthOneYear" name="subscriptionLengthOption" value="1" checked="checked" />
                                <label class="smallLabel" for="lengthOneYear">One year, 25&euro;/&pound;18</label>
                                <input class="smallInput" type="radio" id="lengthTwoYears" name="subscriptionLengthOption" value="2" />
                                <label class="smallLabel" for="lengthTwoYears">Two years, 50&euro;/&pound;36</label>
                            </div>   
                            <label class="clear">Opt-in</label>
                            <input runat="server" class="smallInput" type="checkbox" id="optInCheck" />
                            <label class="smallLabel" for="<%# optInCheck.ClientID %>">Receive latest news, events and information about Nikon products</label>
                            <label class="clear">Privacy Policy</label>
                            <input runat="server" class="smallInput" type="checkbox" id="privacyPolicyCheck" />
                            <asp:CustomValidator runat="server" ID="privacyPolicyCheckReq" ErrorMessage="Privacy Policy"  Display="None" ForeColor="" OnServerValidate="privacyPolicyCheckReq_ServerValidate" />
                            <label class="smallLabel" for="<%# privacyPolicyCheck.ClientID %>">I have read and agree to <a href="http://www.europe-nikon.com/footer/en_GB/footer/broad/privacy.html" rel="external">Nikon's Privacy Policy</a></label>
                        </fieldset>
                        <fieldset class="submit">
						    <p class="beforeSubmit"><strong>Before you submit, have you...</strong></p>
						    <ul class="pointers">	
							    <li>Selected the correct language version?</li>
							    <li>Subscribed for 1 or 2 years?</li>
							    <li>Input all of your address, telephone and email details?</li>
						    </ul>
                    	    <p>By subscribing to Nikon Pro you accept our Terms &amp; Conditions outlined below</p>
                		    <input class="submitForm" type="submit" id="submit" />
                        </fieldset>
                    </form>
                    <div class="terms">				
					    <p>*Credit/debit card payments will be charged in £ sterling. One year subscription is £18.00. Two years subscription is £36.00 or the converted rates in Euro (€). Payment will appear on your debit/credit card statement as Cedar Communications Ltd/HSBC.</p>
					    <p>BACS and cheque payments can be made in sterling for £18 (if you’re based in the UK) or for €22 for  European transactions outside of the UK. Please note that if you are not resident within Europe we can only accept payment by credit or debit card. </p>
					    <p>It may take up to 30 days to process your request/queries. You will receive a confirmation email when payment has been processed. Your subscription will then begin with the next available issue of the magazine. Nikon Pro is published three times a year, Spring, Summer and Winter.</p>
                        <p>If you have any questions, please don't hesitate to contact the Nikon Pro subscriptions department on +44 (0) 207 550 8000 or <a href="mailto:nikonprosubs@cedarcom.co.uk">nikonprosubs@cedarcom.co.uk</a></p>
                        <h4>Terms &amp; Conditions</h4>
                        <p>Nikon Pro magazine is published three times a year by Cedar Communications Ltd. A single year subscription will provide three issues of Nikon Pro magazine and two years will provide six issues. In the event that Nikon Pro magazines is discontinued, Cedar Communications Ltd will provide a refund for any issues not mailed for the remainder of the subscription term. If the frequency of Nikon Pro magazine is changed to be either more or less frequent, Cedar Communications Ltd will notify you in writing and give you the option of increasing or reducing your subscription accordingly.</p>
                        <p>You can cancel your subscription to Nikon Pro magazine at any time during its term, once the first magazine in your subscription period has been sent. A refund of any un-mailed magazines will be sent by cheque. Please note that cancellations need to be advised in writing and will only become active once you have received written confirmation that your cancellation notification has been received.</p>
                    </div>

                </asp:PlaceHolder>

            </div>
            <div class="rightCol">
                
                 <a href="images/magazine1.jpg" rel="gallery" class="fancygallery"><img src="images/gallery-thumb.jpg" /><span>See inside the magazine &raquo;</span></a> 
				 <a href="https://itunes.apple.com/gb/app/nikon-pro/id479875337" target="blank" class="ipad"><img src="images/app-store.gif" /></a> 
				 <a href="https://play.google.com/store/apps/details?id=gomobile.nikon&hl=en_GB" target="blank" class="googlePlay"><img src="images/google-play.gif" /></a> 

            	<div class="rightColPanel panelHelp">
            	      
                    <div style="display:none;"> 
					    <a href="images/magazine2.jpg" rel="gallery" class="fancygallery"></a> 
					    <a href="images/magazine3.jpg" rel="gallery" class="fancygallery"></a> 
					    <a href="images/magazine4.jpg" rel="gallery" class="fancygallery"></a> 
					    <a href="images/magazine5.jpg" rel="gallery" class="fancygallery"></a> 
					    <a href="images/magazine6.jpg" rel="gallery" class="fancygallery"></a> 
					    <a href="images/magazine7.jpg" rel="gallery" class="fancygallery"></a> 
					    <a href="images/magazine8.jpg" rel="gallery" class="fancygallery"></a> 
					    <a href="images/magazine9.jpg" rel="gallery" class="fancygallery"></a> 
				    </div> 

                    <h3>Need help?</h3>
                    <p>If you have any queries regarding subscriptions to Nikon Pro please contact us on <br /><span class="highlight">+44 (0) 207 550 8000</span> or email <a href="mailto:nikonprosubs@cedarcom.co.uk">nikonprosubs@cedarcom.co.uk</a></p>
                    <p class="smallest">Support is only available in English</p>
                </div>
                <div class="rightColPanel panelPayment">
                	<h3>Payment methods</h3>
                    <p>We take the following methods of payment:</p>
                    <ul>
                    	<li><span>Credit/Debit card <em>(not Amex)</em></span><br />Please note this is a non-transactional website. Your details will be sent to Cedar Communications Ltd via <strong>128 bit secure encrypted email</strong>. </li>
                        <li><span>Cheque</span><br />Please send a cheque made payable to Cedar Communications Ltd to Nikon Pro Subscriptions, Cedar Communications Ltd, 85 Strand, London, WC2R 0DW.</li>
                        <li><span>BACS</span><br />Please call Nikon Pro Subscription Dept on +44 (0) 207 550 8000 to arrange a bank transfer.</li>
                    </ul>
                </div>

            </div>
        </div>
    </div>  
    <script language="javascript" type="text/javascript" src="scripts/jquery-1.7.1.min.js"></script>
    <script language="javascript" type="text/javascript" src="scripts/jquery.meio.mask.min.js"></script>
	<script type="text/javascript" src="fancybox/jquery.fancybox-1.3.4.js"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
			$(".fancygallery").fancybox();
			
            $('A[rel="external"]').click(function () {
                window.open($(this).attr('href'));
                return false;
            });
            if ($("#paymentMethodSelect").val() != 0)
                $(".creditCardDetails").hide("slow");
            else
                $(".creditCardDetails").show("slow");
            $("#paymentMethodSelect").change(function () {
                if ($(this).val() != 0)
                    $(".creditCardDetails").hide("slow");
                else
                    $(".creditCardDetails").show("slow");
            });
            $('input:text').setMask();

            var selectedCountry = '<%= Request.Form["countrySelect"] ?? "" %>';
            
            if (selectedCountry.length > 0) 
            {
                // in case of validation error, set the selected option of the country dropdown
                $("#countrySelect").val(selectedCountry);
            }

        });

    </script>
</body>
</html>