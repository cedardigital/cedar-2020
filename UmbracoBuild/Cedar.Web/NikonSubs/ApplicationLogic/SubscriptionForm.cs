using System.Text;

namespace ApplicationLogic
{
    public class SubscriptionForm
    {
        private string title;
        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        private string firstName;
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        private string lastName;
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        private string email;
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        private string phoneNumber;
        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { phoneNumber = value; }
        }

        private string addressLine1;
        public string AddressLine1
        {
            get { return addressLine1; }
            set { addressLine1 = value; }
        }

        private string addressLine2;
        public string AddressLine2
        {
            get { return addressLine2; }
            set { addressLine2 = value; }
        }

        private string county;
        public string County
        {
            get { return county; }
            set { county = value; }
        }

        private string postcode;
        public string Postcode
        {
            get { return postcode; }
            set { postcode = value; }
        }

        private string country;
        public string Country
        {
            get { return country; }
            set { country = value; }
        }

        private IPayment paymentDetails;
        public IPayment PaymentDetails
        {
            get { return paymentDetails; }
            set { paymentDetails = value; }
        }

        private string magazineLanguage;
        public string MagazineLanguage
        {
            get { return magazineLanguage; }
            set { magazineLanguage = value; }
        }

        private SubscriptionLength subscriptionLength;
        public SubscriptionLength SubscriptionLength
        {
            get { return subscriptionLength; }
            set { subscriptionLength = value; }
        }

        private bool privacyPolicy;
        public bool PrivacyPolicy
        {
            get { return privacyPolicy; }
            set { privacyPolicy = value; }
        }

        private bool receiveMarketing;
        public bool ReceiveMarketing
        {
            get { return receiveMarketing; }
            set { receiveMarketing = value; }
        }

        private string city;
        public string City
        {
            get { return city; }
            set { city = value; }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Content-Type: text/plain;charset=\"iso-8859-1\"\nContent-Transfer-Encoding: quoted-printable\r\n\r\n");
            sb.Append("Request to subscribe to Nikon Pro magazine.\n\n");
            sb.AppendFormat("Name: {0} {1} {2}\n", title, firstName, lastName);
            sb.AppendFormat("Email: {0}\n", email);
            sb.AppendFormat("Telephone Number: {0}\n", phoneNumber);
            sb.AppendFormat("Address:\n");
            sb.AppendFormat("\tAddress Line 1: {0}\n", addressLine1);
            if (!string.IsNullOrEmpty(addressLine2))
                sb.AppendFormat("\tAddress Line 2: {0}\n", addressLine2);
            sb.AppendFormat("\tCity: {0}\n", city);
            if (country == "en_GB")
                sb.AppendFormat("\tCounty: {0}\n", county);
            sb.AppendFormat("\tPostcode: {0}\n", postcode);
            sb.AppendFormat("\tCountry: {0}\n", country);
            sb.Append(paymentDetails);
            sb.AppendFormat("Subscription Length: {0} year{1}\n", (int)subscriptionLength, subscriptionLength == SubscriptionLength.TwoYears ? "s" : string.Empty);
            sb.AppendFormat("Read and agreed to the privacy policy: {0}\n", privacyPolicy ? "YES" : "NO");
            sb.AppendFormat("Receive latest news, events and information about Nikon products: {0}",
                            receiveMarketing ? "YES" : "NO");
            return sb.ToString();
        }
    }
}
