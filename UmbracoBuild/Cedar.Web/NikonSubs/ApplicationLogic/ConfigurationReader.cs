using System;
using System.Configuration;

namespace ApplicationLogic
{
    public class ConfigurationReader
    {
        public string SubscriptionEmailSubject
        {
            get { return GetStringSetting("SubscriptionEmailSubject"); }
        }

        public string SubscriptionEmailToAddress
        {
            get { return GetStringSetting("SubscriptionEmailToAddress"); }
        }

        public bool IsDebug
        {
            get { return GetBoolSetting("IsDebug", false, false); }
        }

        public string PublicCertificateFileLocation
        {
            get { return GetStringSetting("PublicCertificateFileLocation"); }
        }

        /// <summary>
        /// Name or IP address of the SMTP mail host
        /// </summary>
        public string SmtpHost
        {
            get { return GetStringSetting("SmtpHost"); }
        }

        /// <summary>
        /// User name for SMTP authentication
        /// </summary>
        public string SmtpUser
        {
            get { return GetStringSetting("SmtpUser"); }
        }

        /// <summary>
        /// Password for SMTP authentication
        /// </summary>
        public string SmtpPassword
        {
            get { return GetStringSetting("SmtpPassword"); }
        }
        /// <summary>
        /// Return a configuration setting, or an empty string if it's not present
        /// </summary>
        /// <param name="key">Key of the configuration setting</param>
        /// <param name="allowOverrides">Whether or not to allow overriden settings</param>
        protected string GetStringSetting(string key, bool allowOverrides)
        {
            return GetStringSetting(key, String.Empty, allowOverrides);
        }

        /// <summary>
        /// Return a configuration setting, or an empty string if it's not present
        /// </summary>
        protected string GetStringSetting(string key)
        {
            return GetStringSetting(key, String.Empty);
        }

        /// <summary>
        /// Return a configuration setting of type T, or the default value for type T
        /// if the setting is not present. 
        /// </summary>
        protected T GetSetting<T>(string key)
            where T : struct, IConvertible
        {
            return GetSetting(key, default(T), true);
        }

        /// <summary>
        /// Return a configuration setting of type T, or the default value for type T
        /// if the setting is not present. Also specify whether to allow overriden 
        /// settings
        /// </summary>
        protected T GetSetting<T>(string key, bool allowOverrides)
            where T : struct, IConvertible
        {
            return GetSetting(key, default(T), allowOverrides);
        }

        /// <summary>
        /// Return a configuration setting string, or a default value 
        /// if it's not present
        /// </summary>
        protected string GetStringSetting(string key, string defaultValue)
        {
            return GetStringSetting(key, defaultValue, true);
        }

        /// <summary>
        /// Return a configuration setting of type T, or a default value 
        /// if it's not present
        /// </summary>
        protected T GetSetting<T>(string key, T defaultValue)
            where T : struct, IConvertible
        {
            return GetSetting(key, defaultValue, true);
        }

        /// <summary>
        /// Return a configuration setting of type T, or a default value 
        /// if it's not present. Also specify whether to allow overriden settings
        /// </summary>
        protected T GetSetting<T>(string key, T defaultValue, bool allowOverrides)
            where T : struct, IConvertible
        {
            if (typeof(T).Equals(typeof(bool)))
            {
                return (T)Convert.ChangeType(GetBoolSetting(key, Convert.ToBoolean(defaultValue), allowOverrides), typeof(T));
            }
            string value = GetStringSetting(key, defaultValue.ToString());
            try
            {
                return (T)Convert.ChangeType(value, typeof(T));
            }
            catch
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Override suffix which allows overridden settings to be used. Inheriting classes must 
        /// define the override suffix. For example, a web site could use the server name as an
        /// override suffix. Then you could have separate "ConnectionString" and "ConnectionString.localhost"
        /// settings, and the appropriate one would be read by this class's ConnectionString Property.
        /// </summary>
        protected virtual string OverrideSuffix
        {
            get { return String.Empty; }
        }

        /// <summary>
        /// Get a boolean from a setting. Values can be sored as true/false, on/off, yes/no, 1/0.
        /// </summary>
        /// <param name="key">Key of the setting to retrieve</param>
        /// <param name="defaultValue">Default value, true or false</param>
        /// <param name="allowOverrides">Whether to allow overriden settings</param>
        /// <returns>true or false</returns>
        private bool GetBoolSetting(string key, bool defaultValue, bool allowOverrides)
        {
            string str = GetStringSetting(key, defaultValue.ToString(), allowOverrides).ToLower();

            switch (str)
            {
                case "yes":
                case "on":
                case "true":
                case "1":
                case "-1":
                    return true;

                case "no":
                case "off":
                case "false":
                case "0":
                    return false;

                default:
                    return defaultValue;
            }
        }

        /// <summary>
        /// Return a setting from the config file. 
        /// </summary>
        /// <param name="key">Key of the setting to retrieve</param>
        /// <param name="defaultValue">Default value to return if key is not present</param>
        /// <param name="allowOverrides">Whether to allow overriden settings</param>
        private string GetStringSetting(string key, string defaultValue, bool allowOverrides)
        {
            try
            {
                string value = null;

                if (allowOverrides)
                {
                    value = ConfigurationManager.AppSettings[key + OverrideSuffix];
                }

                if (value == null)
                {
                    value = ConfigurationManager.AppSettings[key] ?? defaultValue;
                }

                return value;
            }
            catch
            {
                return defaultValue;
            }
        }
    }
}
