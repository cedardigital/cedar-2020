namespace ApplicationLogic
{
    public enum PaymentMethods
    {
        Empty = -1,
        CreditCard,
        Cheque,
        Bacs
    }
}
