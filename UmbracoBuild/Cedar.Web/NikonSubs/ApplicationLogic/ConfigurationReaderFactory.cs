namespace ApplicationLogic
{
    public static class ConfigurationReaderFactory
    {
        private static ConfigurationReader configuration;

        public static ConfigurationReader Get()
        {
            if (configuration == null)
                configuration = new ConfigurationReader();
            return configuration;
        }
    }
}
