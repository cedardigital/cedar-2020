using System;
using System.Collections.Generic;

namespace ApplicationLogic
{
    public static class PaymentFactory
    {
        public static IPayment Get(PaymentMethods paymentMethod, List<KeyValuePair<string, string>> collection)
        {
            switch (paymentMethod)
            {
                case PaymentMethods.Bacs:
                    return new Bacs();
                case PaymentMethods.Cheque:
                    return new Cheque();
                case PaymentMethods.CreditCard:
                    CreditCard creditCard = new CreditCard();
                    int expiryDateMonth = DateTime.MinValue.Month;
                    int expiryDateYear = DateTime.MinValue.Year;
                    foreach(KeyValuePair<string, string> keyValue in collection)
                    {
                        switch (keyValue.Key)
                        {
                            case "creditCardTypeSelect":
                                creditCard.CreditCardType = keyValue.Value;
                                break;
                            case "creditCardNumberField":
                                creditCard.CreditCardNumber = keyValue.Value;
                                break;
                            case "cardHoldersNameField":
                                creditCard.CardHoldersName = keyValue.Value;
                                break;
                            case "issueNoField":
                                creditCard.IssueNumber = keyValue.Value;
                                break;
                            case "expiryDateMonthSelect":
                                expiryDateMonth = int.Parse(keyValue.Value);
                                break;
                            case "expiryDateYearSelect":
                                expiryDateYear = int.Parse(keyValue.Value);
                                break;
                            case "ccvNumber":
                                creditCard.CcvNumber = keyValue.Value;
                                break;
                            default:
                                break;

                        }
                    } 
                    creditCard.ExpiryDate = new DateTime(expiryDateYear, expiryDateMonth, 1);
                    return creditCard;
                default:
                    return null;
            }
        }
    }
}