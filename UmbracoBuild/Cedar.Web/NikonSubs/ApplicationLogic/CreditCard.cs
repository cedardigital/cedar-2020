using System;
using System.Text;

namespace ApplicationLogic
{
    public class CreditCard : IPayment
    {
        private string creditCardType;
        public string CreditCardType
        {
            get { return creditCardType; }
            set { creditCardType = value; }
        }

        private string creditCardNumber;
        public string CreditCardNumber
        {
            get { return creditCardNumber; }
            set { creditCardNumber = value; }
        }

        private string cardHoldersName;
        public string CardHoldersName
        {
            get { return cardHoldersName; }
            set { cardHoldersName = value; }
        }

        private string issueNumber;
        public string IssueNumber
        {
            get { return issueNumber; }
            set { issueNumber = value; }
        }

        private string ccvNumber;
        public string CcvNumber
        {
            get { return ccvNumber; }
            set { ccvNumber = value; }
        }

        private DateTime expiryDate;
        public DateTime ExpiryDate
        {
            get { return expiryDate; }
            set { expiryDate = value; }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Payment Method: Credit Card\n");
            sb.AppendFormat("\tCredit Card Type: {0}\n", creditCardType);
            sb.AppendFormat("\tCredit Card Number: {0}\n", creditCardNumber);
            sb.AppendFormat("\tCard Holder's Name: {0}\n", cardHoldersName);
            sb.AppendFormat("\tCredit Card CCV Number: {0}\n", ccvNumber);
            sb.AppendFormat("\tIssue Number: {0}\n", issueNumber);
            sb.AppendFormat("\tExpiry Date: {0:MM/yy}\n", expiryDate);
            return sb.ToString();
        }
    }
}