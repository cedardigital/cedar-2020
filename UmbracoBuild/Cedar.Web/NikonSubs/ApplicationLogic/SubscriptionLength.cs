namespace ApplicationLogic
{
    public enum SubscriptionLength
    {
        OneYear = 1,
        TwoYears = 2
    }
}