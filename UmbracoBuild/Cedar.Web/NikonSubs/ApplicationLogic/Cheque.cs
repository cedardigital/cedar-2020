namespace ApplicationLogic
{
    public class Cheque : IPayment
    {
        public override string ToString()
        {
            return "Payment Method: Cheque\n";
        }
    }
}