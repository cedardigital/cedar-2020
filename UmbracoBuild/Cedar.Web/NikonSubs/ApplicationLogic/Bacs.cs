namespace ApplicationLogic
{
    public class Bacs : IPayment
    {
        public override string ToString()
        {
            return "Payment Method: BACS\n";
        }
    }
}