using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace ApplicationLogic
{
    public static class SubscriptionFormFactory
    {
        public static SubscriptionForm Get(NameValueCollection collection)
        {
            SubscriptionForm form = new SubscriptionForm();
            PaymentMethods paymentMethod = PaymentMethods.Empty;
            List<KeyValuePair<string, string>> paymentCollection = new List<KeyValuePair<string, string>>();
            foreach (KeyValuePair<string, string> keyValue in ToKeyValuePairs(collection))
            {
                switch (keyValue.Key)
                {
                    case ("titleSelect"):
                        form.Title = keyValue.Value;
                        break;
                    case ("firstNameField"):
                        form.FirstName = keyValue.Value;
                        break;
                    case ("lastNameField"):
                        form.LastName = keyValue.Value;
                        break;
                    case ("emailField"):
                        form.Email = keyValue.Value;
                        break;
                    case ("phoneNumberField"):
                        form.PhoneNumber = keyValue.Value;
                        break;
                    case ("addressLine1Field"):
                        form.AddressLine1 = keyValue.Value;
                        break;
                    case ("addressLine2Field"):
                        form.AddressLine2 = keyValue.Value;
                        break;
                    case ("cityField"):
                        form.City = keyValue.Value;
                        break;
                    case ("postcodeField"):
                        form.Postcode = keyValue.Value;
                        break;
                    case ("countySelect"):
                        form.County = keyValue.Value;
                        break;
                    case ("countrySelect"):
                        form.Country = keyValue.Value;
                        break;
                    case ("paymentMethodSelect"):
                        paymentMethod = (PaymentMethods)Enum.Parse(typeof(PaymentMethods), keyValue.Value);
                        break;
                    case ("creditCardTypeSelect"):
                    case ("creditCardNumberField"):
                    case ("cardHoldersNameField"):
                    case ("issueNoField"):
                    case ("expiryDateMonthSelect"):
                    case ("expiryDateYearSelect"):
                    case ("ccvNumber"):
                        paymentCollection.Add(keyValue);
                        break;
                    case ("magazineLanguageSelect"):
                        form.MagazineLanguage = keyValue.Value;
                        break;
                    case ("subscriptionLengthOption"):
                        form.SubscriptionLength =
                            (SubscriptionLength)Enum.Parse(typeof(SubscriptionLength), keyValue.Value);
                        break;
                    case ("privacyPolicyCheck"):
                        form.PrivacyPolicy = keyValue.Value == "on";
                        break;
                    case ("optInCheck"):
                        form.ReceiveMarketing = keyValue.Value == "on";
                        break;
                    default:
                        break;
                }
            }
            form.PaymentDetails = PaymentFactory.Get(paymentMethod, paymentCollection);
            return form;
        }

        public static IEnumerable<KeyValuePair<string, string>> ToKeyValuePairs(NameValueCollection collection)
        {
            List<KeyValuePair<string, string>> keyValuePairs = new List<KeyValuePair<string, string>>();
            foreach (string key in collection.Keys)
            {
                keyValuePairs.Add(new KeyValuePair<string, string>(key, collection[key]));
            }
            return keyValuePairs;
        }
    }
}