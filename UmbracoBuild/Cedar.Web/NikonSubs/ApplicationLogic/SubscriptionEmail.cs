using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace ApplicationLogic
{
    public class SubscriptionEmail : IDisposable 
    {
        private readonly MailMessage email;
        private readonly ConfigurationReader configuration = ConfigurationReaderFactory.Get();

        public SubscriptionEmail(SubscriptionForm form)
        {
            var certificate = new X509Certificate2(configuration.PublicCertificateFileLocation);
            var data = Encoding.ASCII.GetBytes(form.ToString());

            var envelopedCms = new EnvelopedCms(new ContentInfo(data));

            var recipient = new CmsRecipient(SubjectIdentifierType.IssuerAndSerialNumber, certificate);
            envelopedCms.Encrypt(recipient);

            var encryptedData = envelopedCms.Encode();

            email = new MailMessage
            {
                From = new MailAddress(configuration.SmtpUser, string.Format("{0} {1}", form.FirstName, form.LastName)),
                Subject = configuration.SubscriptionEmailSubject
            };
            email.To.Add(new MailAddress(configuration.SubscriptionEmailToAddress));
            email.ReplyToList.Add(form.Email);

            var ms = new MemoryStream(encryptedData);
            var av = new AlternateView(ms, "application/pkcs7-mime; smime-type=enveloped-data;name=smime.p7m");
            email.AlternateViews.Add(av);
        }

        public void Send()
        {
            var smtpClient = new SmtpClient
            {
                Host = configuration.SmtpHost, 
                UseDefaultCredentials = true
            };
            if (!string.IsNullOrEmpty(configuration.SmtpPassword))
            {
                smtpClient.Credentials = new NetworkCredential(configuration.SmtpUser, configuration.SmtpPassword);
            }
            smtpClient.Send(email);
        }

        public void Dispose()
        {
            email.Dispose();
        }
    }
}
