﻿[
	{
	    "name": "Rich text editor",
	    "alias": "rte",
	    "view": "rte",
	    "icon": "icon-article"
	},
	{
	    "name": "Image",
	    "alias": "media",
	    "view": "media",
	    "icon": "icon-picture"
	},
	{
	    "name": "Macro",
	    "alias": "macro",
	    "view": "macro",
	    "icon": "icon-settings-alt"
	},
	{
	    "name": "Embed",
	    "alias": "embed",
	    "view": "embed",
	    "icon": "icon-movie-alt"
	},
    {
        "name": "Headline",
        "alias": "headline",
        "view": "textstring",
        "icon": "icon-coin",
        "config": {
            "style": "font-size: 36px; line-height: 45px; font-weight: bold",
            "markup": "<h2>#value#</h2>"
        }
    },
	{
	    "name": "Quote",
	    "alias": "quote",
	    "view": "/app_plugins/quote/quote.html",
	    "render": "/Views/Partials/Grid/Editors/Quote.cshtml",
	    "icon": "icon-quote",
	    "config": {
	        "style": "padding: 10px; color: #284d8a; font-size: 32px; line-height: 42px; text-align: center",
	        "sourcestyle": "color: grey; font-size: 22px; line-height: 42px; text-align: center",
	        "markup": "<blockquote class=\"no-mar\">#value#<footer>#source#</footer></blockquote>"
	    }
	},
	{
	    "name": "Stat",
	    "alias": "stat",
	    "view": "/app_plugins/stat/stat.html",
	    "render": "/Views/Partials/Grid/Editors/Stat.cshtml",
	    "icon": "icon-chart",
	    "config": {
	        "style": "padding: 0; color: #284d8a; font-size: 102px; line-height: 92px; font-weight: bold; letter-spacing: -4px;",
	        "descstyle": "padding: 0; color: #284d8a; font-size: 51px; line-height: 46px; font-weight: bold;",
	    }
	}
]