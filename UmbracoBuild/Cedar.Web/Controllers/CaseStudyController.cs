﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cedar.UmbracoExtensions.Extensions;
using Cedar.Web.Infrastructure.Data.Models;
using Cedar.Web.Infrastructure.Data.Models.Supplemental;
using Cedar.Web.Infrastructure.Helpers;
using Our.Umbraco.Vorto.Extensions;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace Cedar.Web.Controllers
{
    public class CaseStudyController : RenderMvcController
    {
        public override ActionResult Index(RenderModel model)
        {
            var awardsList = NodeHelper.GetAllAwards();
            var customModel = new CaseStudyModel(model)
            {
                Title = model.Content.GetVortoValue("Title") as string ?? string.Empty,
                Awards = awardsList.Where(x => x.CaseStudyLink == model.Content.Id),
                RelatedCaseStudies = NodeHelper.GetMultiNodePickerItems<CaseStudy>(model.Content.GetProperty("RelatedCaseStudies").Value.ToString()),
                Tags = NodeHelper.GetMultiNodePickerItems<Tag>(model.Content.GetProperty("Tags").Value.ToString()),
            };

            customModel.CanonicalUrl = NodeHelper.GetCanonicalUrl(customModel);
            customModel.FooterIncludes["React"] = true;
            customModel.FooterIncludes["Slick"] = true;

            return CurrentTemplate(customModel);
        }
    }
}