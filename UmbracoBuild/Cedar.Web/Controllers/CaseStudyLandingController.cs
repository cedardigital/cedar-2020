﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cedar.UmbracoExtensions.Extensions;
using Cedar.Web.Infrastructure.Data.Models;
using Our.Umbraco.Vorto.Extensions;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace Cedar.Web.Controllers
{
    public class CaseStudyLandingController : RenderMvcController
    {
        public override ActionResult Index(RenderModel model)
        {
            var customModel = new CaseStudyLandingModel(model)
            {
                Title = model.Content.GetVortoValue("Title") as string ?? string.Empty,
                SubTitle = model.Content.GetVortoValue("SubTitle") as string ?? string.Empty,
                Description = model.Content.GetVortoValue("Description") as string ?? string.Empty,
                Widgets = model.Content.GetWidgets("Widgets"),
                CTA = model.Content.GetWidget("CTA")
            };

            customModel.GetFooterIncludes();
            customModel.FooterIncludes["React"] = true;

            return CurrentTemplate(customModel);
        }
    }
}