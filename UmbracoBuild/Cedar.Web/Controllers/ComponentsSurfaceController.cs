﻿using System.Globalization;
using Cedar.Web.Infrastructure.Data.Models.JsonResult;
using Cedar.Web.Infrastructure.Data.Models.Supplemental;
using Cedar.Web.Infrastructure.Data.ViewModels;
using Cedar.Web.Infrastructure.Helpers;
using Cedar.Web.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace Cedar.Web.Controllers
{
    public class ComponentsSurfaceController : SurfaceController
    {
        readonly ComponentsService _componentsService = new ComponentsService();

        public ActionResult HomeBanner(int dataSourceId)
        {
            try
            {
                var component = _componentsService.GetHomeBanner(dataSourceId);
                return PartialView("~/Views/Partials/Shared/HomeBanner.cshtml", component);
            }
            catch (Exception e)
            {
                //If errror just return new, with no links, prevent from breaking every single page
                //_context.Logger.Error(e, typeof(ComponentsSurfaceController));
                return PartialView("~/Views/Partials/Shared/HomeBanner.cshtml", new HomeBannerVm());
            }
        }

        public ActionResult Banner(int dataSourceId)
        {
            try
            {
                var component = _componentsService.GetBanner(dataSourceId);
                return PartialView("~/Views/Partials/Shared/Banner.cshtml", component);
            }
            catch (Exception e)
            {
                //If errror just return new, with no links, prevent from breaking every single page
                //_context.Logger.Error(e, typeof(ComponentsSurfaceController));
                return PartialView("~/Views/Partials/Shared/Banner.cshtml", new BannerVm());
            }
        }

        //public ActionResult BannerWithLogo(int dataSourceId)
        //{
        //    try
        //    {
        //        var component = _componentsService.GetBannerWithLogo(dataSourceId);
        //        return PartialView("~/Views/Partials/Shared/BannerWithLogo.cshtml", component);
        //    }
        //    catch (Exception e)
        //    {
        //        //If errror just return new, with no links, prevent from breaking every single page
        //        //_context.Logger.Error(e, typeof(ComponentsSurfaceController));
        //        return PartialView("~/Views/Partials/Shared/BannerWithLogo.cshtml", new BannerWithLogoVm());
        //    }
        //}

        public ActionResult AwardsList(int dataSourceId)
        {
            try
            {
                var component = _componentsService.GetAwardsList(dataSourceId);
                return PartialView("~/Views/Partials/Shared/AwardsList.cshtml", component);
            }
            catch (Exception e)
            {
                //If errror just return new, with no links, prevent from breaking every single page
                //_context.Logger.Error(e, typeof(ComponentsSurfaceController));
                return PartialView("~/Views/Partials/Shared/AwardsList.cshtml", new AwardsListVm());
            }
        }

        public ActionResult CaseStudiesList(int dataSourceId)
        {
            try
            {
                var component = _componentsService.GetCaseStudiesList(dataSourceId);
                return PartialView("~/Views/Partials/Shared/CaseStudiesList.cshtml", component);
            }
            catch (Exception e)
            {
                //If errror just return new, with no links, prevent from breaking every single page
                //_context.Logger.Error(e, typeof(ComponentsSurfaceController));
                return PartialView("~/Views/Partials/Shared/CaseStudiesList.cshtml", new CaseStudiesListVm());
            }
        }

        public ActionResult ClientsList(int dataSourceId)
        {
            try
            {
                var component = _componentsService.GetClientsList(dataSourceId);
                return PartialView("~/Views/Partials/Shared/ClientsList.cshtml", component);
            }
            catch (Exception e)
            {
                //If errror just return new, with no links, prevent from breaking every single page
                //_context.Logger.Error(e, typeof(ComponentsSurfaceController));
                return PartialView("~/Views/Partials/Shared/ClientsList.cshtml", new ClientsListVm());
            }
        }

        public ActionResult CTAFollow()
        {
            try
            {
                var component = new CtaFollowVm();

                var socialLinks = NodeHelper.GetSocialElements().Children();
                component.SocialLinks = new List<ExternalLink>();
                foreach (var item in socialLinks)
                {
                    component.SocialLinks.Add(new ExternalLink
                    {
                        Name = item.Name,
                        Class = item["Class"].ToString(),
                        Url = item["LinkUrl"].ToString()
                    });
                }

                return PartialView("~/Views/Partials/Shared/CTAFollow.cshtml", component);
            }
            catch (Exception e)
            {
                //If errror just return new, with no links, prevent from breaking every single page
                //_context.Logger.Error(e, typeof(ComponentsSurfaceController));
                return PartialView("~/Views/Partials/Shared/CTAFollow.cshtml", new CtaFollowVm());
            }
        }

        public ActionResult CTAMessageUs()
        {
            return PartialView("~/Views/Partials/Shared/CTAMessageUs.cshtml", new CtaMessageUsVm());
        }

        public ActionResult CTANewsletter()
        {
            return PartialView("~/Views/Partials/Shared/CTANewsletter.cshtml");
        }

        public ActionResult FullWidthImage(int image, string caption)
        {
            try
            {
                var component = _componentsService.GetFullWidthImage(image, caption);
                return PartialView("~/Views/Partials/Shared/FullWidthImage.cshtml", component);
            }
            catch (Exception e)
            {
                //If errror just return new, with no links, prevent from breaking every single page
                //_context.Logger.Error(e, typeof(ComponentsSurfaceController));
                return PartialView("~/Views/Partials/Shared/FullWidthImage.cshtml", new FullWidthImageVm());
            }
        }

        public ActionResult GridContent(int dataSourceId, string docAlias = "")
        {
            try
            {
                // use an alternate grid framework for homepage content
                var framework = (docAlias == "HomePage") ? "HomePageGridContent" : "GridContent";

                var component = _componentsService.GetGridContent(dataSourceId, framework);
                return PartialView("~/Views/Partials/Shared/GridContent.cshtml", component);
            }
            catch (Exception e)
            {
                //If errror just return new, with no links, prevent from breaking every single page
                //_context.Logger.Error(e, typeof(ComponentsSurfaceController));
                return PartialView("~/Views/Partials/Shared/GridContent.cshtml", new GridContentVm());
            }
        }

        public ActionResult JobsList(int dataSourceId)
        {
            try
            {
                var component = _componentsService.GetJobsList(dataSourceId);
                return PartialView("~/Views/Partials/Shared/JobsList.cshtml", component);
            }
            catch (Exception e)
            {
                //If errror just return new, with no links, prevent from breaking every single page
                //_context.Logger.Error(e, typeof(ComponentsSurfaceController));
                return PartialView("~/Views/Partials/Shared/JobsList.cshtml", new JobsListVm());
            }
        }

        public ActionResult MediaList(int dataSourceId)
        {
            try
            {
                var component = _componentsService.GetMediaList(dataSourceId);
                return PartialView("~/Views/Partials/Shared/MediaList.cshtml", component);
            }
            catch (Exception e)
            {
                //If errror just return new, with no links, prevent from breaking every single page
                //_context.Logger.Error(e, typeof(ComponentsSurfaceController));
                return PartialView("~/Views/Partials/Shared/MediaList.cshtml", new MediaListVm());
            }
        }

        public ActionResult RichTextContent(int dataSourceId)
        {
            try
            {
                var component = _componentsService.GetRichTextContent(dataSourceId);
                return PartialView("~/Views/Partials/Shared/RichTextContent.cshtml", component);
            }
            catch (Exception e)
            {
                //If errror just return new, with no links, prevent from breaking every single page
                //_context.Logger.Error(e, typeof(ComponentsSurfaceController));
                return PartialView("~/Views/Partials/Shared/RichTextContent.cshtml", new RichTextContentVm());
            }
        }

        public ActionResult WhitePapersList(int dataSourceId)
        {
            try
            {
                var component = _componentsService.GetWhitePapersList(dataSourceId);
                return PartialView("~/Views/Partials/Shared/WhitePapersList.cshtml", component);
            }
            catch (Exception e)
            {
                //If errror just return new, with no links, prevent from breaking every single page
                //_context.Logger.Error(e, typeof(ComponentsSurfaceController));
                return PartialView("~/Views/Partials/Shared/WhitePapersList.cshtml", new WhitePapersListVm());
            }
        }

        public ActionResult OfficesList(int dataSourceId)
        {
            try
            {
                var component = _componentsService.GetOfficesList(dataSourceId);
                return PartialView("~/Views/Partials/Shared/OfficesList.cshtml", component);
            }
            catch (Exception e)
            {
                //If errror just return new, with no links, prevent from breaking every single page
                //_context.Logger.Error(e, typeof(ComponentsSurfaceController));
                return PartialView("~/Views/Partials/Shared/OfficesList.cshtml", new OfficesListVm());
            }
        }

        public ActionResult PeopleList(int dataSourceId)
        {
            try
            {
                var component = _componentsService.GetPeopleList(dataSourceId);
                return PartialView("~/Views/Partials/Shared/PeopleList.cshtml", component);
            }
            catch (Exception e)
            {
                //If errror just return new, with no links, prevent from breaking every single page
                //_context.Logger.Error(e, typeof(ComponentsSurfaceController));
                return PartialView("~/Views/Partials/Shared/PeopleList.cshtml", new PeopleListVm());
            }
        }

        public ActionResult TechnologiesList(int dataSourceId)
        {
            try
            {
                var component = _componentsService.GetTechnologiesList(dataSourceId);
                return PartialView("~/Views/Partials/Shared/TechnologiesList.cshtml", component);
            }
            catch (Exception e)
            {
                //If errror just return new, with no links, prevent from breaking every single page
                //_context.Logger.Error(e, typeof(ComponentsSurfaceController));
                return PartialView("~/Views/Partials/Shared/TechnologiesList.cshtml", new TechnologiesListVm());
            }
        }

        public ActionResult TweetsList(int dataSourceId)
        {
            try
            {
                var component = _componentsService.GetTweetsList(dataSourceId);
                return PartialView("~/Views/Partials/Shared/TweetsList.cshtml", component);
            }
            catch (Exception e)
            {
                //If errror just return new, with no links, prevent from breaking every single page
                //_context.Logger.Error(e, typeof(ComponentsSurfaceController));
                return PartialView("~/Views/Partials/Shared/TweetsList.cshtml", new TweetsListVm());
            }
        }

        [HttpGet]
        public JsonResult JsonPeopleList(string id)
        {
            try
            {
                var dataSourceId = Convert.ToInt32(id.Split('_').First());
                var language = id.Split('_').Last();
                System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);

                var jsonModel = _componentsService.GetPeopleListJson(dataSourceId);
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new PeopleListJson(), JsonRequestBehavior.AllowGet);
            }
        }

        //public ActionResult ServicesList(int dataSourceId)
        //{
        //    ViewData["React"] = "true";
        //    try
        //    {
        //        var component = _componentsService.GetServicesList(dataSourceId);
        //        return PartialView("~/Views/Partials/Shared/ServicesList.cshtml", component);
        //    }
        //    catch (Exception e)
        //    {
        //        //If errror just return new, with no links, prevent from breaking every single page
        //        //_context.Logger.Error(e, typeof(ComponentsSurfaceController));
        //        return PartialView("~/Views/Partials/Shared/ServicesList.cshtml", new ServicesListVm());
        //    }
        //}

        [HttpGet]
        public JsonResult JsonServicesList(string id)
        {
            try
            {
                var dataSourceId = Convert.ToInt32(id.Split('_').First());
                var language = id.Split('_').Last();
                System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);

                var jsonModel = _componentsService.GetServicesListJson(dataSourceId);
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new ServicesListJson(), JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Carousel(int dataSourceId)
        {
            try
            {
                var component = _componentsService.GetCarousel(dataSourceId);
                return PartialView("~/Views/Partials/Shared/Carousel.cshtml", component);
            }
            catch (Exception e)
            {
                //If errror just return new, with no links, prevent from breaking every single page
                //_context.Logger.Error(e, typeof(ComponentsSurfaceController));
                return PartialView("~/Views/Partials/Shared/Carousel.cshtml", new CarouselVm());
            }
        }

        [HttpGet]
        public JsonResult JsonCarousel(int id)
        {
            try
            {
                var jsonModel = _componentsService.GetCarouselJson(id);
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new CarouselJson(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult JsonCaseStudyList(string id)
        {
            try
            {
                var dataSourceId = Convert.ToInt32(id.Split('_').First());
                var language = id.Split('_').Last();
                System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);

                var jsonModel = _componentsService.GetCaseStudyLandingJson(dataSourceId);
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new CaseStudyLandingJson(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult JsonArticleList(string id)
        {
            try
            {
                var dataSourceId = Convert.ToInt32(id.Split('_').First());
                var language = id.Split('_').Last();
                System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);

                var jsonModel = _componentsService.GetNewsLandingJson(dataSourceId);
                return Json(jsonModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new NewsLandingJson(), JsonRequestBehavior.AllowGet);
            }
        }
    }
}