﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cedar.UmbracoExtensions.Extensions;
using Cedar.Web.Infrastructure.Data.Models;
using Cedar.Web.Infrastructure.Data.Models.Supplemental;
using Our.Umbraco.Vorto.Extensions;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace Cedar.Web.Controllers
{
    public class ServiceController : RenderMvcController
    {
        public override ActionResult Index(RenderModel model)
        {
            var customModel = new ServiceModel(model)
            {
                Widgets = model.Content.GetWidgets("Widgets"),
                CTA = model.Content.GetWidget("CTA"),
                Description = model.Content.GetVortoValue("Description") as string ?? string.Empty,
                Details = model.Content.Children.Select(x => x.As<Details>())
            };

            customModel.GetFooterIncludes();

            return CurrentTemplate(customModel);
        }
    }
}