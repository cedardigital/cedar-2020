﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cedar.Web.Infrastructure.Data.ViewModels;
using Umbraco.Core.Services;
using umbraco.NodeFactory;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace Cedar.Web.Controllers
{
    public class FormSurfaceController : SurfaceController
    {
        [HttpPost]
        public JsonResult Contact(CtaMessageUsVm message)
        {
            const int formId = 1401;

            if (ModelState.IsValid)
            {
                try
                {
                    var referrer = HttpContext.Request.UrlReferrer != null ? HttpContext.Request.UrlReferrer.ToString() : string.Empty;

                    var contentService = Services.ContentService;
                    var content = contentService.CreateContent(DateTime.Today.ToString("yy/MM/dd"), formId, "ContactFormSubmission");
                    content.SetValue("name", message.Name);
                    content.SetValue("jobTitle", message.JobTitle);
                    content.SetValue("email", message.Email);
                    content.SetValue("message", message.Message);
                    content.SetValue("urlReferrer", referrer);
                    contentService.Save(content);

                    var helper = new UmbracoHelper(UmbracoContext.Current);
                    var formFolder = helper.TypedContent(formId);
                    var sendEmailsFrom = formFolder.GetPropertyValue<string>("emailFrom");
                    var sendEmailsTo = formFolder.GetPropertyValue<string>("emailTo");

                    var body = String.Format("<p>New contact form submission</p><p>Name: {0}<br />Job Title: {1}<br />Email: {2}<br />Message: {3}<br />Url: {4}</p>", 
                        message.Name, 
                        message.JobTitle, 
                        message.Email,
                        message.Message.Replace("\n", "<br />"),
                        referrer);
                    var subject = "Cedar Contact Form - Message Sent " + DateTime.Today.ToString("yy/MM/dd");

                    umbraco.library.SendMail(sendEmailsFrom, sendEmailsTo, subject, body, true);

                    return Json(new { Result = true });
                }
                catch (Exception ex)
                {
                    // send a different message for an exception?
                }
            }
            return Json(new { Result = false });
        }
    }
}