﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cedar.UmbracoExtensions.Extensions;
using Cedar.Web.Infrastructure.Data.Models;
using Our.Umbraco.Vorto.Extensions;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace Cedar.Web.Controllers
{
    public class HomePageController : RenderMvcController
    {
        public override ActionResult Index(RenderModel model)
        {
            var helper = new UmbracoHelper(UmbracoContext.Current);
            var customModel = new HomePageModel(model)
            {
                IntroTitle = model.Content.GetVortoValue("IntroTitle") as string ?? string.Empty,
                IntroText = model.Content.GetVortoValue("IntroText") as string ?? string.Empty,
                IntroLink = helper.TypedContent(model.Content.GetProperty("IntroLink").Value),
                Widgets = model.Content.GetWidgets("Widgets"),
                CTA = model.Content.GetWidget("CTA")
            };

            customModel.GetFooterIncludes();

            return CurrentTemplate(customModel);
        }
    }
}