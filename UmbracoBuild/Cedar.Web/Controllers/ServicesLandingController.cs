﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cedar.UmbracoExtensions.Extensions;
using Cedar.Web.Infrastructure.Data.Models;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace Cedar.Web.Controllers
{
    public class ServicesLandingController : RenderMvcController
    {
        public override ActionResult Index(RenderModel model)
        {
            var customModel = new ServicesLandingModel(model)
            {
                Widgets = model.Content.GetWidgets("Widgets")
            };

            customModel.GetFooterIncludes();
            customModel.FooterIncludes["React"] = true;

            return CurrentTemplate(customModel);
        }
    }
}