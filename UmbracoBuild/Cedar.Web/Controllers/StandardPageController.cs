﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cedar.UmbracoExtensions.Extensions;
using Cedar.Web.Infrastructure.Data.Models;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace Cedar.Web.Controllers
{
    public class StandardPageController : RenderMvcController
    {
        public override ActionResult Index(RenderModel model)
        {
            var customModel = new StandardPageModel(model)
            {
                Widgets = model.Content.GetWidgets("Widgets"),
                CTA = model.Content.GetWidget("CTA")
            };

            customModel.GetFooterIncludes();

            return CurrentTemplate(customModel);
        }
    }
}