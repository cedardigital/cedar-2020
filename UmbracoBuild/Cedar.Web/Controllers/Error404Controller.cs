﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cedar.UmbracoExtensions.Extensions;
using Cedar.Web.Infrastructure.Data.Models;
using Our.Umbraco.Vorto.Extensions;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace Cedar.Web.Controllers
{
    public class Error404Controller : RenderMvcController
    {
        public override ActionResult Index(RenderModel model)
        {
            var customModel = new Error404Model(model)
            {
                Title = model.Content.GetVortoValue("Title") as string ?? string.Empty,
                SubTitle = model.Content.GetVortoValue("SubTitle") as string ?? string.Empty,
                IntroText = model.Content.GetVortoValue("IntroText") as string ?? string.Empty
            };

            return CurrentTemplate(customModel);
        }
    }
}