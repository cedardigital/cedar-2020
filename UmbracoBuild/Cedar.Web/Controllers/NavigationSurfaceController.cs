﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Cedar.Web.Infrastructure.Data.Models.Folder;
using Cedar.Web.Infrastructure.Data.Models.Supplemental;
using Cedar.Web.Infrastructure.Data.ViewModels;
using Cedar.Web.Infrastructure.Extensions;
using umbraco.cms.presentation.Trees;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;
using Cedar.UmbracoExtensions.Extensions;
using Cedar.Web.Infrastructure.Helpers;
using Umbraco.Web.Mvc;

namespace Cedar.Web.Controllers
{
    public class NavigationSurfaceController : SurfaceController
    {
        public ActionResult Header()
        {
            var header = NodeHelper.GetHeaderNavigation().As<Header>();
            var headerVm = header.Map();

            var currentSite = NodeHelper.GetCurrentSite();
            headerVm.SiteTitle = currentSite["Title"].ToString();

            var sites = NodeHelper.GetSites();
            headerVm.Sites = new List<SiteContainer>();            
            foreach (var site in sites)
            {
                var homepage = site.Children().First(x => x.DocumentTypeAlias.Equals("HomePage"));
                var domains = umbraco.library.GetCurrentDomains(homepage.Id);
                foreach (var domain in domains)
                {
                    var language = string.Empty;
                    if (domains.Count() > 1)
                    {
                        var cultureInfo = new CultureInfo(domain.DomainEntity.LanguageIsoCode);
                        var nativeName = cultureInfo.NativeName;
                        if (nativeName.Contains('('))
                        {
                            nativeName = nativeName.Remove(nativeName.IndexOf('(')).Trim();
                        }
                        language = string.Format(" ({0})", nativeName);
                    }

                    headerVm.Sites.Add(new SiteContainer
                    {
                        Title = site["Title"] + language,
                        Url = "//" + domain.Name
                    });
                }
            }

            var socialLinks = NodeHelper.GetSocialElements().Children();
            headerVm.SocialLinks = new List<ExternalLink>();
            foreach (var item in socialLinks)
            {
                headerVm.SocialLinks.Add(new ExternalLink
                {
                    Name = item.Name,
                    Class = item["Class"].ToString(),
                    Url = item["LinkUrl"].ToString()
                });
            }
            headerVm.LocalizedHomeUrl = NodeHelper.LogoLocalizedUrl(CurrentPage);
            return PartialView("~/Views/Partials/_Header.cshtml", headerVm);
        }

        public ActionResult Footer()
        {
            var footer = NodeHelper.GetFooterNavigation().As<Footer>();
            var footerVm = footer.Map();

            var socialLinks = NodeHelper.GetSocialElements().Children();
            footerVm.SocialLinks = new List<ExternalLink>();
            foreach (var item in socialLinks)
            {
                footerVm.SocialLinks.Add(new ExternalLink
                {
                    Name = item.Name,
                    Class = item["Class"].ToString(),
                    Url = item["LinkUrl"].ToString()
                });
            }

            return PartialView("~/Views/Partials/_Footer.cshtml", footerVm);
        }
    }
}