﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cedar.UmbracoExtensions.Extensions;
using Cedar.Web.Infrastructure.Data.Models;
using Cedar.Web.Infrastructure.Data.Models.Supplemental;
using Cedar.Web.Infrastructure.Helpers;
using Our.Umbraco.Vorto.Extensions;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace Cedar.Web.Controllers
{
    public class ArticleController : RenderMvcController
    {
        public override ActionResult Index(RenderModel model)
        {
            var articlesList = NodeHelper.GetAllArticles();
            var helper = new UmbracoHelper(UmbracoContext.Current);
            var customModel = new ArticleModel(model)
            {
                Title = model.Content.GetVortoValue("Title") as string ?? string.Empty,
                CTA = model.Content.GetWidget("CTA"),
                PrimarySite = helper.TypedContent(model.Content.GetProperty("PrimarySite").Value).As<SiteContainer>(),
                Author = model.Content.GetProperty("Author").HasValue ? helper.TypedContent(model.Content.GetProperty("Author").Value).As<Person>() : null,
                DisplayAuthorLink = model.Content.GetProperty("Author").HasValue && articlesList.GroupBy(article => article.Author).Where(group => group.Count() >= 3).Any(x => x.Key == (int)model.Content.GetProperty("Author").Value),
                RelatedArticles = NodeHelper.GetMultiNodePickerItems<Article>(model.Content.GetProperty("RelatedArticles").Value.ToString()),
                Tags = NodeHelper.GetMultiNodePickerItems<Tag>(model.Content.GetProperty("Tags").Value.ToString()),
                BackgroundPosition = string.IsNullOrEmpty(model.Content.GetProperty("backgroundPosition").DataValue.ToString()) ? "bg-img-top" : model.Content.GetProperty("backgroundPosition").DataValue.ToString()
            };

            customModel.CanonicalUrl = NodeHelper.GetCanonicalUrl(customModel);
            customModel.FooterIncludes["TwitterIntents"] = true;

            return CurrentTemplate(customModel);
        }
    }
}