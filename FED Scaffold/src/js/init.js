/* ----------
Cedar DCX
- Init JS
---------- */

// Content to execute on page load
dcx.init = function() {
	"use strict";

	// Deferred loading of custom fonts
	dcx.fontLoader();

	// Call top navigation function
	dcx.topNavigation();

	// Configure smooth scrolling plugin - see https://zengabor.github.io/zenscroll
	zenscroll.setup(null, 0);

	// Call the accordion function
	dcx.accordion();

	// Call Scroll Down arrow on homepage
	dcx.scrollToFirstPanel();

	// Load more button functionality
	dcx.loadMoreButtons();

	// Footer Newsletter function
	dcx.footerNewsletter();

	// Footer contact form
	dcx.footerContactForm();

	// Contact us page Google Maps
	dcx.mapInit();

	// Carousel nav functionality
	dcx.carouselNav();

	// Google Analytics tracking events
	dcx.analytics().init();
}(); // Self calling