/* ----------
Cedar DCX
- Carousel JS
---------- */

/** Uses React Slick plugin [https://github.com/akiran/react-slick] **/

// DOM node to mount to
var carouselDOM = document.querySelectorAll(".carousel");

// Only fire if carousel exists on current page
if (carouselDOM.length > 0) {

	// Is touchscreen device (carousel works differently between touch and non-touch devices)
	var isTouch = dcx.helpers.hasClass(document.getElementsByTagName("body")[0], "is-touch");

	// Single item carousel
	var SingleItem = React.createClass({
		// Prop validation (https://facebook.github.io/react/docs/reusable-components.html)
		propTypes: {
			feed: React.PropTypes.string
		},

		// Initial state
		getInitialState: function() {
			"use strict";

			return {
				carouselData: []
			};
		},

		// The component has mounted
		componentDidMount: function() {
			"use strict";

			var self = this,
				request = new XMLHttpRequest();

			// Ajax Call
			request.open("GET", this.props.feed, true);
			request.onload = function() {
				if (request.status >= 200 && request.status < 400) { // Success
					var data = JSON.parse(request.responseText);

					self.setState({carouselData: data});
				}
			};
			request.send();
		},

		render: function() {
			"use strict";

			// Slick config options
			var settings = {
				dots: true,
				infinite: !isTouch,
				swipe: isTouch,
				speed: 500,
				slidesToShow: 1,
				slidesToScroll: 1,
				afterChange: function() {
					// Fire GA event
					ga("send", "event", "Image carousel", "Interaction", "" + dcx.helpers.getPageTitle() + "");
				}
			};

			// Declare our variables
			var output = [],
				data = this.state.carouselData;

			for (var i = 0; i < data.length; i++) {
				output.push(
					<figure key={data[i].id}>
						<img src={data[i].imageDesktop} srcSet={data[i].imageDesktop + " 1366w, " + data[i].imageMobile + " 720w"} sizes="(max-width: 720px) 50vw, 100vw" />
						<figcaption>{data[i].caption}</figcaption>
					</figure>
				);
			}

			return (
				<Slider {...settings}>
					{output}
				</Slider>
			);
		}
	});

	// Enable touch events
	React.initializeTouchEvents && React.initializeTouchEvents(true);

	// Render to page
	for (var i = 0; i < carouselDOM.length; i++) {
		ReactDOM.render(<SingleItem feed={carouselDOM[i].getAttribute("data-feed")} />, carouselDOM[i]); // Loop allows for multiple carousels on a page
	}
}