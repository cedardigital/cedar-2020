// DOM element to mount to
var filterGrid = document.getElementById("filtered-content");

// ESLint overrides
/*eslint react/no-find-dom-node: 0*/

if (filterGrid !== null) {
	// Define our variables
	var mountTo = filterGrid, // Element to Mount to
		feed = mountTo.getAttribute("data-feed"), // JSON Feed to use
		dataType = mountTo.getAttribute("data-type"), // Type of data (news or work)
		domain = mountTo.getAttribute("data-domain"), // Full domain to use in href attributes
		resultsCount = {
			// Number of filtered results
			num: null
		},
		elementsToShow = {
			// Number of items to show in this breakpoint
			num: null
		};

	// Main Component
	var Main = React.createClass({
		getInitialState: function() {
			"use strict";

			return {
				itemsList: [], // Full list of items
				allTags: [], // Full list of tags
				activeAuthors: [], // List of active authors
				allCategories: [], // Full list of categories
				filter: [], // Active filter (type and value)
				animate: "", // Animate class to animate list items
				tagButtonClass: "", // Class to change styling on Filter Toggle Button
				tagActive: true, // Is there a tag/author actiFve?
				tagActiveName: "", // Nice name for the active tag/author (used on Tag Indicator)
				tagsShown: false, // Is the tag list being shown? Used to prevent Categories from appearing
				validQueryString: true, // Is there a valid query string?

				// Label/button text (defined via data attributes)
				labelOpenFilter: mountTo.getAttribute("data-openfilter-label"),
				labelCloseFilter: mountTo.getAttribute(
					"data-closefilter-label"
				),
				labelAuthors: mountTo.getAttribute("data-authors-label"),
				labelTags: mountTo.getAttribute("data-tags-label"),
				labelArticlesTagged: mountTo.getAttribute(
					"data-articles-tagged"
				)
			};
		},

		// Once the component has mounted
		componentWillMount: function() {
			"use strict";

			// Ajax Call
			var self = this,
				request = new XMLHttpRequest();

			request.open("GET", feed, true);
			request.onload = function() {
				if (request.status >= 200 && request.status < 400) {
					// Success
					var data = JSON.parse(request.responseText);

					// Create the lists for our items, authors, categories and tags
					self.createLists(data);
				}
			};
			request.send();

			// Check if there is a query string
			if (!checkIfQueryStringExists()) {
				this.setState({
					tagActive: false
				});
			}
		},

		// Once a filter is selected
		// Passes in 3 variables:
		// - The type of item (category, tag, author)
		// - The name of that item as a slug (news, views, tag-1, tag-2)
		// - The nice name for that item (News, Views, Tag 1, Tag 2)
		selectFilter: function(eleType, ele, name) {
			"use strict";

			var eleArr = [eleType, ele],
				self = this,
				tag = "",
				loadMore = document.getElementById("load-more-stories");

			// Check if the button exists and show it
			if (loadMore !== null) {
				dcx.helpers.removeClass(loadMore, "hide");
			}

			// If this is a tag or an author, set the tagActive to true
			// Categories are not considered as a "tag"
			if (eleArr[0] === "tag" || eleArr[0] === "author") {
				tag = true;
			} else {
				tag = false;
			}

			// Change the states and add our .animate class
			// This will cause the elements to animate out
			this.setState({
				animate: "animate",
				tagActive: tag,
				tagActiveName: name
			});

			// After animation has finished, filter our items
			// Ensure the filtered elements have the animate class so they can animate back in
			setTimeout(function() {
				self.setState({
					filter: eleArr,
					animate: "animate"
				});
			}, 350);

			// Once they have been rendered onto the page, remove the class so that they can animate back into view
			setTimeout(function() {
				self.setState({
					animate: ""
				});
			}, 400);
		},

		// Create our lists of filters (tags, categories, authors)
		createLists: function(data) {
			"use strict";

			var authors, categories, items, usedTags;

			// Turn our objects into arrays
			// Is this the news page?
			if (isNewsPage()) {
				categories = data.categories.map(function(cats) {
					return cats;
				});

				authors = data.activeAuthors.map(function(auths) {
					return auths;
				});

				usedTags = data.tags.map(function(tags) {
					return tags;
				});

				items = data.items;
			} else {
				usedTags = data.tags.map(function(tags) {
					return tags;
				});

				items = data.items;
			}

			// Set states to have the correct list of filters
			this.setState({
				activeAuthors: authors,
				allTags: usedTags,
				allCategories: categories,
				itemsList: items
			});

			// Check Query String
			this.checkQueryString();
		},

		checkQueryString: function() {
			"use strict";

			// If query string has been set
			if (checkIfQueryStringExists()) {
				// Find the nicename value
				var niceName = this.returnNiceName(
					getQueryString("filter"),
					getQueryString("val")
				);

				// If it's a valid query string
				if (this.state.validQueryString) {
					// Filter data
					this.selectFilter(
						getQueryString("filter"),
						getQueryString("val"),
						niceName
					);
					// Set the Active Filter
					this.setActiveFilter();
				} else {
					// If it's not a valid Query String
					// Set tagActive to false (so it doesn't show the tag indicator)
					// and shows the categories instead
					this.setState({
						tagActive: false
					});
					// Show all items
					this.selectFilter("all", "all", "All");
				}
			} else {
				// Show all items
				this.selectFilter("all", "all", "All");
			}
		},

		returnNiceName: function(type, val) {
			"use strict";

			var cats, authors, tags;

			if (isNewsPage()) {
				if (type === "category") {
					// If it's a category
					cats = this.state.allCategories;

					// Loop through the list of categories
					return this.findNiceName(cats, val);
				} else if (type === "author") {
					// If it's an author
					authors = this.state.activeAuthors;

					// Loop through the list of active authors
					return this.findNiceName(authors, val);
				} else if (type === "tag") {
					// If it's a tag
					tags = this.state.allTags;

					// Loop through the list of tags
					return this.findNiceName(tags, val);
				} else {
					// If it's not a valid type
					this.setState({
						validQueryString: false
					});
					return null;
				}
			} else {
				if (type === "tag") {
					// If it's a tag
					tags = this.state.allTags;

					// Loop through the list of tags
					return this.findNiceName(tags, val);
				} else {
					// If it's not a valid type
					this.setState({
						validQueryString: false
					});
					return null;
				}
			}
		},

		findNiceName: function(filter, val) {
			"use strict";

			var count = 0,
				name = "";

			// Loop through the list of items
			for (var i = 0; i < filter.length; i++) {
				if (val === filter[i].slug) {
					count++;
					name = filter[i].name;
				}
			}

			// If there is no match
			if (count === 0) {
				// Set the state to be false
				this.setState({
					validQueryString: false
				});
			}

			return name;
		},

		// Set Active Filter Indicator
		setActiveFilter: function() {
			"use strict";

			var fType = getQueryString("filter"),
				fVal = getQueryString("val"),
				tagLists = document.querySelectorAll("#tags-wrap ul"),
				catList = document.querySelectorAll(".category-wrap");

			if (fType === "category") {
				// If it's a category filter
				// Reset Categories
				this.resetCategories();

				// Set Active Category
				this.setActiveFilterButton(catList, "on", fVal, "cat");
			} else if (fType === "tag" || fType === "author") {
				// If it's an author/tag filter
				// Loop through the 2 tag tagLists
				this.setActiveFilterButton(tagLists, "rem-tag", fVal, "tag");
			}
		},

		// Set Active Filter Button
		setActiveFilterButton: function(list, activeClass, val, type) {
			"use strict";

			for (var i = 0; i < list.length; i++) {
				for (var a = 0; a < list[i].children.length; a++) {
					var el = list[i].children[a].children[0];
					var id = list[i].children[a].children[0].id;

					// Activate the appropriate tag
					if (id === val) {
						dcx.helpers.addClass(el, activeClass);
						if (type === "cat") {
							el.disabled = true;
						}
					}
				}
			}
		},

		// Toggle(Show/Hide) the Tags panel function
		toggleTags: function(e) {
			"use strict";

			var text = "",
				btnClass = "",
				tagWrap = document.getElementById("tags-wrap"),
				tagsShown = false;

			// If the event is undefined
			if (typeof e !== "undefined") {
				// If it's the toggle button
				if (
					dcx.helpers.hasClass(e.target, "toggle-btn") &&
					dcx.helpers.hasClass(e.target, "close")
				) {
					// Fire GA event
					// Hide Tags
					ga(
						"send",
						"event",
						"Filter",
						"Filter close",
						"" + dcx.helpers.getPageTitle() + ""
					);
				} else if (
					dcx.helpers.hasClass(e.target, "toggle-btn") &&
					!dcx.helpers.hasClass(e.target, "close")
				) {
					// Fire GA event
					// Show Tags
					ga(
						"send",
						"event",
						"Filter",
						"Filter by topic",
						"" + dcx.helpers.getPageTitle() + ""
					);
				}
			}

			// Check existing content in Filter button
			// Open Accordion
			if (
				this.state.labelOpenFilter ===
				mountTo.getAttribute("data-openfilter-label")
			) {
				// Change text value to close
				text = this.state.labelCloseFilter;
				// Add the close class
				btnClass = "close";
				// Show the Tag Panel
				dcx.helpers.addClass(tagWrap, "opened");
				// Set Opened to true
				tagsShown = true;

				// Close Accordion
			} else {
				// Change text value
				text = mountTo.getAttribute("data-openfilter-label");
				// Remove the close class
				btnClass = "";
				// Hide the Tag Panel
				dcx.helpers.removeClass(tagWrap, "opened");
				// Set Opened to false
				tagsShown = false;
			}

			// Change the state of the text and class values
			this.setState({
				labelOpenFilter: text,
				tagButtonClass: btnClass,
				tagsShown: tagsShown
			});
		},

		// Reset all categories
		resetCategories: function() {
			"use strict";

			var activeButtons = document.querySelectorAll(
				".category-wrap button"
			);

			// Remove all instances of the class "on" from all the category buttons
			dcx.helpers.removeClass(activeButtons, "on");

			// Re-enable all category buttons
			for (var i = 0; i < activeButtons.length; i++) {
				activeButtons[i].disabled = false;
			}
		},

		// On Render
		render: function() {
			"use strict";

			var self = this,
				tagIndicator = function() {
					// Check if a tag is active and the panel is hidden
					// If it returns true, add a tag indicator button next to the filter button
					if (
						self.state.tagActive &&
						typeof self.state.tagActiveName !== "undefined" &&
						self.state.tagsShown === false
					) {
						return (
							<div className="flex falgncntr">
								<span className="tagged-text">
									{self.state.labelArticlesTagged}
								</span>
								<FilterButton
									btnType={"reset"}
									filter={"all"}
									selectFilter={self.selectFilter}
									filterNicename={self.state.tagActiveName}
									filterType={"all"}
									classes={"tag"}
									toggleTags={self.toggleTags}
								/>
							</div>
						);
					}
				},
				categoriesList = function() {
					// If it's the News page and there is no tag active
					if (
						isNewsPage() &&
						!self.state.tagActive &&
						!self.state.tagsShown
					) {
						return (
							<div className="fgrow1 flex catswrap">
								<FilterList
									resetCategories={self.resetCategories}
									selectFilter={self.selectFilter}
									allFilters={self.state.allCategories}
									filterType="category"
									classes="category"
								/>
							</div>
						);
					}
				},
				tagList = function() {
					return (
						<div>
							<div className="text-center font110">
								{self.state.labelTags}:
							</div>
							<FilterList
								resetCategories={self.resetCategories}
								selectFilter={self.selectFilter}
								toggleTags={self.toggleTags}
								allFilters={self.state.allTags}
								filterType="tag"
								classes="tag"
							/>
						</div>
					);
				},
				authorsList = function() {
					// If it's the News and Views page and there is at least 1 active author
					if (isNewsPage() && self.state.activeAuthors.length > 0) {
						return (
							<div>
								<div className="text-center font110">
									{self.state.labelAuthors}:
								</div>
								<FilterList
									resetCategories={self.resetCategories}
									selectFilter={self.selectFilter}
									toggleTags={self.toggleTags}
									allFilters={self.state.activeAuthors}
									filterType="author"
									classes="tag"
								/>
							</div>
						);
					}
				};

			// Main Container and elements
			// tagIndicator() will only show and output if the conditions are met above
			return (
				<div>
					<div className="filters">
						<div className="flex">
							<button
								className={
									"button-reset tag toggle-btn " +
									this.state.tagButtonClass
								}
								onClick={this.toggleTags}
							>
								{this.state.labelOpenFilter}
							</button>
							{tagIndicator()}
							{categoriesList()}
						</div>
						<div id="tags-wrap" className="oflow-hidden">
							{tagList()}
							{authorsList()}
						</div>
					</div>
					<StoriesContainer
						categories={this.state.allCategories}
						validQueryString={this.state.validQueryString}
						stories={this.state.itemsList}
						filter={this.state.filter}
						animateClass={this.state.animate}
					/>
				</div>
			);
		}
	});

	// Filter Button Component
	var FilterButton = React.createClass({
		// Prop validation (ESLint requirement)
		propTypes: {
			classes: React.PropTypes.string,
			resetCategories: React.PropTypes.func,
			selectFilter: React.PropTypes.func,
			filterType: React.PropTypes.string,
			filter: React.PropTypes.string,
			filterNicename: React.PropTypes.string,
			toggleTags: React.PropTypes.func,
			btnType: React.PropTypes.string
		},

		handleClick: function() {
			"use strict";

			var ele = ReactDOM.findDOMNode(this),
				eleClass = this.props.classes;

			// If user clicked on a category
			if (eleClass === "category") {
				// Reset all other categories
				this.props.resetCategories();

				// Set new button
				dcx.helpers.addClass(ele, "on");
				ele.disabled = true;

				// Filter items
				this.props.selectFilter(
					this.props.filterType,
					this.props.filter,
					this.props.filterNicename
				);

				// Fire GA event
				ga(
					"send",
					"event",
					"Category",
					"" + this.props.filterNicename + "",
					"" + dcx.helpers.getPageTitle() + ""
				);
			} else if (eleClass === "tag" || eleClass === "author") {
				// If the user clicked on a tag or author

				// Check if the element has been selected
				if (ele.classList.contains("rem-tag")) {
					// Remove the class
					dcx.helpers.removeClass(
						document.querySelectorAll(".rem-tag"),
						"rem-tag"
					);

					// Set filters to show all items
					this.props.selectFilter("all", "all", "All");

					// Fire GA event
					ga(
						"send",
						"event",
						"Filter",
						"Filter clear",
						"" + dcx.helpers.getPageTitle() + ""
					);
				} else {
					// Reset all other tags
					this.resetTags();

					// Set new button
					setTimeout(function() {
						dcx.helpers.addClass(ele, "rem-tag");
					}, 350);

					// Filter items
					this.props.selectFilter(
						this.props.filterType,
						this.props.filter,
						this.props.filterNicename
					);

					// Fire GA event
					ga(
						"send",
						"event",
						"Filter",
						"" + this.props.filterNicename + "",
						"" + dcx.helpers.getPageTitle() + ""
					);
				}

				// If it's not the Tag Indicator Button
				if (ele.id !== "reset") {
					// Toggle the Tags Panel
					this.props.toggleTags();
				}
			}
		},

		// Reset all tags
		resetTags: function() {
			"use strict";

			var activeTags = document.querySelectorAll("#tags-wrap button");

			// Remove all the instances of the class "rem-tag" from all tag elements
			dcx.helpers.removeClass(activeTags, "rem-tag");
		},

		render: function() {
			"use strict";

			var classes = this.props.classes,
				filter = this.props.filterType,
				btnID = this.props.filter;

			// Show All Button Turned on by default
			if (filter === "all" && classes === "category") {
				btnID = "btn-all";
				classes = classes + " on";
			} else if (filter === "all" && classes === "tag") {
				classes = classes + " rem-tag";

				if (this.props.btnType) {
					btnID = this.props.btnType;
				}
			}

			return (
				<button
					id={btnID}
					className={"button-reset " + classes}
					onClick={this.handleClick}
				>
					{this.props.filterNicename}
				</button>
			);
		}
	});

	// List of Filters
	var FilterList = React.createClass({
		// Prop validation (ESLint requirement)
		propTypes: {
			allFilters: React.PropTypes.array
		},

		// On mount, disable the "All" category button
		componentDidMount: function() {
			"use strict";

			var ele = document.getElementById("btn-all");

			if (ele !== null) {
				ele.disabled = true;
			}
		},

		render: function() {
			"use strict";

			var self = this,
				all = function() {
					// If this is a categories list
					// Add the "All" button at the end on the Categories filters
					if (self.props.classes === "category") {
						return (
							<li className="inline-block" key="all">
								<FilterButton
									resetCategories={self.props.resetCategories}
									filter={"all"}
									selectFilter={self.props.selectFilter}
									toggleTags={self.props.toggleTags}
									filterNicename={mountTo.getAttribute(
										"data-all-label"
									)}
									filterType={"all"}
									classes={"category"}
								/>
							</li>
						);
					}
				};

			// Map our filter buttons
			var filterButtons = this.props.allFilters.map(function(allFilters) {
				return (
					<li key={allFilters.slug} className="inline-block">
						<FilterButton
							resetCategories={self.props.resetCategories}
							filter={allFilters.slug}
							toggleTags={self.props.toggleTags}
							filterNicename={allFilters.name}
							selectFilter={self.props.selectFilter}
							filterType={self.props.filterType}
							classes={self.props.classes}
						/>
					</li>
				);
			});

			return (
				<ul className={"no-mar no-pad " + self.props.classes + "-wrap"}>
					{filterButtons}
					{all()}
				</ul>
			);
		}
	});

	// Story/Item Components
	var Story = React.createClass({
		// Prop validation (ESLint requirement)
		propTypes: {
			hideClass: React.PropTypes.string,
			animateClass: React.PropTypes.string,
			categories: React.PropTypes.array,
			stories: React.PropTypes.object
		},

		// Get the nice name for the filter, including translation
		getNiceName: function(list, el) {
			"use strict";

			for (var i = 0; i < list.length; i++) {
				if (list[i].slug === el) {
					return list[i].name;
				}
			}
		},

		render: function() {
			"use strict";

			var image;

			if (this.props.hideClass === " hide") {
				image = {
					backgroundImage: "none"
				};
			} else {
				image = {
					backgroundImage: "url(" + this.props.stories.image + ")"
				};
			}

			if (isNewsPage()) {
				return (
					<li
						className={
							"col flex box-size padleft05 padright05 marginbot1 " +
							this.props.animateClass +
							this.props.hideClass
						}
					>
						<a
							href={domain + this.props.stories.url}
							className="flex fcol text-center"
						>
							<div className="img-wrapper-16-9 oflow-hidden">
								<div
									className="img-zoom"
									style={image}
									data-img={this.props.stories.image}
								></div>
							</div>
							<div className="pad1 flex fgrow1 fcol">
								<span className="colour-d primary-font text-upper font80 letter-spacing block padbot05">
									{this.getNiceName(
										this.props.categories,
										this.props.stories.category
									)}
								</span>
								<strong className="webfont-secondary black block padbot05 fgrow1">
									{this.props.stories.title}
								</strong>
								<div className="btn-more">
									<div className="text-upper btn-primary-on-hover primary-font">
										{mountTo.getAttribute(
											"data-readmore-label"
										)}
									</div>
								</div>
							</div>
						</a>
					</li>
				);
			} else {
				return (
					<li
						className={
							"text-center col box-size flex " +
							this.props.animateClass +
							this.props.hideClass
						}
					>
						<a
							href={domain + this.props.stories.url}
							className="fgrow1 flex fcol width-100"
						>
							<div className="img-wrapper-16-9 oflow-hidden">
								<div
									className="img-zoom"
									style={image}
									data-img={this.props.stories.image}
								></div>
							</div>
							<div className="content box-size flex fcol fgrow1">
								<span className="colour-d text-upper primary-font">
									{this.props.stories.location}
								</span>
								<h3>{this.props.stories.title}</h3>
								<p className="width-100 fgrow1">
									{this.props.stories.desc}
								</p>
								<div className="btn btn-primary-on-hover primary-font">
									{mountTo.getAttribute(
										"data-readmore-label"
									)}
								</div>
							</div>
						</a>
					</li>
				);
			}
		}
	});

	// Stories Container
	var StoriesContainer = React.createClass({
		// Prop validation (ESLint requirement)
		propTypes: {
			filter: React.PropTypes.array,
			stories: React.PropTypes.array
		},

		storiesList: function() {
			"use strict";

			var self = this,
				type = this.props.filter[0],
				ele = this.props.filter[1],
				count = 0,
				hideClass = "";

			// Find out how many to load more
			if (isNewsPage()) {
				if (dcx.helpers.bpxs()) {
					elementsToShow.num = 4; // Columns of 1
				} else if (dcx.helpers.bpm()) {
					elementsToShow.num = 4; // Columns of 2
				} else if (dcx.helpers.bpl()) {
					elementsToShow.num = 6; // Columns of 3
				} else if (dcx.helpers.greaterThanBpl()) {
					elementsToShow.num = 8; // Columns of 4
				}
			} else {
				if (dcx.helpers.bpm()) {
					elementsToShow.num = 4;
				} else {
					elementsToShow.num = 6;
				}
			}

			var filteredStories = this.props.stories.map(function(stories) {
				// If the filter state is empty (buttons have not been clicked/on load)
				// Check what kind of filter it is
				// Look for a match for that specific element
				// Display this story

				if (
					self.props.filter === "" ||
					type === "all" ||
					(type === "category" && ele === stories.category) ||
					(type === "tag" && checkTag(ele, stories.tags)) ||
					(type === "location" && ele === stories.location) ||
					(type === "author" && ele === stories.author)
				) {
					count++;

					if (count > elementsToShow.num) {
						hideClass = " hide";
					}

					return (
						<Story
							categories={self.props.categories}
							hideClass={hideClass}
							key={stories.id}
							stories={stories}
							animateClass={self.props.animateClass}
						/>
					);
				} else {
					return null;
				}
			});

			// If no results are found, show everything
			if (count === 0) {
				count++;

				if (count > elementsToShow.num) {
					hideClass = " hide";
				}

				filteredStories = this.props.stories.map(function(stories) {
					return (
						<Story
							hideClass={hideClass}
							key={stories.id}
							stories={stories}
							animateClass={self.props.animateClass}
						/>
					);
				});
			}

			resultsCount.num = count;

			if (typeof type !== "undefined" && typeof ele !== "undefined") {
				return filteredStories;
			}
		},

		loadMoreAction: function() {
			"use strict";

			var numToShow = 4, // Number of items to show on Load More
				hiddenElements = document.querySelectorAll(
					"#articles-list .hide"
				),
				loadMoreBtn = document.getElementById("load-more-stories");

			if (isNewsPage()) {
				// Find out how many to load more
				if (dcx.helpers.bpxs()) {
					numToShow = 2; // Columns of 1
				} else if (dcx.helpers.bpm()) {
					numToShow = 4; // Columns of 2
				} else if (dcx.helpers.bpl()) {
					numToShow = 3; // Columns of 3
				} else if (dcx.helpers.greaterThanBpl()) {
					numToShow = 4; // Columns of 4
				}
			} else {
				numToShow = 2;
			}

			// Remove focus from the Show More button
			loadMoreBtn.blur();

			// Loop through number of items we're showing
			for (var i = 0; i < numToShow; i++) {
				// Show this element (remove class "hide")
				if (typeof hiddenElements[i] !== "undefined") {
					// Set the image src to be the correct path
					var bgImg =
							hiddenElements[i].children[0].children[0]
								.children[0],
						image = bgImg.getAttribute("data-img");

					bgImg.setAttribute(
						"style",
						"background-image:url(" + image + ")"
					);

					// Show the story
					dcx.helpers.removeClass(hiddenElements[i], "hide");
				}
			}

			// Get the new list of items hidden elements
			hiddenElements = document.querySelectorAll("#articles-list .hide");

			// Check if there are still items to show
			if (hiddenElements.length === 0 || hiddenElements === null) {
				dcx.helpers.addClass(loadMoreBtn, "hide");
			}

			// Fire GA event
			ga(
				"send",
				"event",
				"CTA",
				"Load more",
				"" + dcx.helpers.getPageTitle() + ""
			);
		},

		loadMoreButton: function(results, show) {
			"use strict";

			if (results > show) {
				return (
					<button
						id="load-more-stories"
						className={
							"button-reset btn btn-primary margintop1 marginbot1"
						}
						onClick={this.loadMoreAction}
					>
						{mountTo.getAttribute("data-loadmore-label")}
					</button>
				);
			}
		},

		render: function() {
			"use strict";

			if (isNewsPage()) {
				return (
					<div>
						<ul
							id="articles-list"
							className="no-pad no-mar flex frow fwrap col4md articles-list filter-grid margintop1-5"
						>
							{this.storiesList()}
						</ul>
						{this.loadMoreButton(
							resultsCount.num,
							elementsToShow.num
						)}
					</div>
				);
			} else {
				return (
					<div
						id="articles-list"
						className="col2 container no-side-pad"
					>
						<ul className="promo-list no-pad flex frow fwrap filter-grid">
							{this.storiesList()}
						</ul>
						{this.loadMoreButton(
							resultsCount.num,
							elementsToShow.num
						)}
					</div>
				);
			}
		}
	});

	// Check if it's the News Page
	function isNewsPage() {
		"use strict";

		if (dataType === "news") {
			return true;
		} else {
			return false;
		}
	}

	// Check if a Query String has been set
	function checkIfQueryStringExists() {
		"use strict";

		var url = window.location.href;

		if (url.indexOf("?filter=") !== -1) {
			return true;
		} else {
			return false;
		}
	}

	// Get the value of the Query String
	function getQueryString(value) {
		"use strict";

		var url = window.location.href;

		value = value.replace(/[\[\]]/g, "\\$&");

		var regex = new RegExp("[?&]" + value + "(=([^&#]*)|&|#|$)"),
			results = regex.exec(url);

		if (!results) {
			return null;
		}

		if (!results[2]) {
			return "";
		}

		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}

	// Check if value is in Tag array
	function checkTag(str, arr) {
		"use strict";

		// Loop through all of the elements in array
		for (var i = 0; i < arr.length; i++) {
			// Check if there is a match

			if (arr[i] === str) {
				return true;
			}
		}
	}

	ReactDOM.render(<Main />, mountTo);
}
