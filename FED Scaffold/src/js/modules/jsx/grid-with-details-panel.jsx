// ESLint overrides
/*eslint consistent-this: 0*/

// DOM element to mount to
var detailsGrid = document.getElementById("details-grid");

// Only fire this code on pages that have the mount div present
if (detailsGrid !== null) {
	// Define our variables
	var mountTo = detailsGrid,
		ReactCSSTransitionGroup = React.addons.CSSTransitionGroup, // React's animation add-on: https://facebook.github.io/react/docs/animation.html
		slideAndScrollDuration = 350,
		sectionHeading = mountTo.getAttribute("data-heading"),
		primaryClasses = mountTo.getAttribute("data-primarystyles"),
		secondaryClasses = mountTo.getAttribute("data-secondarystyles"),
		btnReadMoreLabel = mountTo.getAttribute("data-readmore-label"),
		btnFindOutMoreLabel = mountTo.getAttribute("data-findoutmore-label"),
		domain = mountTo.getAttribute("data-domain"),
		feed = mountTo.getAttribute("data-feed"),
		width = window.innerWidth,
		panelHeight = {
			prevHeight: null
		};

	// Main component logic
	var Main = React.createClass({
		getInitialState: function() {
			"use strict";

			return {
				gridData: [],
				detailsID: null
			};
		},

		// Update item state so we know whose details to display
		itemState: function(gridItemID) {
			"use strict";

			this.setState({
				detailsID: gridItemID
			});
		},

		// The component has mounted
		componentDidMount: function() {
			"use strict";

			var self = this,
				request = new XMLHttpRequest();

			// Ajax Call
			request.open("GET", feed, true);
			request.onload = function() {
				if (request.status >= 200 && request.status < 400) {
					// Success
					var data = JSON.parse(request.responseText);

					self.setState({ gridData: data });
				}
			};
			request.send();
		},

		render: function() {
			"use strict";

			// Does this section have a heading?
			var heading =
				sectionHeading !== null ? <h2>{sectionHeading}</h2> : "";

			return (
				<section className={primaryClasses}>
					{heading}
					<ItemsList
						items={this.state.gridData}
						gridItemID={this.state.detailsID}
						itemState={this.itemState}
					/>
				</section>
			);
		}
	});

	// Items list component
	var ItemsList = React.createClass({
		render: function() {
			"use strict";

			var props = this.props,
				length = props.items.length, // Number of records
				gridItems = [],
				selectedClass = "",
				itemsPerRow,
				insertAfterItem = 0,
				totalItemsRequired = 0;

			// How many items per row? (See js > modules > column-calc.js)
			itemsPerRow = dcx.columnNumber().columns(4); // 4 = maximum items per row

			// How many grid items are needed on wide desktops (must be full rows of 4)
			totalItemsRequired = Math.ceil(length / 4) * 4;

			// Work out position of the details panel
			if (props.gridItemID !== null) {
				insertAfterItem =
					Math.ceil((props.gridItemID + 1) / itemsPerRow) *
					itemsPerRow;

				// Item to insert after cannot exceed the total number of items, including placeholders (denoted by .plh-grid class)
				if (insertAfterItem > totalItemsRequired) {
					insertAfterItem = length;
				}
			}

			// Build our array to output
			for (var i = 0; i < totalItemsRequired; i++) {
				// Set selected class for current opened item
				if (props.gridItemID === i) {
					selectedClass = "selected";
				} else {
					selectedClass = "";
				}

				// Build grid
				if (i < length) {
					// Add each item to the array
					gridItems.push(
						<li
							className={
								"col flex box-size padleft05 padright05 margintop1-5 marginbot1-5 posrel " +
								selectedClass
							}
							key={i}
						>
							<ItemMarkup
								id={i}
								item={props.items[i]}
								itemState={props.itemState}
							/>
						</li>
					);
				} else {
					// Add empty placeholders for grids that are not rows of 4
					gridItems.push(
						<li
							className={"col flex box-size pos-rel plh-grid"}
							key={i}
						></li>
					);
				}

				// Add details panel only once after the correct item
				if (insertAfterItem - 1 === i) {
					gridItems.push(
						<DetailsPanel
							items={props.items}
							gridItemID={props.gridItemID}
							itemState={props.itemState}
							key={"b" + i}
						/>
					);
				}
			}

			// Return every record, using React animation group
			return (
				<ReactCSSTransitionGroup
					component="ul"
					className="no-pad no-mar flex frow fwrap fjustcntr"
					transitionAppear={true}
					transitionAppearTimeout={slideAndScrollDuration}
					transitionName="react-anim-accordion"
					transitionEnterTimeout={slideAndScrollDuration}
					transitionLeaveTimeout={slideAndScrollDuration}
				>
					{gridItems}
				</ReactCSSTransitionGroup>
			);
		}
	});

	// HTML markup for each list item
	var ItemMarkup = React.createClass({
		// Prop validation (https://facebook.github.io/react/docs/reusable-components.html)
		propTypes: {
			itemState: React.PropTypes.func,
			item: React.PropTypes.object,
			id: React.PropTypes.number
		},

		// Update state on link click and scroll to relevant element
		handleClick: function(e) {
			"use strict";

			e.preventDefault();

			var props = this.props,
				id = props.id;

			// Update state
			props.itemState(id);

			// Scroll to element, delayed to allow for any animations to complete first
			setTimeout(function() {
				dcx.scrollTo(
					document.getElementById("item-" + id),
					slideAndScrollDuration
				);
			}, slideAndScrollDuration + 100);

			// On browser resize, hide details panel by setting state back to -1 (width change only);
			window.onresize = function() {
				if (width !== window.innerWidth) {
					// If the page width has changed
					props.itemState(-1);
					width = window.innerWidth;
				}
			};
		},

		render: function() {
			"use strict";

			var self = this.props.item,
				title,
				image = {
					backgroundImage: "url(" + self.thumb + ")"
				};

			// To use if a title is defined in the JSON
			if (typeof self.title !== "undefined") {
				title = (
					<span
						className="primary-font black"
						dangerouslySetInnerHTML={{
							__html: self.title.replace(/,/g, "<br>")
						}}
					></span>
				);
			}

			return (
				<a
					href={"#item-" + this.props.id}
					onClick={this.handleClick}
					className="flex fcol text-center"
				>
					<div className={"oflow-hidden " + secondaryClasses}>
						<div className="img-zoom" style={image}></div>
					</div>
					<div
						id={"item-" + self.id}
						className="padtop1 flex fgrow1 fcol"
					>
						<strong className="webfont-secondary black">
							{self.name}
						</strong>
						{title}
						<div className="btn-more">
							<div className="text-upper btn-primary-on-hover primary-font">
								{btnReadMoreLabel}
							</div>
						</div>
					</div>
				</a>
			);
		}
	});

	// Output biog info
	var DetailsPanel = React.createClass({
		// Store height value of existing details panel
		componentWillUpdate: function() {
			"use strict";

			var panelSection = document.querySelectorAll(".grid-details");

			panelSection[0].setAttribute("style", ""); // Clear any existing height

			// Get panel details
			var size = document.getElementById("panel").getBoundingClientRect();

			// Store height
			panelHeight.prevHeight = size.height;
		},

		// Adjust height after component has updated (this allows for panel height to animate)
		componentDidUpdate: function() {
			"use strict";

			var size = document.getElementById("panel").getBoundingClientRect(),
				panelSection = document.querySelectorAll(".grid-details"),
				panel = panelSection[0];

			panel.setAttribute(
				"style",
				"height:" + panelHeight.prevHeight + "px"
			);

			setTimeout(function() {
				panel.setAttribute("style", "height:" + size.height + "px");
			}, 50);

			panelHeight.prevHeight = size.height;
		},

		render: function() {
			"use strict";

			var props = this.props,
				data = props.items[props.gridItemID], // Get item's data
				item = typeof data === "object" ? data : "", // If the AJAX call has fired and returned some data
				length = props.items.length, // Number of records;
				img = item.img,
				output;

			// Output is different depending if the details panel should show an image or not
			if (typeof img !== "undefined") {
				// Has image (people)
				output = (
					<div id="panel" className="content padbot3">
						<div className="details-img-wrapper">
							<div
								className="img-large img-wrap circle"
								style={{ backgroundImage: "url(" + img + ")" }}
							/>
						</div>
						<div className="details-content-wrapper">
							<h2 className="black primary-font text-left no-mar">
								{item.name}
							</h2>
							<span className="primary-font">{item.title}</span>
							<p
								dangerouslySetInnerHTML={{ __html: item.desc }}
							></p>
							<a href={"mailto:" + item.email}>{item.email}</a>
						</div>
					</div>
				);
			} else {
				// Doesn't have an image (services)
				var button;

				// Does this item have a button?
				if (item.link !== "") {
					button = (
						<a
							href={domain + item.link}
							className="btn btn-primary width-auto margintop2 primary-font"
						>
							{btnFindOutMoreLabel}
						</a>
					);
				}
				output = (
					<div id="panel" className="content padbot3">
						<h2 className="webfont-secondary no-mar">
							{item.name}
						</h2>
						<span className="primary-font">{item.title}</span>
						<p dangerouslySetInnerHTML={{ __html: item.desc }}></p>
						{button}
					</div>
				);
			}

			// Fire GA event
			ga(
				"send",
				"event",
				"Content carousel",
				"" + item.name + "",
				"" + dcx.helpers.getPageTitle() + ""
			);

			return (
				<section
					className="grid-details oflow-hidden no-pad bg-grey posrel marginbot0"
					key={props.gridItemID}
				>
					<div className="container posrel">
						<DetailsNavigation
							id={props.gridItemID}
							label="Previous item"
							type="prev"
							itemState={props.itemState}
							length={length}
						/>
						<DetailsNavigation
							id={null}
							label="Close"
							type="close"
							itemState={props.itemState}
							length={length}
						/>
						<DetailsNavigation
							id={props.gridItemID}
							label="Next item"
							type="next"
							itemState={props.itemState}
							length={length}
						/>
						{output}
					</div>
				</section>
			);
		}
	});

	// Details panel navigation button actions
	var DetailsNavigation = React.createClass({
		// Prop validation (https://facebook.github.io/react/docs/reusable-components.html)
		propTypes: {
			type: React.PropTypes.string,
			id: React.PropTypes.number,
			length: React.PropTypes.number,
			itemState: React.PropTypes.func,
			label: React.PropTypes.string
		},

		handleClick: function() {
			"use strict";

			var buttonType = this.props.type,
				newGridItemID = null,
				length = this.props.length;

			// Increment/decrement depending on button clicked
			if (buttonType === "prev") {
				newGridItemID = this.props.id - 1;

				// Fire GA event
				ga(
					"send",
					"event",
					"Content carousel",
					"Profile previous",
					"" + dcx.helpers.getPageTitle() + ""
				);
			} else if (buttonType === "next") {
				newGridItemID = this.props.id + 1;

				// Fire GA event
				ga(
					"send",
					"event",
					"Content carousel",
					"Profile next",
					"" + dcx.helpers.getPageTitle() + ""
				);
			}

			// Ensure infinite nav is possible
			if (newGridItemID > length - 1) {
				newGridItemID = 0;
			} else if (newGridItemID < 0) {
				newGridItemID = length - 1;
			}

			// Change the state to the new user ID
			this.props.itemState(newGridItemID);

			// If clicking on next or prev buttons
			if (this.props.type !== "close") {
				// Scroll to correct position
				setTimeout(function() {
					dcx.scrollTo(
						document.getElementById("item-" + newGridItemID),
						slideAndScrollDuration
					);
				}, slideAndScrollDuration + 100);

				// Show all items on mobile viewpoint & delete show all button (only first 4 shown by default)
				dcx.helpers.removeClass(
					document.getElementById("details-grid"),
					"mobile-limit"
				);
				var gridBtn = document.querySelectorAll(".grid-btn");

				if (gridBtn.length > 0) {
					gridBtn[0].parentNode.removeChild(gridBtn[0]); // Delete button
				}
			}

			// Tracking
			if (this.props.type === "close") {
				ga(
					"send",
					"event",
					"Content carousel",
					"Profile close",
					"" + dcx.helpers.getPageTitle() + ""
				);
			}
		},

		render: function() {
			"use strict";

			return (
				<button
					className="button-reset grey-rollover text-indent"
					id={this.props.type}
					onClick={this.handleClick}
				>
					{this.props.label}
				</button>
			);
		}
	});

	// Render Main component to the screen, to the mountTo div (declared above)
	ReactDOM.render(<Main />, mountTo);
}
