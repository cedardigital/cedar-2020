/* ----------
Cedar DCX
- Handle deferred font loading - Uses Font Face Observer (https://github.com/bramstein/fontfaceobserver)
---------- */

// Defer font loading by loading via JS once the DOM has loaded
dcx.fontLoader = function() {
	"use strict";

	var bodyDOM = document.querySelector("body"),
		timeoutDuration = 30000, // 30 seconds
		webfonts = {
			primaryFont: "universcondensed", // Add your font names here
			secondaryFont: "universboldcondensed",
			tertiaryFont: "universcondensedoblique"
		};

	// Only fire code if body tag has no webfont-* classes
	if (
		!dcx.helpers.hasClass(bodyDOM, "webfont-primary") &&
		!dcx.helpers.hasClass(bodyDOM, "webfont-secondary") &&
		!dcx.helpers.hasClass(bodyDOM, "webfont-tertiary")
	) {
		// Load primary font
		var webfontPrimary = new FontFaceObserver(webfonts.primaryFont, {});

		webfontPrimary.check(null, timeoutDuration).then(
			function() {
				// Font loaded! Add font class to body tag
				bodyDOM.className = bodyDOM.className + " webfont-primary";

				// Set font cookie that expires in 1 year
				dcx.helpers.setCookie("webfont-primary", "true", 365);
			},
			function() {
				console.log(
					webfonts.primaryFont +
						" font did not load within the timeout of " +
						timeoutDuration +
						" ms"
				);
			}
		);

		// Load secondary font
		var webfontSecondary = new FontFaceObserver(webfonts.secondaryFont, {});

		webfontSecondary.check(null, timeoutDuration).then(
			function() {
				// Font loaded! Add font class to body tag
				bodyDOM.className = bodyDOM.className + " webfont-secondary";

				// Set font cookie that expires in 1 year
				dcx.helpers.setCookie("webfont-secondary", "true", 365);
			},
			function() {
				console.log(
					webfonts.secondaryFont +
						" font did not load within the timeout of " +
						timeoutDuration +
						" ms"
				);
			}
		);

		// Load tertiary font
		var webfontTertiary = new FontFaceObserver(webfonts.tertiaryFont, {});

		webfontTertiary.check(null, timeoutDuration).then(
			function() {
				// Font loaded! Add font class to body tag
				bodyDOM.className = bodyDOM.className + " webfont-tertiary";

				// Set font cookie that expires in 1 year
				dcx.helpers.setCookie("webfont-tertiary", "true", 365);
			},
			function() {
				console.log(
					webfonts.tertiaryFont +
						" font did not load within the timeout of " +
						timeoutDuration +
						" ms"
				);
			}
		);
	}
};
