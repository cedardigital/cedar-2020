/* ----------
Cedar DCX
- Accordion JS
---------- */

// Accordion
dcx.accordion = function() {
	"use strict";

	var accordion = document.querySelectorAll(".accordion:not(#contact-wrapper)");		// Accordion Headings (not for the contact form)

	if (accordion !== null) {
		// When the user clicks on that element
		// Toggle Class "opened"

		// Add event listener to all of the elements with the "accordion" class
		for (var i = 0; i < accordion.length; i++) {
			accordion[i].addEventListener("click", function() {
				// Toggle the accordion you're clicking on
				dcx.helpers.toggleClass(this.parentNode, "opened");

				// Fire GA event
				var label = (this.getAttribute("data-accordion") === "footer") ? "Footer" : dcx.helpers.getPageTitle();

				ga("send", "event", "Accordion", "" + this.innerHTML.replace(/<[^>]*>?/g, "") + "", "" + label + "");
			});
		}
	}
};