/* ----------
Cedar DCX
- Carousel Arrows JS
---------- */

// Remove Focus from Carousel Arrows
dcx.carouselNav = function() {
	"use strict";

	var previous = document.querySelectorAll(".slick-prev"),
		next = document.querySelectorAll(".slick-next");

	// Remove the focus
	var removeFocus = function(el) {
		el.blur();
	};

	// If the carousel exists (if the arrows are present)
	if (previous.length > 0 && next.length > 0) {
		// Add Event Listener to Previous Arrow
		previous[0].addEventListener("click", function() {
			removeFocus(previous[0]);
		});

		// Add Event Listener to Next Arrow
		next[0].addEventListener("click", function() {
			removeFocus(next[0]);
		});
	}
};