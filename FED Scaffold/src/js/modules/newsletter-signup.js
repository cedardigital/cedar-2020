/* ----------
Cedar DCX
- Newsletter signup JS
---------- */

// Post to Campaign Monitor using JSONP (to get around cross-domain access issues in some browsers)
/* eslint-disable */
dcx.postToCampaignMonitor = function(elForm, email, toCall) {
	"use strict";

	// Campaign Monitor specific URL to post to
	var url = elForm.getAttribute("action") + "?cm-tjhukk-tjhukk=" + email;

	// No specific data need to be sent
	var data = {};

	// We need a function callback to be executed after the response is received
	var callback = function(response) {
		toCall(response.Status);
	};

	// Call J50Npi function (https://github.com/robertodecurnex/J50Npi/)
	J50Npi.getJSON(url, data, callback);
};
/* eslint-enable */