/* ----------
Cedar DCX
- Footer Forms JS
---------- */

dcx.footerNewsletter = function() {
	"use strict";

	var newsletterSubmit = document.getElementById("newsletter-signup"), // Newsletter Submit button
		panel1 = document.getElementsByClassName("panel1"),				 // Stage 1
		panel2 = document.getElementsByClassName("panel2");				 // Stage 2

	// Function to update the panels
	var updatePanels = function(eleHide, eleShow) {

		// Hide previous panel
		dcx.helpers.removeClass(eleShow, "opacity0");
		eleShow.style.visibility = "visible";

		// Show new panel
		dcx.helpers.addClass(eleHide, "opacity0");
		eleHide.style.visibility = "hidden";
	};

	// If the page contains the Footer Newsletter panel
	if (newsletterSubmit !== null) {

		// Newsletter form feedback
		var signupFeedback = function(statusCode) {

			if (statusCode === 200) { // 200 = successfully added to Campaign Monitor
				updatePanels(panel1[0], panel2[0]);

				// Fire GA event
				ga("send", "event", "CTA", "Newsletter Signup", "" + dcx.helpers.getPageTitle() + "");
			} else {
				alert("There was a problem adding your details.\nPlease try again.");

				// Fire GA event
				ga("send", "event", "CTA", "Newsletter Signup - failed", "Header");
			}
		};

		newsletterSubmit.addEventListener("submit", function(e) {
			e.preventDefault();

			// Add data to Campaign Monitor
			dcx.postToCampaignMonitor(newsletterSubmit, document.getElementById("fieldEmail").value, signupFeedback);
		});
	}
};


dcx.footerContactForm = function() {
	"use strict";

	var messageBtn = document.getElementById("message-btn"),				// Message Button
		messageSubmit = document.getElementById("contact-form"),			// Message Submit button
		panel2 = document.getElementsByClassName("panel2"),					// Panel 2
		panel3 = document.getElementsByClassName("panel3"),					// Panel 3
		container = document.querySelectorAll(".footer-cta .extra-pad"),	// Container
		sendBtn = document.getElementById("message-submit");				// Send form button

	// If the page contains the Footer contact panel
	if (messageBtn !== null) {
		// Step 1
		messageBtn.addEventListener("click", function() {
			// Hide button
			dcx.helpers.addClass(this, "opacity0");

			// Animate in accordion
			dcx.helpers.addClass(panel2, "opened");

			// Fire GA event
			ga("send", "event", "CTA", "Get in touch open", "" + dcx.helpers.getPageTitle() + "");

			// Step 2
			messageSubmit.addEventListener("submit", function(e) {
				e.preventDefault();

				// Add disabled state to button
				sendBtn.setAttribute("disabled", "disabled");

				// Form elements
				var formFields = document.querySelectorAll("#contact-form input:not(#message-submit), #contact-form textarea"),
					formName = formFields[0].value,
					formJob = formFields[1].value,
					formEmail = formFields[2].value,
					formMessage = formFields[3].value;

				// Post to web service
				var request = new XMLHttpRequest();

				request.open("POST", "/umbraco/Surface/FormSurface/Contact", true); // This won't work on FED localhost
				request.setRequestHeader("Content-type", "application/json;charset=UTF-8");

				// If post page exists
				request.onreadystatechange = function() {
					if (request.readyState === 4 && request.status === 200) {
						var success = JSON.parse(request.responseText); // Server response

						// If server response is true (success)
						if (success.Result) {
							dcx.helpers.addClass(container, "opacity0");		// Fade out panel
							dcx.helpers.removeClass(panel2, "opened");			// Close accordion
							panel3[0].style.visibility = "visible";				// Make panel visible
							setTimeout(function() {
								dcx.helpers.removeClass(panel3, "opacity0");	// Fade in thank you message
							}, 500);

							// Fire GA event
							ga("send", "event", "CTA", "Get in touch send", "" + dcx.helpers.getPageTitle() + "");

						} else { // If server response is false (failure)
							// Show error message
							dcx.footerContactFormError(sendBtn);
						}

					} else {
						// Show error message
						dcx.footerContactFormError(sendBtn);
					}
				};

				// Post data
				var data = JSON.stringify({
					"message":
					{
						"Name": formName,
						"JobTitle": formJob,
						"Email": formEmail,
						"Message": formMessage
					}
				});

				request.send(data);

			});
		});
	}
};


// Form post error message
dcx.footerContactFormError = function(sendBtn) {
	"use strict";

	var errorText = document.getElementById("contact-form-error");

	errorText.removeAttribute("class");

	// Remove button disabled state
	sendBtn.removeAttribute("disabled");

	// Fire GA event
	ga("send", "event", "CTA", "Get in touch send - failed", "" + dcx.helpers.getPageTitle() + "");
};