/* ----------
Cedar DCX
- Homepage Banner JS
---------- */

dcx.scrollToFirstPanel = function() {
	"use strict";

	var scrollBtn = document.getElementById("hp-scroll"),
		homeBanner = document.getElementById("home-banner");

	if (scrollBtn !== null) {
		scrollBtn.addEventListener("click", function() {
			dcx.scrollTo(homeBanner.nextElementSibling, 500);
		});
	}
};