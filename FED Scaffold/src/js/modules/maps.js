/* ----------
Cedar DCX
- Contact us maps
---------- */

dcx.mapInit = function() {
	"use strict";

	// Get nodelist of map DOM elements
	var maps = document.querySelectorAll(".map-wrapper");

	// If we have any maps on this page
	if (maps !== null) {
		// Loop through and draw each map
		for (var i = 0; i < maps.length; i++) {
			dcx.drawMap(maps[i]);
		}
	}
};


// Draw map to screen (uses Google Maps API)
/*eslint-disable */
dcx.drawMap = function(el) {
	"use strict";

	// Declare our variables
	var lat = parseFloat(el.getAttribute("data-map-lat")),
		lng = parseFloat(el.getAttribute("data-map-lng"));

	// Do we have valid lat and lng values
	if (!isNaN(lat) && !isNaN(lng)) {
		var map = new google.maps.Map(el, {
			center: {
				lat: lat,
				lng: lng
			},
			scrollwheel: false,
  			mapTypeControl: false,
			zoom: 16
		});

		// Add custom marker to map
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(lat, lng),
			map: map,
			icon: "/images/map-pointer.png"
		});

		// Configure visual appearance of the map
		var styleArray = [
			{
				"featureType": "administrative",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#444444"
					}
				]
			},
			{
				"featureType": "landscape",
				"elementType": "all",
				"stylers": [
					{
						"color": "#f2f2f2"
					}
				]
			},
			{
				"featureType": "poi",
				"elementType": "all",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "road",
				"elementType": "all",
				"stylers": [
					{
						"saturation": -100
					},
					{
						"lightness": 45
					}
				]
			},
			{
				"featureType": "road.highway",
				"elementType": "all",
				"stylers": [
					{
						"visibility": "simplified"
					}
				]
			},
			{
				"featureType": "road.highway",
				"elementType": "geometry.fill",
				"stylers": [
					{
						"color": "#ffffff"
					}
				]
			},
			{
				"featureType": "road.arterial",
				"elementType": "labels.icon",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "transit",
				"elementType": "all",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "water",
				"elementType": "all",
				"stylers": [
					{
						"color": "#dde6e8"
					},
					{
						"visibility": "on"
					}
				]
			}
		];

		map.setOptions({styles: styleArray});
	}
};
/*eslint-enable */