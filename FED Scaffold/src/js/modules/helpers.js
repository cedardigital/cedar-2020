/* ----------
Cedar DCX
- Helpers to use in the absence of jQuery
---------- */

// Add class to ID selector
dcx.helpers.addClass = function(el, className) {
	"use strict";

	var _size = el.length, // Get size of selector
		_applyClass = function(el, className) { // Logic for adding class
			if (el.classList) {
				el.classList.add(className);
			} else {
				el.className += " " + className;
			}
		};

	// If there are multiple elements (ie, document.querySelectorAll)
	if (_size > 0) {
		for (var i = 0; i < el.length; i++) {
			_applyClass(el[i], className);
		}
	} else { // Single element (ie, document.getElementById)
		_applyClass(el, className);
	}
};


// Remove class
dcx.helpers.removeClass = function(el, className) {
	"use strict";

	var _size = el.length, // Get size of selector
		_applyClass = function(el, className) { // Logic for adding class
			if (el.classList) {
				el.classList.remove(className);
			} else {
				try { // If the class to remove isn't found, an error is thrown
					el.className = el.className.replace(new RegExp("(^|\\b)" + className.split(" ").join("|") + "(\\b|$)", "gi"), " ");
				} catch (e) {
					console.log(e);
				}
			}
		};

	// If there are multiple elements (ie, document.querySelectorAll)
	if (_size > 0) {
		for (var i = 0; i < el.length; i++) {
			_applyClass(el[i], className);
		}
	} else { // Single element (ie, document.getElementById)
		_applyClass(el, className);
	}
};


// Toggle class
dcx.helpers.toggleClass = function(el, className) {
	"use strict";

	var _size = el.length, // Get size of selector
		_applyClass = function(el, className) { // Logic for toggling class
			if (el.classList) {
				el.classList.toggle(className);
			} else {
				var classes = el.className.split(" ");
				var existingIndex = classes.indexOf(className);

				if (existingIndex >= 0) {
					classes.splice(existingIndex, 1);
				} else {
					classes.push(className);
				}

				el.className = classes.join(" ");
			}
		};

	// Undefined _size means a single element (ie, document.getElementById)
	if (typeof _size === "undefined") {
		_applyClass(el, className);
	} else { // A NodeList (ie, document.querySelectorAll)
		// Loop through all elements in the NodeList
		for (var i = 0; i < el.length; i++) {
			_applyClass(el[i], className);
		}
	}
};


// Has class
dcx.helpers.hasClass = function(el, className) {
	"use strict";

	if (el.classList) {
		return el.classList.contains(className);
	} else {
		return new RegExp("(^| )" + className + "( |$)", "gi").test(el.className);
	}
};


// Add event listener
dcx.helpers.addEventListener = function(el, eventName, handler) {
	"use strict";

	if (el.addEventListener) {
		el.addEventListener(eventName, handler);
	} else {
		el.attachEvent("on" + eventName, function() {
			handler.call(el);
		});
	}
};


// Check if it's the bp-xs breakpoint
dcx.helpers.bpxs = function() {
	"use strict";

	if (window.innerWidth < 480) {
		return true;
	} else {
		return false;
	}
};


// Check if it's the bp-s breakpoint
dcx.helpers.bps = function() {
	"use strict";

	if (window.innerWidth < 600) {
		return true;
	} else {
		return false;
	}
};


// Check if it's the bp-m breakpoint
dcx.helpers.bpm = function() {
	"use strict";

	if (window.innerWidth < 740) {
		return true;
	} else {
		return false;
	}
};


// Check if it's the bp-l breakpoint
dcx.helpers.bpl = function() {
	"use strict";

	if (window.innerWidth < 960) {
		return true;
	} else {
		return false;
	}
};


// Check if it's the bp-xl breakpoint
dcx.helpers.greaterThanBpl = function() {
	"use strict";

	if (window.innerWidth > 960) {
		return true;
	} else {
		return false;
	}
};


// Get page title, minus strapline
dcx.helpers.getPageTitle = function() {
	"use strict";

	var pageTitle = document.title;

	return pageTitle.substring(0, pageTitle.indexOf(" | "));
};


// Extract email address from mailto href
dcx.helpers.getEmailFromHref = function(emailAddress) {
	"use strict";

	return emailAddress.substring(emailAddress.indexOf(":") + 1, emailAddress.length);
};


// Helper to see if class exists
dcx.helpers.hasClass = function(target, className) {
	"use strict";

	return new RegExp("(\\s|^)" + className + "(\\s|$)").test(target.className);
};


// Set cookie
dcx.helpers.setCookie = function(name, value, days) {
	"use strict";

	var date = new Date(),
		expires;

	days = (!days) ? 30 : days; // No days set? Use 30 days
	date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
	expires = "; expires=" + date.toUTCString();

	document.cookie = name + "=" + value + expires + "; path=/";
};