/* ----------
Cedar DCX
- Load more button action
---------- */

// NOTE: For these buttons to work, they must immediately follow an element with a class name of "mobile-limit" in the markup
dcx.loadMoreButtons = function() {
	"use strict";

	var loadMoreBtns = document.querySelectorAll(".mobile-only");

	// Show all elements in the list
	var	showAll = function(el) {
		return function() {
			dcx.helpers.removeClass(el.previousElementSibling, "mobile-limit"); // Remove class from previous sibling
			el.parentNode.removeChild(el); // Delete button
		};
	};

	// If a load more button exists on the page
	if (loadMoreBtns.length > 0) {
		for (var i = 0; i < loadMoreBtns.length; i++) {
			loadMoreBtns[i].onclick = showAll(loadMoreBtns[i]);
		}
	}
};