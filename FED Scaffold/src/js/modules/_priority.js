/* ----------
Cedar DCX
- Priority JavaScript that is initiated in page <head>
---------- */

// Priority namespace
var dcxPriority = dcxPriority || {};

// Deferred stylesheet loading
dcxPriority.stylesheetloader = function() {
	"use strict";

	var cb = function() {
		var l = document.createElement("link"),
			// The CSS path will change on each deploy for caching reasons - need to extract the pathname from the noscript tag, <link> content
			h = document.getElementsByTagName("head")[0],
			noscript = h.querySelector("noscript");

		l.rel = "stylesheet";
		l.href = noscript.getAttribute("data-css-path");

		h.appendChild(l, h); // Inject stylesheet <link> tag into page head
	};

	var raf = requestAnimationFrame || mozRequestAnimationFrame || webkitRequestAnimationFrame || msRequestAnimationFrame;

	if (raf) {
		raf(cb);
	} else {
		window.addEventListener("load", cb);
	}

}(); // Self calling