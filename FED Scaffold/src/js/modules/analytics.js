/* ----------
Cedar DCX
- Analytics JS
---------- */

// Enable Google Analytics custom events
dcx.analytics = function() {
	"use strict";

	// Add event listener to listen for any click
	var init = function() {
		document.addEventListener("click", fireAnalyticsEvent, false);
	};

	// Fire a Google Analytics event if the link clicked has tracking data-* attribute values
	var fireAnalyticsEvent = function(e) {
		e = window.e || e;

		// Analytics data attributes
		var target = e.target,
			eventCategory = target.getAttribute("data-event-category"),
			eventAction = target.getAttribute("data-event-action"),
			eventLabel = target.getAttribute("data-event-label"),
			eventPosition = target.getAttribute("data-event-position");

		// No assigned label? Use the page title
		if (eventLabel === null) {
			if (eventPosition !== null) { // If a position exists, prepend to the event label
				eventLabel = eventPosition + " - " + dcx.helpers.getPageTitle();
			} else {
				eventLabel = dcx.helpers.getPageTitle();
			}
		}

		// Fire Google Analytics tracking function if eventCategory & eventAction have values (eventLabel is optional)
		if (eventCategory !== null && eventAction !== null) {
			ga("send", "event", "" + eventCategory + "", "" + eventAction + "", "" + eventLabel + "");
		} else if (target.tagName === "A") { // Is an anchor tag
			if (target.href.indexOf("mailto:") >= 0) { // Email address
				ga("send", "event", "Email link", "" + dcx.helpers.getEmailFromHref(target.href) + "", "" + dcx.helpers.getPageTitle() + "");
			} else if (target.href.indexOf("tel:") >= 0) { // Telephone link
				ga("send", "event", "Phone number", "" + target.innerHTML + "", "" + dcx.helpers.getPageTitle() + "");
			} else if (target.parentNode.getAttribute("data-tweet") === "text") { // Inline text href
				ga("send", "event", "Tweet", "Link click", "" + dcx.helpers.getPageTitle() + "");
			}
		}
	};

	return {
		init: init
	};

};