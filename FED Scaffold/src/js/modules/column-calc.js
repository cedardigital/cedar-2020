/* ----------
Cedar DCX
- Calculate the number of columns at the current viewpoint
---------- */

dcx.columnNumber = function() {
	"use strict";

	// Map your column breakpoints defined in the CSS
	var columns = function(max) {
		var bp = [],
			columnNo,
			windowWidth = window.innerWidth;

		// Max: 4 columns (add other max column else if statement - ie 3 or 5 - where applicable)
		if (max === 4) {
			bp.push(
				[320, 2],	// Wider or equal to 320px show 2 columns
				[600, 3], 	// Wider or equal to 600px show 3 columns
				[960, 4] 	// Wider or equal to 960px show 4 columns
			);
		}

		// Return column size
		for (var i = 0; i < bp.length; i++) {
			var bpWidth = bp[i][0],
				cols 	= bp[i][1];

			if (windowWidth >= bpWidth) {
				columnNo = cols;
			}
		}

		return columnNo;
	};

	return {
		columns: columns
	};

};