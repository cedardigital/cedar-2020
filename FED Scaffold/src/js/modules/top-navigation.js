/* ----------
Cedar DCX
- Top Navigation JS
---------- */

// Top Navigation
dcx.topNavigation = function() {
	"use strict";

	var overlay = document.getElementById("overlay-bg"), // Overlay
		mainNav = document.getElementById("main-nav"), // Main Navigation
		locNav = document.getElementById("loc-nav"), // Location Navigation
		follow = document.getElementById("follow"), // Follow Panel
		close = document.getElementById("mob-close"), // Close panel
		social = document.getElementById("animate-social"), // Social links to animate
		feedback = document.getElementById("feedback"); //,			// Feedback Message
	// form = document.getElementById("nav-form"),				// Newsletter Form
	// navForm = document.getElementById("nav-form");			// Newsletter Nav Form

	// Function to reset the top navigation
	var resetNav = function(addEl, removeEl, removeEl2) {
		dcx.helpers.addClass(addEl, "open-nav");
		dcx.helpers.removeClass(removeEl, "open-nav");
		dcx.helpers.removeClass(removeEl2, "open-nav");
		dcx.helpers.addClass(close, "slide");
		dcx.helpers.addClass(close, "show-btn");
	};

	var updateNav = function(e) {
		e = window.e || e;

		if (dcx.helpers.bpm()) {
			// If it's the mobile nav (less than 740)

			if (e.target.id === "menu-btn" || e.target.id === "loc-btn" || e.target.id === "follow-btn") {
				// Check what you've clicked on
				// Show the overlay
				dcx.helpers.addClass(overlay, "overlay-bg");
				dcx.helpers.addClass(mainNav, "hide");

				if (e.target.id === "menu-btn") {
					// If it's the Main Navigation
					resetNav(mainNav, locNav, follow);

					// Fire GA event
					ga("send", "event", "CTA mobile", "Show menu", "Header");
				} else if (e.target.id === "loc-btn") {
					// If it's the Location Button
					resetNav(locNav, mainNav, follow);

					// Fire GA event
					ga("send", "event", "CTA mobile", "Choose site", "Header");
				} else if (e.target.id === "follow-btn") {
					// If it's the Follow Button
					resetNav(follow, locNav, mainNav);

					// Fire GA event
					ga("send", "event", "CTA mobile", "Follow open", "Header");
				}
			} else if (e.target.id === "overlay-bg" || e.target.id === "close-btn") {
				// If it's the Overlay or the close button
				dcx.helpers.removeClass(mainNav, "open-nav");
				dcx.helpers.removeClass(mainNav, "hide");
				dcx.helpers.removeClass(locNav, "open-nav");
				dcx.helpers.removeClass(follow, "open-nav");
				dcx.helpers.removeClass(overlay, "overlay-bg");
				dcx.helpers.removeClass(close, "slide");

				// Fire GA event
				ga("send", "event", "CTA mobile", "Close button", "Header");
			}
		} else {
			if (e.target.id === "follow-btn") {
				// If it's the desktop navigation
				// Get the button width of the follow button
				var btnWidth = document.getElementById("follow-btn").offsetWidth;

				// // If it's a wide screen
				// if (dcx.helpers.greaterThanBpl()) {
				// 	// Set the width of the submit button
				// 	document.getElementById("nav-submit").style.width = btnWidth + "px";
				// }

				// Set the width of the close button
				document.getElementById("mob-close").style.width = btnWidth + "px";

				setTimeout(function() {
					dcx.helpers.addClass(follow, "slide"); // Slide down
					dcx.helpers.addClass(close, "show"); // Show the close button (X)
					dcx.helpers.addClass(social, "animate"); // Animate social buttons when you slide down the panel
				}, 120);

				// Fire GA event
				ga("send", "event", "CTA", "Follow open", "Header");
			} else if (e.target.id === "close-btn") {
				// If it's the close button
				dcx.helpers.removeClass(follow, "slide"); // Slide up
				dcx.helpers.removeClass(close, "show"); // Hide the close button (X)
				dcx.helpers.removeClass(social, "animate"); // Remove the animate class from the social buttons

				// Fire GA event
				ga("send", "event", "CTA", "Follow close", "Header");
			}
		}

		if (e.target.id === "close-btn") {
			// When user clicks on the close button (X)
			setTimeout(function() {
				dcx.helpers.addClass(feedback, "hide"); // Hide the feedback message (if it's visible)
			}, 300);

			//form.classList.remove("hide");	// Show the form
		}
	};

	// // Newsletter form feedback
	// var signupFeedback = function(statusCode) {
	// 	if (statusCode === 200) { // 200 = successfully added to Campaign Monitor
	// 		dcx.helpers.removeClass(feedback, "hide");
	// 		//form.classList.add("hide");

	// 		// Clear the input field - Reset form
	// 		navForm.reset();

	// 		// Fire GA event
	// 		ga("send", "event", "CTA", "Newsletter Signup", "Header");
	// 	} else {
	// 		// Alert error message - message taken from a data attribute in the form tag for translation purposes
	// 		alert(navForm.getAttribute("data-error"));

	// 		// Fire GA event
	// 		ga("send", "event", "CTA", "Newsletter Signup - failed", "Header");
	// 	}
	// };

	document.addEventListener("click", updateNav);

	// form.addEventListener("submit", function(e) {	// On submit of top navigation
	// 	e.preventDefault();

	// 	// Add data to Campaign Monitor
	// 	dcx.postToCampaignMonitor(document.getElementById("nav-form"), document.getElementById("newsletter").value, signupFeedback);
	// });
};
