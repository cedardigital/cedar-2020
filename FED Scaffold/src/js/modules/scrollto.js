/* ----------
Cedar DCX
- Scroll to element
---------- */

// Note - Uses third party scroll to plugin (see https://zengabor.github.io/zenscroll/)
dcx.scrollTo = function(el, duration) {
	"use strict";

	// Variables
	var bodyRect 	= document.body.getBoundingClientRect(),
		elemRect 	= el.getBoundingClientRect(),
		offset   	= elemRect.top - bodyRect.top,
		scrollDiff 	= offset - Math.abs(bodyRect.top),
		nav			= document.getElementById("top-bar"),
		navRect		= nav.getBoundingClientRect(),
		navOffset	= Math.ceil(navRect.height);

	if (dcx.helpers.bpm()) {
		navOffset = 0;
	}

	// Chrome has an issue where it scrolls one pixel up and down rather than stay put
	// Only fire scroll event if you're more than 10 pixels from where the scrollTo will be
	if (scrollDiff < -10 || scrollDiff > 10) {
		// Fire scroll event
		zenscroll.toY(Math.round(offset) - navOffset, duration);
	}
};