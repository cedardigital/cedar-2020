/* ----------
Cedar DCX
- Gruntfile
---------- */

// ESLint overrides
/*eslint no-undef: 0*/
/*eslint key-spacing: 0*/

const sass = require("node-sass");

module.exports = function(grunt) {
	"use strict";

	/* Configuration */
	grunt.initConfig({
		pkg: grunt.file.readJSON("package.json"),

		// Global Settings
		g: {
			src: "src",
			dest: "build"
		},

		// Clean
		clean: {
			html: ["<%= g.dest %>/*.html"],
			css: ["<%= g.dest %>/styles/**/*.*"],
			images: ["<%= g.dest %>/images/**/*.*"],
			fonts: ["<%= g.dest %>/fonts/**/*.*"],
			feeds: ["<%= g.dest %>/feeds/*.*"], // Remove Feeds
			jsmin: [
				"<%= g.dest %>/scripts/**/*.*",
				"!<%= g.dest %>/scripts/react-lib/*.min.js", // Don"t remove minified React
				"!<%= g.dest %>/scripts/**/modernizr.js", // Don"t remove modernizr
				"!<%= g.dest %>/scripts/**/*.min.js"
			], // Remove all but min.js
			jsx: ["<%= g.src %>/js/modules/jsx/transpiled/*.*"],
			cssmin: ["<%= g.dest %>/styles/**/*.*", "!<%= g.dest %>/styles/**/*.min.css"], // Remove all but min.css
			critical: ["<%= g.src %>/templates/partials/*.css"] // Remove temp partial CSS files (these are used to generate .hbs files)
		},

		// Watch
		watch: {
			options: {
				spawn: false,
				livereload: true
			},
			css: {
				files: ["<%= g.src %>/scss/**/*.scss"],
				tasks: ["stylelint", "sass", "postcss", "cssmin:build"]
			},
			js: {
				files: [
					"<%= g.src %>/js/**/*.js",
					"<%= g.src %>/js/modules/jsx/**/*.jsx",
					"!<%= g.src %>/js/libs/**/*.js", // Don't watch libs
					"!<%= g.src %>/js/react-lib/**/*.js", // Don't watch React library
					"!<%= g.src %>/js/modules/jsx/transpiled/*.js", // Don't watch transpiled JSX file
					"!<%= g.src %>/js/modernizr/*.js"
				], // Don't watch modernizr
				tasks: ["eslint", "concat:basic", "uglify:preserveConsole"]
			},
			jsx: {
				files: ["<%= g.src %>/js/modules/jsx/*.jsx"],
				tasks: ["clean:jsx", "browserify:build", "concat:basic", "uglify:preserveConsole"]
			},
			tmpl: {
				files: ["<%= g.src %>/templates/**/*.hbs"],
				tasks: ["assemble:build"]
			},
			img: {
				files: ["<%= g.src %>/images/**/*.*"],
				tasks: ["clean:images", "copy:images"]
			},
			html: {
				files: ["<%= g.dest %>/*.html"]
			}
		},

		// SASS
		sass: {
			options: {
				implementation: sass,
				sourceMap: true
			},
			dist: {
				files: [{src: "<%= g.src %>/scss/style.scss", dest: "<%= g.dest %>/styles/style.css"}]
			}
		},

		// PostCSS
		postcss: {
			options: {
				map: true,
				processors: [
					require("autoprefixer")({browsers: "> 1%, ie 10-11"}), // add vendor prefixes
					require("css-mqpacker")({sort: true}), // group media query declarations into single statements
					require("postcss-focus"), // Adds :focus to all :hover declarations
					require("postcss-responsive-type")() // Use responsive typography [https://www.npmjs.com/package/postcss-responsive-type]
				]
			},
			dist: {
				src: ["<%= g.dest %>/styles/style.css"]
			}
		},

		// CSSMin
		cssmin: {
			build: {
				// Minify, keeping sourcemaps
				options: {
					sourceMap: true
				},
				files: [{"<%= g.dest %>/styles/style.min.css": ["<%= g.dest %>/styles/style.css"]}]
			},
			dist: {
				// Minify for deployment, removing sourcemaps
				options: {
					sourceMap: false
				},
				files: [{"<%= g.dest %>/styles/style.min.css": ["<%= g.dest %>/styles/style.css"]}]
			},
			critical: {
				// Minify critical path CSS and convert to handlebars files
				options: {
					sourceMap: false
				},
				files: [
					{
						expand: true,
						cwd: "<%= g.src %>/templates/partials/",
						src: ["*.css"],
						dest: "<%= g.src %>/templates/partials/",
						ext: ".hbs"
					}
				]
			}
		},

		// Remove unused CSS
		purifycss: {
			options: {},
			target: {
				src: [
					"<%= g.dest %>/*.html",
					"<%= g.dest %>/scripts/*.min.js",
					"<%= g.dest %>/scripts/react-lib/react-slick.min.js",
					"<%= g.dest %>/scripts/modernizr.js"
				], // Observe all html & JS files, inc slick
				css: ["<%= g.dest %>/styles/style.css"], // Take all css files into consideration
				dest: "<%= g.dest %>/styles/style.css" // Write to this path
			}
		},

		// Generate critical CSS - create new hardcoded :( record for each template
		penthouse: {
			homepage: {
				outfile: "<%= g.src %>/templates/partials/_critical-homepage.css",
				css: "<%= g.dest %>/styles/style.min.css",
				url: grunt.option("url") + "/homepage.html",
				width: 1366,
				height: 990,
				skipErrors: false
			},
			defaultPage: {
				outfile: "<%= g.src %>/templates/partials/_critical-default.css",
				css: "<%= g.dest %>/styles/style.min.css",
				url: grunt.option("url") + "/default.html",
				width: 1366,
				height: 990,
				skipErrors: false
			},
			article: {
				outfile: "<%= g.src %>/templates/partials/_critical-article.css",
				css: "<%= g.dest %>/styles/style.min.css",
				url: grunt.option("url") + "/article.html",
				width: 1366,
				height: 990,
				skipErrors: false
			},
			casestudy: {
				outfile: "<%= g.src %>/templates/partials/_critical-casestudy.css",
				css: "<%= g.dest %>/styles/style.min.css",
				url: grunt.option("url") + "/case-study.html",
				width: 1366,
				height: 990,
				skipErrors: false
			},
			contact: {
				outfile: "<%= g.src %>/templates/partials/_critical-contact.css",
				css: "<%= g.dest %>/styles/style.min.css",
				url: grunt.option("url") + "/contact.html",
				width: 1366,
				height: 990,
				skipErrors: false
			},
			news: {
				outfile: "<%= g.src %>/templates/partials/_critical-news.css",
				css: "<%= g.dest %>/styles/style.min.css",
				url: grunt.option("url") + "/news-and-views.html",
				width: 1366,
				height: 990,
				skipErrors: false
			},
			subpage: {
				outfile: "<%= g.src %>/templates/partials/_critical-subpage.css",
				css: "<%= g.dest %>/styles/style.min.css",
				url: grunt.option("url") + "/subpage.html",
				width: 1366,
				height: 990,
				skipErrors: false
			},
			work: {
				outfile: "<%= g.src %>/templates/partials/_critical-work.css",
				css: "<%= g.dest %>/styles/style.min.css",
				url: grunt.option("url") + "/work.html",
				width: 1366,
				height: 990,
				skipErrors: false
			}
		},

		// ESLint - see: http://eslint.org/docs/rules/
		eslint: {
			target: ["<%= watch.js.files %>"]
		},

		// Stylelint - see: http://stylelint.io/user-guide/rules/
		stylelint: {
			target: ["<%= g.src %>/scss/**/*.scss"]
		},

		browserify: {
			options: {
				debug: true,
				transform: ["reactify"],
				extensions: [".js"]
			},
			build: {
				options: {
					debug: false
				},
				src: ["<%= g.src %>/js/modules/jsx/*.jsx"],
				dest: "<%= g.src %>/js/modules/jsx/transpiled/jsx-concat.js"
			}
		},

		// Concat
		concat: {
			basic: {
				options: {
					separator: "\n\n"
				},
				src: [
					// Third party libraries
					"<%= g.src %>/js/libs/*",
					// Namespace
					"<%= g.src %>/js/namespace.js",
					// Site specific modules (except source JSX files & priority JS file)
					"<%= g.src %>/js/modules/**/*",
					//"!<%= g.src %>/js/modules/_priority.js",
					"!<%= g.src %>/js/modules/jsx/*.jsx",
					// Don"t concat our static feed files
					"!<%= g.src %>/js/feeds/*.js",
					// Init
					"<%= g.src %>/js/init.js"
				],
				dest: "<%= g.dest %>/scripts/script.js"
			}
		},

		// Uglify
		uglify: {
			build: {
				options: {
					mangle: true,
					preserveComments: false,
					compress: {
						drop_console: true
					},
					sourceMap: false
				},
				src: "<%= g.dest %>/scripts/script.js",
				dest: "<%= g.dest %>/scripts/script.min.js"
			},
			preserveConsole: {
				options: {
					mangle: true,
					preserveComments: false,
					sourceMap: true
				},
				src: "<%= g.dest %>/scripts/script.js",
				dest: "<%= g.dest %>/scripts/script.min.js"
			},
			buildPriority: {
				options: {
					mangle: true,
					preserveComments: false,
					compress: {
						drop_console: false
					},
					sourceMap: false
				},
				src: "<%= g.dest %>/scripts/_priority.js",
				dest: "<%= g.dest %>/scripts/_priority.min.js"
			}
		},

		// Minify SVGs
		svgmin: {
			options: {
				plugins: [{removeViewBox: false}, {removeUselessStrokeAndFill: false}]
			},
			dist: {
				files: [
					{
						expand: true,
						cwd: "<%= g.src %>/images",
						src: ["**/*.svg"],
						dest: "<%= g.dest %>/images"
					}
				]
			}
		},

		// Optimise images
		smushit: {
			options: {
				service: "http://resmush.it/ws.php"
			},
			files: {
				expand: true,
				src: ["<%= g.src %>/images/**/*.png", "<%= g.src %>/images/**/*.jpg"],
				dest: "<%= g.dest %>/images"
			}
		},

		// Assemble
		assemble: {
			options: {
				partials: ["<%= g.src %>/templates/partials/**/*.hbs"],
				layout: ["<%= g.src %>/templates/layout.hbs"]
			},
			build: {
				options: {
					flatten: true
				},
				src: ["<%= g.src %>/templates/pages/**/*.hbs"],
				dest: "<%= g.dest %>/"
			},
			dist: {
				options: {
					data: "type.json",
					flatten: true
				},
				src: ["<%= g.src %>/templates/pages/**/*.hbs"],
				dest: "<%= g.dest %>/"
			}
		},

		// Copy
		copy: {
			images: {
				expand: true,
				cwd: "<%= g.src %>/images/",
				src: "**",
				dest: "<%= g.dest %>/images/",
				flatten: false
			},
			fonts: {
				expand: true,
				cwd: "<%= g.src %>/fonts/",
				src: "**",
				dest: "<%= g.dest %>/fonts/",
				flatten: false
			},
			modernizr: {
				src: "<%= g.src %>/js/modernizr/modernizr.js",
				dest: "<%= g.dest %>/scripts/modernizr.js"
			},
			feeds: {
				expand: true,
				cwd: "<%= g.src %>/js/feeds/",
				src: "**",
				dest: "<%= g.dest %>/feeds/",
				flatten: false
			},
			react: {
				expand: true,
				cwd: "<%= g.src %>/js/react-lib/",
				src: "**",
				dest: "<%= g.dest %>/scripts/react-lib/",
				flatten: false
			},
			favicon: {
				src: "<%= g.src %>/images/favicon/favicon.ico",
				dest: "<%= g.dest %>/favicon.ico"
			},
			jsPriority: {
				src: "<%= g.src %>/js/modules/_priority.js",
				dest: "<%= g.dest %>/scripts/_priority.js"
			}
		},

		// express will serve files (to enable livereload)
		express: {
			all: {
				options: {
					port: 8080,
					hostname: "0.0.0.0",
					bases: [__dirname + "/build"],
					livereload: true
				}
			}
		},

		// grunt-open will open your browser at the project's URL
		open: {
			all: {
				// Gets the port from the connect configuration
				path: "http://localhost:<%= express.all.options.port%>"
			}
		},

		// browserSync
		browserSync: {
			dev: {
				bsFiles: {
					src: ["<%= g.dest %>/**/**/*.*"]
				},
				options: {
					watchTask: true,
					server: "<%= g.dest %>"
				}
			}
		}
	});

	/* Load NPM Tasks */
	grunt.loadNpmTasks("grunt-assemble");
	grunt.loadNpmTasks("grunt-browserify");
	grunt.loadNpmTasks("grunt-browser-sync");
	grunt.loadNpmTasks("grunt-contrib-clean");
	grunt.loadNpmTasks("grunt-contrib-concat");
	grunt.loadNpmTasks("grunt-contrib-copy");
	grunt.loadNpmTasks("grunt-contrib-cssmin");
	grunt.loadNpmTasks("grunt-contrib-watch");
	grunt.loadNpmTasks("grunt-contrib-uglify");
	grunt.loadNpmTasks("grunt-eslint");
	grunt.loadNpmTasks("grunt-express");
	grunt.loadNpmTasks("grunt-open");
	grunt.loadNpmTasks("grunt-penthouse");
	grunt.loadNpmTasks("grunt-postcss");
	grunt.loadNpmTasks("grunt-purifycss");
	grunt.loadNpmTasks("grunt-sass");
	grunt.loadNpmTasks("grunt-smushit");
	grunt.loadNpmTasks("grunt-stylelint");
	grunt.loadNpmTasks("grunt-svgmin");

	/* Tasks */

	// Default Task
	grunt.registerTask("default", ["devPrep", "watch"]);

	// Watch, with livereload
	grunt.registerTask("serve", ["devPrep", "express", "open", "watch"]);

	// Watch, with browserSync
	grunt.registerTask("sync", ["devPrep", "browserSync", "watch"]);

	// Create critical path CSS. Usage: add URL - ie: grunt critical --url=http://localsitename
	grunt.registerTask("critical", ["penthouse", "cssmin:critical", "assemble:dist", "clean:critical"]);

	// Build (for deployment)
	grunt.registerTask("build", [
		"stylelint",
		"eslint",
		"clean",
		"assemble:dist",
		"browserify:build",
		"concat:basic",
		"uglify:build",
		"sass",
		"postcss",
		"purifycss",
		"cssmin:dist",
		"copy",
		"uglify:buildPriority",
		"smushit",
		"svgmin",
		"clean:cssmin",
		"clean:jsmin"
	]);

	// Prepare environment for React development
	grunt.registerTask("devPrep", ["assemble:build", "copy:react", "concat:basic", "uglify:preserveConsole"]);

	// All - run critical path, build, watch and livereload. Need to add URL parameter - ie: grunt all --url=http://localsitename
	grunt.registerTask("all", ["critical", "build", "serve"]);
};
