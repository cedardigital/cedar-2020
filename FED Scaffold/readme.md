###### Cedar DCX

# Front-end Resources

## Folder Structure

The Frontend folder should live next to the webroot in your project eg;

* Project
  * Frontend
  * www

## Environment Dependencies

This project requires for following environment dependencies;

* [Node](http://nodejs.org)
* [Ruby](https://ruby-lang.org)
* [Git](http://git-scm.com/download)
* SASS Ruby gem, installed from the command line with `gem install sass`
* Grunt client, installed from the command line with `npm install -g grunt-cli`

## Project Dependencies

Development packages are managed via [NPM](http://npmjs.org) and are defined in the project's package.json file.

New dependenceis are installed using the following command: `npm install packagename --save`

## Project Information

The site has been built with the following tech:

* Minimum browser support: IE10
* Flexbox (for layout)
* React.js (for some interactive components)
* [Handlebars](http://handlebarsjs.com/) for templating

The site does not use jQuery.

## Setup

From the command line, inside the Frontend folder, run the following command: `npm install` (ignore any errors relating to Phython)

## File structure

### build
The files compiled by Grunt. Do not edit these files directly. Back-end developers should copy these files for deployment.

### src
Source files for the project. Edit these as part of development and maintenance.

#### fonts
Add any self-hosted web fonts here.

#### images
Images directory.

#### js
JavaScript directory. Contains:

* feeds - Custom hard-coded JSON feeds
* libs - Add any third party, self-hosted libraries or plugins here.
* modernizr - Add minified custom [Modernizr](http://www.modernizr.com)
* modules - Use a modular approach to JS development. Place each module in its own file in this directory.
* react-lib - React.js library files.
* init.js - Add code to fire on page load here.
* namespace.js - Declares our DCX namespacing.

#### scss
SASS directory. Note - prefix all scss filenames with an underscore if they are only to be imported. Otherwise a separate css file will be created when the Grunt task runs.

Contents:

* style.scss - Responsively enabled stylesheet that imports the files listed below.

##### base
* _buttons.scss - Styles for site-wide buttons.
* _core.scss - Generic site-wide styling (headings, p, links, etc).
* _fonts.scss - Declare any custom web fonts here.
* _helpers.scss - Collection of useful classes (text aligns, floats, etc).
* _reset.scss - Reset stylesheet to aid consistent appearance across browsers.

##### components
Use a modular approach to SASS development. Place each non-page specific module in its own file in this directory.

##### pages
Use a modular approach to SASS development. Place each page specific module in its own file in this directory.

##### utils
* _media-queries.scss - Media query mixins and configuration.
* _variables.scss - Site-wide SASS variables.

#### templates
Built using [Handlebars](http://handlebarsjs.com/)

Contents:

* layout.hbs - Generic page structure (head, scripts, styles) go here.

##### pages
Specific page layouts go here.

* index.hbs - This will be a page listing all user created pages in this directory. Do not delete.

##### partials
Home to page partials/sections. Critical path CSS files are also generated here.

## Grunt configuration

Notable Gruntfile actions:

* Code concatenation and minification
* Image (JPG, PNG, SVG) optimisation
* [stylelint](http://stylelint.io/) for SASS linting
* [ESLint](http://eslint.org/) for JavaScript/JSX linting
* [PostCSS](http://postcss.org/) for Autoprefixing and media query compaction (and other tasks)
* [purifycss](https://github.com/purifycss/purifycss) to remove unused CSS
* [penthouse](https://www.npmjs.com/package/penthouse) for critical path CSS extraction
* [browserify](http://browserify.org/) for transpiling React JSX files

There are six Grunt tasks that can be run from the command line:

### grunt
The default task will watch for changes in SASS, JavaScript, handlebar templates and images and recompile when a change is made.

### grunt serve
Same as the default grunt task, but uses live reload.

### grunt sync
Same as the default grunt task, but uses browserSync with live reload.

### grunt critical
Creates critical path CSS. Usage: add URL - ie: `grunt critical --url=http://localsitename`
Be sure to add all your templates to the `penthouse` task within the `Gruntfile.js` file.
For the critical path CSS handlebars includes to be used, you must add `inlineCritical: true` and the partial file name (ie: `criticalPartial: critical-homepage`) to your templates > pages > pagename.hbs file.

### grunt build
Builds/compiles/compresses/minifies assets ready for deployment. This removes CSS map file references and JavaScript console.log commands from the output files.

### grunt devPrep
Prepare environment for React development.

### grunt all
Run critical path, build, watch and livereload (excludes browserSync). Need to add URL parameter - ie: `grunt all --url=http://localsitename`